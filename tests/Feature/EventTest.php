<?php

namespace Tests\Feature;

use \DateTime;

use App\Models\Club;
use App\Models\Event;
use App\Models\Playground;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EventTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testNotBlockSchedule()
    {
        $date = (new DateTime())->format('Y-m-d');
        $interval = ['9:00', '10:00'];
        $intervalPg = "['9:00:00+03', '10:00:00+03')";

        $event = factory(Event::class)->create([
            'type' => Event::TYPE_MEETING,
            'blockSchedule' => false,
            'date' => $date,
            'interval' => $interval
        ]);

        $this->assertTrue($event->id > 0, 'Event not created');
        $this->assertDatabaseHas('events', [
            'id' => $event->id,
            'date' => $date,
            'interval' => $intervalPg,
        ]);

        $this->assertDatabaseMissing('busy_records', [
            'busy_recordable_id' => $event->id,
            'busy_recordable_type' => 'events'
        ]);
    }

    public function testBlockSchedule()
    {
        $date = (new DateTime())->format('Y-m-d');
        $interval = ['9:00', '10:00'];
        $intervalPg = "['9:00:00+03', '10:00:00+03')";

        $playground = factory(Playground::class)->create([
            'club_id' => factory(Club::class)->create()->id
        ]);

        $event = factory(Event::class)->create([
            'type' => Event::TYPE_GAME,
            'blockSchedule' => true,
            'playground_id' => $playground->id,
            'date' => $date,
            'interval' => $interval
        ]);

        $this->assertTrue($event->id > 0, 'Event not created');
        $this->assertDatabaseHas('events', [
            'id' => $event->id,
            'date' => $date,
            'interval' => $intervalPg,
        ]);
        $this->assertDatabaseHas('busy_records', [
            'busy_recordable_id' => $event->id,
            'busy_recordable_type' => 'events',
            'date' => $date,
            'interval' => $intervalPg
        ]);
    }
}
