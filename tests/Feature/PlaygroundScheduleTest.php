<?php

namespace Tests\Feature;

use App\Models\Club;
use App\Models\GameLevel;
use App\Models\GameStatus;
use App\Models\Playground;
use App\Models\PlaygroundSchedule;
use App\Models\Sport;
use App\Models\User;
use Tests\TestCase;

class PlaygroundScheduleTest extends TestCase
{

    /** @var Club $club */
    protected $club;

    /** @var Playground $playground */
    protected $playground;

    /**
     * @throws \Exception
     */
    public function test()
    {
        //TODO: check
        $this->assertTrue(true);
        /*
        $user = User::query()->first();
        $this->actingAs($user, 'api');
        $club = $this->createClub();
        $playground = $this->createPlayground($club);
        $this->scheduling($playground);
        */
    }

    /**
     * @return Club
     */
    public function createClub()
    {
        /** @var Club $club */
        $club = factory(Club::class)->make();
        $club->save();
        $this->assertTrue($club->id > 0, 'Club not created');
        return $club;
    }

    /**
     * @param Club $club
     * @return Playground
     * @throws \Exception
     */
    public function createPlayground(Club $club)
    {
        /** @var Playground $playground */
        $playground = factory(Playground::class)->make();
        $playground->club()->associate($club);
        $playground->save();
        $this->assertTrue($playground->id > 0, 'Playground not created');

        $response = $this->json('GET', "/api/playgrounds/$playground->id", [], [
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
        ]);
        $response->assertStatus(200);
        $id = $response->decodeResponseJson('data.id');

        $this->assertTrue($id === $playground->id, "Wrong api response: expect [$playground->id], got [$id]");

        return $playground;
    }


    /**
     * A basic test example.
     *
     * @param Playground $playground
     * @return void
     * @throws \Exception
     */
    public function scheduling(Playground $playground)
    {
        // get all playground schedules
        $response = $this->json('GET', "/api/playground-schedules/", [], [
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
        ]);
        $response->assertStatus(422);
        $error = $response->decodeResponseJson('errors.playground_id');
        $this->assertNotEmpty($error, 'Expect validation error');

        // create empty playground schedule
        $response = $this->json('POST', '/api/playground-schedules', [
            [
                'playground_id' => null,
            ]
        ], [
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
        ]);

        $response->assertStatus(422);
        $error = $response->decodeResponseJson('errors.playground_id');
        $this->assertNotEmpty($error, 'Expect validation error');
        $error = $response->decodeResponseJson('errors.state');
        $this->assertNotEmpty($error, 'Expect validation error');
        $error = $response->decodeResponseJson('errors.type');
        $this->assertNotEmpty($error, 'Expect validation error');
        $error = $response->decodeResponseJson('errors.interval');
        $this->assertNotEmpty($error, 'Expect validation error');

        // create playground schedule with invalid state
        $response = $this->json('POST', '/api/playground-schedules', [
            'playground_id' => $playground->id,
            'state' => PlaygroundSchedule::STATE_CLOSED,
            'type' => PlaygroundSchedule::TYPE_ANY,
        ], [
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
        ]);
        $response->assertStatus(422);
        $error = $response->decodeResponseJson('errors.state');
        $this->assertNotEmpty($error, 'Expect validation error');

        // create valid playground schedule
        $response = $this->json('POST', '/api/playground-schedules', [
            'playground_id' => $playground->id,
            'state' => PlaygroundSchedule::STATE_OPEN,
            'type' => PlaygroundSchedule::TYPE_ANY,
            'duration' => [
                '01 Oct 2018 12:00:00',
                '26 Oct 2018 12:00:00',
            ],
            'interval' => [
                '15:00',
                '16:00',
            ],
            'price' => 2.13,
        ], [
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
        ]);
        $response->assertStatus(201);

        // create valid playground schedule
        $response = $this->json('POST', '/api/games', [
            'playground_id' => $playground->id,
            'name' => 'test game 1',
            'description' => 'test game description 1',
            'min_players' => 1,
            'max_players' => 10,
            'sport_id' => Sport::query()->first()->id,
            'game_status_id' => GameStatus::query()->first()->id,
            'game_level_id' => GameLevel::query()->first()->id,
            'duration' => [
                '26 Oct 2018 12:00:00',
                '26 Oct 2018 13:00:00',
            ],
        ], [
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
        ]);
//        dd($response->decodeResponseJson());
        $response->assertStatus(201);

        // create valid playground schedule
        $response = $this->json('POST', '/api/games', [
            'playground_id' => $playground->id,
            'name' => 'test game 1',
            'description' => 'test game description 1',
            'min_players' => 1,
            'max_players' => 10,
            'sport_id' => Sport::query()->first()->id,
            'game_status_id' => GameStatus::query()->first()->id,
            'game_level_id' => GameLevel::query()->first()->id,
            'duration' => [
                '26 Oct 2018 12:00:00',
                '26 Oct 2018 13:00:00',
            ],
        ], [
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
        ]);
//                dd($response->getContent());
        $response->assertStatus(400);
    }
}
