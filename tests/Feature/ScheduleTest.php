<?php

namespace Tests\Feature;

use DateTime;

use App\Models\Club;
use App\Models\Playground;
use App\Services\ScheduleService;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ScheduleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCreating()
    {
        $club = $this->createClub();
        $playground = $this->createPlayground($club);
        $schedules = $this->createSchedules($playground);

        //$this->assertTrue(true);
    }

    public function createClub()
    {
        $club = factory(Club::class)->create();
        $this->assertTrue($club->id > 0, 'Club not created');
        return $club;
    }

    public function createPlayground(Club $club)
    {
        $playground = factory(Playground::class)->make();
        $playground->club()->associate($club);
        $playground->save();
        $this->assertTrue($playground->id > 0, 'Playground not created');
        return $playground;
    }

    public function createSchedules(Playground $playground)
    {
        $date = (new DateTime())->modify('+2 days')->format('Y-m-d');
        $data = [
            [
                'interval' => ['9:00', '10:00'],
                'date' => $date,
                'price' => '100'
            ],
            [
                'interval' => ['10:00', '11:00'],
                'date' => $date,
                'price' => '200'
            ],
            [
                'interval' => ['11:00', '16:00'],
                'date' => $date,
                'price' => '300'
            ],
            [
                'interval' => ['9:30', '11:30'],
                'date' => $date,
                'price' => '400'
            ]
        ];

        $schedules = array_map(function ($schedule) use ($playground) {
            $newSchedule = ScheduleService::create($playground, 'open', $schedule['date'], $schedule['interval'], $schedule['price']);
            $this->assertTrue($newSchedule->id > 0, 'Schedule not created');
        }, $data);

        $this->assertDatabaseHas('playground_schedules', [
                'date' => $date,
                'interval' => "['9:00:00+03', '9:30:00+03')",
                'price' => '100'
        ]);

        $this->assertDatabaseHas('playground_schedules', [
            'date' => $date,
            'interval' => "['9:30:00+03', '11:30:00+03')",
            'price' => '400'
        ]);

        $this->assertDatabaseHas('playground_schedules', [
            'date' => $date,
            'interval' => "['11:30:00+03', '16:00:00+03')",
            'price' => '300'
        ]);

        return $schedules;
    }
}
