<?php

use Illuminate\Database\Seeder;

class GameStatusesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statuses = [
            'new',
        ];

        foreach ($statuses as $status){
            \App\Models\GameStatus::create(['name' => $status]);
        }
    }
}
