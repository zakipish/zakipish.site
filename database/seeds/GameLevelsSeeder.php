<?php

use Illuminate\Database\Seeder;

class GameLevelsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $gameLevels = [
            [
                'name' => 'Новичок',
                'engName' => 'junior',
                'color' => '27AE60'
            ],
            [
                'name' => 'Любитель',
                'engName' => 'nopro',
                'color' => 'D3EF30'
            ],
            [
                'name' => 'Средний',
                'engName' => 'middle',
                'color' => 'F68623'
            ],
            [
                'name' => 'Профи',
                'engName' => 'pro',
                'color' => 'EB5757'
            ],
            [
                'name' => 'Мастер',
                'engName' => 'expert',
                'color' => '9B51E0'
            ]
        ];

        foreach ($gameLevels as $level){
            \App\Models\GameLevel::create($level);
        }
    }
}
