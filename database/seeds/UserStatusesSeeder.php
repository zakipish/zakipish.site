<?php

use Illuminate\Database\Seeder;

class UserStatusesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statuses = [
            'new',
        ];

        foreach ($statuses as $status){
            \App\Models\PlayerStatus::create(['name' => $status]);
        }
    }
}
