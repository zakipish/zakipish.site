<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CitiesSeeder::class);
        $this->call(CreateContactTypes::class);
        $this->call(InfraSeeder::class);
        $this->call(SurfacesSeeder::class);
    }
}
