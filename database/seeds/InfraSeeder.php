<?php

use Illuminate\Database\Seeder;

class InfraSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            'Парковка',
            'Инвентарь',
            'Душ',
            'Кафе',
        ];

        foreach ($items as $item){
            \App\Models\Infrastructure::create(['name' => $item]);
        }
    }
}
