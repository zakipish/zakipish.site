<?php

use Illuminate\Database\Seeder;

class CreateContactTypes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = [
            'Телефон',
            'E-mail',
            'Сайт',
            'Вконтакте',
            'Facebook',
            'Прочее'
        ];

        foreach ($types as $type){
            \App\Models\ContactType::create(['name' => $type]);
        }
    }
}
