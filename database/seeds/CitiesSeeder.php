<?php

use Illuminate\Database\Seeder;

class CitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cities = [
            'Москва',
            'Санкт-Петербург',
            'Новосибирск',
            'Екатеринбург',
            'Нижний Новгород',
            'Казань',
            'Челябинск',
            'Самара',
            'Омск',
            'Ростов-на-Дону',
        ];

        foreach ($cities as $city){
            \App\Models\City::create(['name' => $city]);
        }
    }
}
