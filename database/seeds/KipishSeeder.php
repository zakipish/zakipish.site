<?php

use App\Models\Kipish;
use App\Models\User;
use App\Models\Club;
use App\Models\Playground;
use App\Models\Sport;
use App\Models\GameLevel;

use Illuminate\Database\Seeder;

class KipishSeeder extends Seeder
{
    public function run()
    {
        $kipishes = [
            [
                'owner_id' => User::first()->id,
                'club_id' => Club::first()->id,
                'playground_id' => Playground::first()->id,
                'sport_id' => Sport::first()->id,
                'chat_disabled' => false,
                'type' => Kipish::TYPE_GAME,
                'title' => 'Какой-то кипиш (в зале, игра)',
                'description' => 'Кипиш-игра в первом клубе, первом зале, первого юзера, с первым спортом, с первым уровнем игры, чат включен, 1000р, 10-30 игроков',
                'price' => 1000,
                'game_level_id' => GameLevel::first()->id,
                'min_players' => 10,
                'max_players' => 30,
                'blockSchedule' => true,
                'date' => (new DateTime())->modify('+3 days')->format('Y-m-d'),
                'interval' => ['9:00', '10:00'],
            ],
            [
                'owner_id' => User::first()->id,
                'club_id' => Club::first()->id,
                'sport_id' => Sport::first()->id,
                'chat_disabled' => true,
                'type' => Kipish::TYPE_MEETING,
                'title' => 'Выездной кипиш (в зале, встреча)',
                'description' => 'Кипиш-игра в первом клубе, в парке горького, первого юзера, с первым спортом, с первым уровнем игры, чат выключен, 1200р, 8-20 игроков',
                'coordinates' => '(55.727575, 37.601473)',
                'price' => 1200,
                'game_level_id' => GameLevel::first()->id,
                'min_players' => 8,
                'max_players' => 20,
                'blockSchedule' => false,
                'date' => (new DateTime())->modify('+3 days')->format('Y-m-d'),
                'interval' => ['10:00', '11:00'],
            ],
        ];

        foreach ($kipishes as $kipish){
            Kipish::create($kipish);
        }
    }
}
