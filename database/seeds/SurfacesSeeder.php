<?php

use Illuminate\Database\Seeder;

class SurfacesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = [
            'Грунт',
            'Трава',
        ];

        foreach ($types as $type){
            \App\Models\Surface::create(['name' => $type]);
        }
    }
}
