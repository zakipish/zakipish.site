<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Playground::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->name,
    ];
});
