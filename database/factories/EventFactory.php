<?php

use App\Models\Club;
use App\Models\Playground;
use App\Models\User;
use App\Models\Event;
use App\Models\GameLevel;

use Faker\Generator as Faker;

$factory->define(Event::class, function (Faker $faker) use ($factory) {
    $club = $factory->create(Club::class);
    $playground = $factory->create(Playground::class, ['club_id' => $club->id]);
    return [
        'title' => $faker->unique()->sentence(5),
        'description' => $faker->text(),
        'owner_id' => $factory->create(User::class, [ 'role' => User::ROLE_MODERATOR ])->id,
        'club_id' => $club->id,
        'playground_id' => $playground->id,
        'game_level_id' => $factory->create(GameLevel::class)->id,
        'max_players' => 10,
    ];
});
