<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SetKipishGameLevelIdNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('kipishes', function (Blueprint $table) {
            $table->unsignedInteger('game_level_id')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('kipishes', function (Blueprint $table) {
            $table->unsignedInteger('game_level_id')->nullable(false)->change();
        });
    }
}
