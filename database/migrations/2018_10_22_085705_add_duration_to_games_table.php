<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDurationToGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE EXTENSION btree_gist;
        ");
        DB::statement("
            ALTER TABLE games ADD COLUMN duration tstzrange;
        ");
        DB::statement("
            ALTER TABLE games ADD CONSTRAINT games_duration_excl EXCLUDE USING GIST (duration WITH &&, playground_id WITH =);
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("
            ALTER TABLE games DROP COLUMN duration;
        ");
        DB::statement("
            ALTER TABLE games DROP CONSTRAINT games_duration_excl;
        ");
    }
}
