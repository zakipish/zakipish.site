<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MinIntervalDefault30 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        DB::statement("
            ALTER TABLE playgrounds ALTER COLUMN min_interval SET DEFAULT 60;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("
            ALTER TABLE playgrounds ALTER COLUMN min_interval DROP DEFAULT;
        ");
    }
}
