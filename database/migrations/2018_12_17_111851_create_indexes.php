<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIndexes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('booking_types', function (Blueprint $table) {
            $table->index('club_id');
        });

        Schema::table('chat_messages', function (Blueprint $table) {
            $table->index('game_id');
        });

        Schema::table('club_chains', function (Blueprint $table) {
            $table->index('owner_id');
        });

        Schema::table('club_contact', function (Blueprint $table) {
            $table->index('club_id');
        });

        Schema::table('club_image', function (Blueprint $table) {
            $table->index('club_id');
        });

        Schema::table('club_infrastructure', function (Blueprint $table) {
            $table->index('club_id');
        });

        Schema::table('club_service', function (Blueprint $table) {
            $table->index('club_id');
        });

        Schema::table('club_sport', function (Blueprint $table) {
            $table->index('club_id');
        });

        Schema::table('club_user', function (Blueprint $table) {
            $table->index('club_id');
            $table->index('user_id');
        });

        Schema::table('clubs', function (Blueprint $table) {
            $table->index('owner_id');
            $table->index('club_chain_id');
            $table->index('parent_club_id');
        });

        Schema::table('game_user', function (Blueprint $table) {
            $table->index('game_id');
            $table->index('user_id');
        });

        Schema::table('games', function (Blueprint $table) {
            $table->index('playground_id');
            $table->index('sport_id');

            $table->index('date');
            DB::statement("
                CREATE INDEX games_interval_index ON playground_schedules USING btree (interval);
            ");
        });

        Schema::table('playground_equipment', function (Blueprint $table) {
            $table->index('playground_id');
        });

        Schema::table('playground_image', function (Blueprint $table) {
            $table->index('playground_id');
        });

        Schema::table('playground_playground_type', function (Blueprint $table) {
            $table->index('playground_id');
        });

        Schema::table('playground_schedules', function (Blueprint $table) {
            $table->index('playground_id');

            $table->index('date');
            DB::statement("
                CREATE INDEX playground_schedules_interval_index ON playground_schedules USING btree (interval);
            ");
        });

        Schema::table('playground_service', function (Blueprint $table) {
            $table->index('playground_id');
        });

        Schema::table('playground_sport', function (Blueprint $table) {
            $table->index('playground_id');
        });

        Schema::table('playground_surface', function (Blueprint $table) {
            $table->index('playground_id');
        });

        Schema::table('social_accounts', function (Blueprint $table) {
            $table->index('user_id');
        });

        Schema::table('sport_equipment', function (Blueprint $table) {
            $table->index('sport_id');
        });

        Schema::table('sport_user', function (Blueprint $table) {
            $table->index('user_id');
        });

        Schema::table('user_avatars', function (Blueprint $table) {
            $table->index('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('booking_types', function (Blueprint $table) {
            $table->dropIndex('booking_types_club_id_index');
        });

        Schema::table('chat_messages', function (Blueprint $table) {
            $table->dropIndex('chat_messages_game_id_index');
        });

        Schema::table('club_chains', function (Blueprint $table) {
            $table->dropIndex('club_chains_owner_id_index');
        });

        Schema::table('club_contact', function (Blueprint $table) {
            $table->dropIndex('club_contact_club_id_index');
        });

        Schema::table('club_image', function (Blueprint $table) {
            $table->dropIndex('club_image_club_id_index');
        });

        Schema::table('club_infrastructure', function (Blueprint $table) {
            $table->dropIndex('club_infrastructure_club_id_index');
        });

        Schema::table('club_service', function (Blueprint $table) {
            $table->dropIndex('club_service_club_id_index');
        });

        Schema::table('club_sport', function (Blueprint $table) {
            $table->dropIndex('club_sport_club_id_index');
        });

        Schema::table('club_user', function (Blueprint $table) {
            $table->dropIndex('club_user_club_id_index');
            $table->dropIndex('club_user_user_id_index');
        });

        Schema::table('clubs', function (Blueprint $table) {
            $table->dropIndex('clubs_owner_id_index');
            $table->dropIndex('clubs_club_chain_id_index');
            $table->dropIndex('clubs_parent_club_id_index');
        });

        Schema::table('game_user', function (Blueprint $table) {
            $table->dropIndex('game_user_game_id_index');
            $table->dropIndex('game_user_user_id_index');
        });

        Schema::table('games', function (Blueprint $table) {
            $table->dropIndex('games_playground_id_index');
            $table->dropIndex('games_sport_id_index');
            $table->dropIndex('games_date_index');
            $table->dropIndex('games_interval_index');
        });

        Schema::table('playground_equipment', function (Blueprint $table) {
            $table->dropIndex('playground_equipment_playground_id_index');
        });

        Schema::table('playground_image', function (Blueprint $table) {
            $table->dropIndex('playground_image_playground_id_index');
        });

        Schema::table('playground_playground_type', function (Blueprint $table) {
            $table->dropIndex('playground_playground_type_playground_id_index');
        });

        Schema::table('playground_schedules', function (Blueprint $table) {
            $table->dropIndex('playground_schedules_playground_id_index');
            $table->dropIndex('playground_schedules_date_index');
            $table->dropIndex('playground_schedules_interval_index');
        });

        Schema::table('playground_service', function (Blueprint $table) {
            $table->dropIndex('playground_service_playground_id_index');
        });

        Schema::table('playground_sport', function (Blueprint $table) {
            $table->dropIndex('playground_sport_playground_id_index');
        });

        Schema::table('playground_surface', function (Blueprint $table) {
            $table->dropIndex('playground_surface_playground_id_index');
        });

        Schema::table('social_accounts', function (Blueprint $table) {
            $table->dropIndex('social_accounts_user_id_index');
        });

        Schema::table('sport_equipment', function (Blueprint $table) {
            $table->dropIndex('sport_equipment_sport_id_index');
        });

        Schema::table('sport_user', function (Blueprint $table) {
            $table->dropIndex('sport_user_user_id_index');
        });

        Schema::table('user_avatars', function (Blueprint $table) {
            $table->dropIndex('user_avatars_user_id_index');
        });
    }
}
