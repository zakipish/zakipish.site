<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSocialAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('social_accounts', function (Blueprint $table) {
            $table->increments('id');

            $table->string('provider')->nullable();
            $table->bigInteger('provider_id')->nullable();
            $table->string('social_email')->nullable();
            $table->unsignedInteger('user_id');

            $table->string('token')->nullable();

            $table->timestamps();


            $table->index('provider', 'social_accounts_provider_index');
            $table->index(['provider', 'provider_id'], 'social_accounts_provider_provider_id_index');
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->unique(['provider', 'provider_id'], 'social_accounts_provider_provider_id_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('social_accounts');
    }
}
