<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToGameLevels extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('game_levels', function(Blueprint $table) {
            $table->string('color')->nullable();
            $table->string('engName')->unique()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('game_levels', function(Blueprint $table) {
            $table->dropColumn('color', 6);
            $table->dropColumn('engName');
        });
    }
}
