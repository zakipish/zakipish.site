<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameKipishImageToEventImage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('kipish_image', 'event_image');
        Schema::table('event_image', function (Blueprint $table) {
            $table->renameColumn('kipish_id', 'event_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::rename('event_image', 'kipish_image');
        Schema::table('kipish_image', function (Blueprint $table) {
            $table->renameColumn('event_id', 'kipish_id');
        });
    }
}
