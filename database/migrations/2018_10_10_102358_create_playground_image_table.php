<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlaygroundImageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('playground_image', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('playground_id');
            $table->unsignedInteger('image_id');

            $table->integer('order');


            $table->timestamps();

            $table->foreign('playground_id')
                ->references('id')
                ->on('playgrounds')
                ->onDelete('cascade');

            $table->foreign('image_id')
                ->references('id')
                ->on('images')
                ->onDelete('cascade');
        });
        DB::statement('ALTER TABLE playground_image ADD COLUMN main bool CONSTRAINT main_true_or_null CHECK (main), ADD CONSTRAINT playground_id_main_uniquie UNIQUE (playground_id, main);');

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('playground_image');
    }
}
