<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class RenameImageIdInClubsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
//        Schema::table('clubs', function (Blueprint $table) {
//            $table->renameColumn('image_id', 'logo_id');
//        });
        DB::statement('ALTER TABLE clubs RENAME COLUMN image_id TO logo_id;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('ALTER TABLE clubs RENAME COLUMN logo_id TO image_id;');
    }
}
