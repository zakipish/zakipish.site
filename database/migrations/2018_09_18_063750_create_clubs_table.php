<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClubsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clubs', function (Blueprint $table) {
            $table->increments('id');
            $table->text('name');
            $table->text('description')->nullable();

            $table->text('country')->nullable();
            $table->text('region')->nullable();
            $table->text('city')->nullable();
            $table->text('department')->nullable();

            $table->text('source_link')->nullable();

            $table->enum('rank', [
                \App\Models\Club::RANK_1,
                \App\Models\Club::RANK_2,
                \App\Models\Club::RANK_3,
                \App\Models\Club::RANK_4
            ])->nullable();

            $table->unsignedInteger('owner_id')->nullable();
            $table->timestamps();

            $table->foreign('owner_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
        DB::statement("ALTER TABLE clubs ADD COLUMN links text[], ADD COLUMN coordinates point, ADD COLUMN metros text[]");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clubs');
    }
}
