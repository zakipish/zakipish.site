<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClubSourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('club_sources', function (Blueprint $table) {
            $table->increments('id');

            $table->string('remote_type');
            $table->unsignedInteger('remote_id');

            $table->unsignedInteger('club_id')->nullable();
            $table->foreign('club_id')
                ->references('id')
                ->on('clubs')
                ->onDelete('cascade');

            $table->index(['remote_type', 'remote_id'], 'club_sources_remote_index');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('club_sources');
    }
}
