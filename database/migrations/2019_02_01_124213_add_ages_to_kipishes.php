<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAgesToKipishes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('kipishes', function (Blueprint $table) {
            $table->unsignedInteger('age_start')->nullable();
            $table->unsignedInteger('age_end')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('kipishes', function (Blueprint $table) {
            $table->dropColumn('age_start');
            $table->dropColumn('age_end');
        });
    }
}
