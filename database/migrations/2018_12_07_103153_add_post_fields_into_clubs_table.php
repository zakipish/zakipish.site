<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPostFieldsIntoClubsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clubs', function (Blueprint $table) {
            //Доьбавляем почтовые поля
            $table->string('post_index')->nullable();
            $table->string('post_street')->nullable();
            $table->string('post_building')->nullable();
            $table->string('post_building_number')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clubs', function (Blueprint $table) {
            $table->dropColumn('post_index');
            $table->dropColumn('post_street');
            $table->dropColumn('post_building');
            $table->dropColumn('post_building_number');
        });
    }
}
