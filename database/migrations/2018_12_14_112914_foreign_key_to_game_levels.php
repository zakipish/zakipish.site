<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ForeignKeyToGameLevels extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_sports', function (Blueprint $table) {
            $table->renameColumn('game_level', 'game_level_id');
            $table->foreign('game_level_id')
                  ->references('id')->on('game_levels')
                  ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_sports', function (Blueprint $table) {
            $table->dropForeign('user_sports_game_level_id_foreign');
            $table->renameColumn('game_level_id', 'game_level');
        });
    }
}
