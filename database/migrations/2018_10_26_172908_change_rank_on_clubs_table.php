<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeRankOnClubsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            ALTER TABLE clubs DROP CONSTRAINT clubs_rank_check; 
        ");
        DB::statement("
            ALTER TABLE clubs ADD CONSTRAINT clubs_rank_check CHECK (rank::text = ANY (ARRAY['0'::character varying, '1'::character varying, '2'::character varying, '3'::character varying, '4'::character varying]::text[])); 
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("
            ALTER TABLE clubs DROP CONSTRAINT clubs_rank_check; 
        ");
        DB::statement("
            ALTER TABLE clubs ADD CONSTRAINT clubs_rank_check CHECK (rank::text = ANY (ARRAY['1'::character varying, '2'::character varying, '3'::character varying, '4'::character varying]::text[])); 
        ");
    }
}
