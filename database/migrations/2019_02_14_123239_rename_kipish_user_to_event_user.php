<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameKipishUserToEventUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('kipish_user', 'event_user');
        Schema::table('event_user', function (Blueprint $table) {
            $table->renameColumn('kipish_id', 'event_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::rename('event_user', 'kipish_user');
        Schema::table('kipish_user', function (Blueprint $table) {
            $table->renameColumn('event_id', 'kipish_id');
        });
    }
}
