<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKipishImages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kipish_image', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('kipish_id')->index();
            $table->foreign('kipish_id')
                ->references('id')
                ->on('kipishes')
                ->onDelete('cascade');

            $table->unsignedInteger('image_id');
            $table->foreign('image_id')
                ->references('id')
                ->on('images')
                ->onDelete('cascade');

            $table->integer('order');
            $table->timestamps();

            $table->unique(['kipish_id', 'image_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kipish_image');
    }
}
