<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIntervalAndDateToKipishes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('kipishes', function(Blueprint $table) {
            $table->date('date')->nullable();
        });

        DB::statement("
            ALTER TABLE kipishes ADD COLUMN interval timetzrange;
        ");
        DB::statement("
            ALTER TABLE kipishes ADD CONSTRAINT kipishes_interval_excl EXCLUDE USING GIST (interval WITH &&, playground_id WITH =, date WITH =) WHERE (playground_id IS NOT NULL);
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('kipishes', function(Blueprint $table) {
            $table->dropColumn('date');
        });

        DB::statement("
            ALTER TABLE kipishes DROP COLUMN interval;
        ");
    }
}
