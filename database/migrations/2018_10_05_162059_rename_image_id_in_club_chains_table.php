<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class RenameImageIdInClubChainsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
//        Schema::table('clubs', function (Blueprint $table) {
//            $table->renameColumn('image_id', 'logo_id');
//        });
        DB::statement('ALTER TABLE club_chains RENAME COLUMN image_id TO logo_id;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('ALTER TABLE club_chains RENAME COLUMN logo_id TO image_id;');
    }
}
