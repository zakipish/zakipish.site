<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('addresser_id');
            $table->unsignedInteger('addressee_id');
            $table->text('message')->nullable();
            $table->text('uri')->nullable();
            $table->enum('type', \App\Models\Message::TYPES)->default(\App\Models\Message::TYPE_MESSAGE);
            $table->timestamp('sent_at')->nullable();
            $table->timestamps();

            $table->foreign('addresser_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->foreign('addressee_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messages');
    }
}
