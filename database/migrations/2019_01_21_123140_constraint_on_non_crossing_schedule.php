<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ConstraintOnNonCrossingSchedule extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            ALTER TABLE playground_schedules ADD CONSTRAINT playground_schedule_interval_excl EXCLUDE USING GIST (interval WITH &&, playground_id WITH =, date WITH =);
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("
            ALTER TABLE playground_schedules DROP CONSTRAINT playground_schedule_interval_excl;
        ");
    }
}
