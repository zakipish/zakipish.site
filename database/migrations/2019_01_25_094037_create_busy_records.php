<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusyRecords extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('busy_records', function(Blueprint $table) {
            $table->increments('id');
            $table->date('date');

            $table->unsignedInteger('busy_recordable_id');
            $table->string('busy_recordable_type');
            $table->index(['busy_recordable_id', 'busy_recordable_type']);

            $table->unique(['busy_recordable_id', 'busy_recordable_type']);

            $table->unsignedInteger('playground_id');

            $table->foreign('playground_id')
                ->references('id')
                ->on('playgrounds')
                ->onDelete('cascade');

            $table->timestamps();
        });

        DB::statement("
            ALTER TABLE busy_records ADD COLUMN interval timetzrange NOT NULL;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('busy_records');
    }
}
