<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeOnDeleteForSportIconId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sports', function (Blueprint $table) {
            $table->dropForeign('sports_icon_id_foreign');
            $table->foreign('icon_id')
            ->references('id')->on('images')
            ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sports', function (Blueprint $table) {
            $table->dropForeign('sports_icon_id_foreign');
            $table->foreign('icon_id')
            ->references('id')->on('images')
            ->onDelete('cascade');
        });
    }
}
