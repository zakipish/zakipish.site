<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCostAndUnitPlaygroundServiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('playground_service', function (Blueprint $table) {
            $table->decimal('price', 20, 2)->nullable(true)->change();
            $table->string('unit')->nullable(true)->change();
        });
        //
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('playground_service', function (Blueprint $table) {
            $table->decimal('price', 20, 2)->nullable(false)->change();
            $table->string('unit')->nullable(false)->change();

        });
    }
}
