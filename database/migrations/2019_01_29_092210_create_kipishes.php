<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKipishes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kipishes', function(Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('owner_id')->index();
            $table->foreign('owner_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->unsignedInteger('club_id')->index();
            $table->foreign('club_id')
                ->references('id')
                ->on('clubs')
                ->onDelete('cascade');

            $table->unsignedInteger('playground_id')->index()->nullable();
            $table->foreign('playground_id')
                ->references('id')
                ->on('playgrounds')
                ->onDelete('set null');

            $table->unsignedInteger('sport_id')->index()->nullable();
            $table->foreign('sport_id')
                ->references('id')
                ->on('sports')
                ->onDelete('set null');

            $table->boolean('chat_disabled')->default(false);

            $table->unsignedInteger('players_count');
            $table->string('title');
            $table->string('description');
            $table->unsignedInteger('price')->default(0);
            $table->unsignedInteger('type');

            $table->timestamps();
        });

        DB::statement("ALTER TABLE kipishes ADD COLUMN coordinates point;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kipishes');
    }
}
