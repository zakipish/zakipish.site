<?php

use App\Models\PlaygroundSchedule;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveOldSchedulesFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('playground_schedules', function (Blueprint $table) {
            $table->dropColumn('type');
            $table->dropColumn('state');
            $table->dropColumn('week');
            $table->dropColumn('duration');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // in next migration
    }
}
