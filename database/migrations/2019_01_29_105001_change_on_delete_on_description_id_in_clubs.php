<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeOnDeleteOnDescriptionIdInClubs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clubs', function (Blueprint $table) {
            $table->dropForeign('clubs_department_id_foreign');
            $table->foreign('department_id')
                ->references('id')
                ->on('departments')
                ->onDelete('SET NULL');
            $table->index('department_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clubs', function (Blueprint $table) {
            $table->dropIndex('clubs_department_id_index');

            $table->dropForeign('clubs_department_id_foreign');
            $table->foreign('department_id')
                ->references('id')
                ->on('departments')
                ->onDelete('CASCADE');
        });
    }
}
