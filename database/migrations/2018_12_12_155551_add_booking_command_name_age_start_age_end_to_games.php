<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBookingCommandNameAgeStartAgeEndToGames extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('games', function (Blueprint $table) {
            $table->boolean('booking')->default(false);
            $table->string('command_name')->nullable();
            $table->unsignedInteger('age_start')->nullable();
            $table->unsignedInteger('age_end')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('games', function (Blueprint $table) {
            $table->dropColumn('booking');
            $table->dropColumn('command_name');
            $table->dropColumn('age_start');
            $table->dropColumn('age_end');
        });
    }
}
