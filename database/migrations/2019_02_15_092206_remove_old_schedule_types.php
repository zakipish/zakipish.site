<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveOldScheduleTypes extends Migration
{
    const STATE_CLOSED = 'closed';
    const STATE_OPEN = 'open';

    const STATES = [
        self::STATE_CLOSED,
        self::STATE_OPEN,
    ];

    const TYPE_ANY = 'any';
    const TYPE_WEEK = 'week';
    const TYPE_DATE = 'date';

    const TYPES = [
        self::TYPE_ANY,
        self::TYPE_WEEK,
        self::TYPE_DATE,
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            DROP TYPE playground_schedule_states;
        ");

        DB::statement("
            DROP TYPE playground_schedule_types;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('playground_schedules', function (Blueprint $table) {
            $states = implode(',', array_map(function ($e) {return "'$e'";}, [
                self::STATE_CLOSED,
                self::STATE_OPEN,
            ]));
            $types = implode(',', array_map(function ($e) {return "'$e'";}, [
                self::TYPE_ANY,
                self::TYPE_WEEK,
                self::TYPE_DATE,
            ]));
            DB::statement("
                CREATE TYPE playground_schedule_states AS ENUM ($states);
            ");
            DB::statement("
                CREATE TYPE playground_schedule_types AS ENUM ($types);
            ");
            DB::statement("
                ALTER TABLE playground_schedules ADD COLUMN type playground_schedule_types;
            ");
            $stateClosed = self::STATE_CLOSED;
            $typeAny = self::TYPE_ANY;
            DB::statement("
                ALTER TABLE playground_schedules ADD COLUMN state playground_schedule_states
                CHECK (state != '$stateClosed' OR type != '$typeAny');
            ");
            DB::statement("
                ALTER TABLE playground_schedules ADD COLUMN duration tstzrange NOT NULL DEFAULT tstzrange('-infinity', 'infinity', '[)');
            ");
            $typeWeek = self::TYPE_WEEK;
            DB::statement("
                ALTER TABLE playground_schedules ADD COLUMN week smallint CHECK ((week IS NULL AND type != '$typeWeek') OR (type = '$typeWeek' AND week IS NOT NULL AND week >= 0 AND week < 7));
            ");
        });
    }
}
