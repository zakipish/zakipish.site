<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameClubColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            ALTER TABLE clubs RENAME COLUMN \"ORGN\" TO \"OGRN\";
        ");
        DB::statement("
            ALTER TABLE clubs RENAME COLUMN entity_adress TO entity_address;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("
            ALTER TABLE clubs RENAME COLUMN \"OGRN\" TO \"ORGN\";
        ");
        DB::statement("
            ALTER TABLE clubs RENAME COLUMN entity_address TO entity_adress;
        ");
    }
}
