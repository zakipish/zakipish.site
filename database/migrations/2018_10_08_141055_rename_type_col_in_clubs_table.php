<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameTypeColInClubsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clubs', function (Blueprint $table) {
            \Illuminate\Support\Facades\DB::statement('ALTER TABLE clubs RENAME "type" TO "type_old"');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clubs', function (Blueprint $table) {
            \Illuminate\Support\Facades\DB::statement('ALTER TABLE clubs RENAME "type_old" TO "type"');
        });
    }
}
