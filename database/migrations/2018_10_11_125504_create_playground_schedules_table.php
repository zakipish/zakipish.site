<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\PlaygroundSchedule;

class CreatePlaygroundSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('playground_schedules', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('playground_id');
            $table->decimal('price', 20, 2)->nullable();
            $table->timestamps();

            $table->foreign('playground_id')
                ->references('id')
                ->on('playgrounds')
                ->onDelete('cascade');
        });

        $states = implode(',', array_map(function ($e) {return "'$e'";}, [
            PlaygroundSchedule::STATE_CLOSED,
            PlaygroundSchedule::STATE_OPEN,
        ]));
        $types = implode(',', array_map(function ($e) {return "'$e'";}, [
            PlaygroundSchedule::TYPE_ANY,
            PlaygroundSchedule::TYPE_WEEK,
            PlaygroundSchedule::TYPE_DATE,
        ]));
        DB::statement("
            CREATE TYPE playground_schedule_states AS ENUM ($states);
        ");
        DB::statement("
            CREATE TYPE playground_schedule_types AS ENUM ($types);
        ");
        DB::statement("
            ALTER TABLE playground_schedules ADD COLUMN type playground_schedule_types;
        ");
        $stateClosed = PlaygroundSchedule::STATE_CLOSED;
        $typeAny = PlaygroundSchedule::TYPE_ANY;
        DB::statement("
            ALTER TABLE playground_schedules ADD COLUMN state playground_schedule_states
            CHECK (state != '$stateClosed' OR type != '$typeAny');
        ");
        DB::statement("
            ALTER TABLE playground_schedules ADD COLUMN duration tstzrange NOT NULL DEFAULT tstzrange('-infinity', 'infinity', '[)');
        ");
        DB::statement("
            CREATE TYPE timetzrange AS RANGE (
                subtype = timetz
            );
        ");
        DB::statement("
            ALTER TABLE playground_schedules ADD COLUMN interval timetzrange NOT NULL ;
        ");

        $typeWeek = PlaygroundSchedule::TYPE_WEEK;
        DB::statement("
            ALTER TABLE playground_schedules ADD COLUMN week smallint CHECK ((week IS NULL AND type != '$typeWeek') OR (type = '$typeWeek' AND week IS NOT NULL AND week >= 0 AND week < 7));
        ");
        $typeDate = PlaygroundSchedule::TYPE_DATE;
        DB::statement("
            ALTER TABLE playground_schedules ADD COLUMN \"date\" date CHECK ((\"date\" IS NULL AND type != '$typeDate') OR (\"date\" IS NOT NULL AND type = '$typeDate'));
        ");

        // Напоминалка
        // посмотреть тип: \dT[+S] playground_schedule_states
        // Изменить тип ALTER TYPE playground_schedule_states ADD VALUE 'vip';
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('playground_schedules');
        DB::statement("DROP TYPE IF EXISTS playground_schedule_states");
        DB::statement("DROP TYPE IF EXISTS playground_schedule_types");
        DB::statement("DROP TYPE IF EXISTS timetzrange");
    }
}
