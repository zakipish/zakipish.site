<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveGameIntervalExclConstraint extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            ALTER TABLE games DROP CONSTRAINT game_interval_excl;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("
            ALTER TABLE games ADD CONSTRAINT game_interval_excl EXCLUDE USING GIST (interval WITH &&, playground_id WITH =, date WITH =) WHERE (playground_id IS NOT NULL);
        ");
    }
}
