<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeOnDeleteForClubsLogoId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clubs', function (Blueprint $table) {
            $table->dropForeign('clubs_image_id_foreign');
            $table->foreign('logo_id')
            ->references('id')->on('images')
            ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clubs', function (Blueprint $table) {
            $table->dropForeign('clubs_logo_id_foreign');
            $table->foreign('logo_id', 'clubs_image_id_foreign')
            ->references('id')->on('images')
            ->onDelete('cascade');
        });
    }
}
