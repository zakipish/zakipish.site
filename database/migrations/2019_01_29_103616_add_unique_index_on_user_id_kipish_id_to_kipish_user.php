<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUniqueIndexOnUserIdKipishIdToKipishUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('kipish_user', function(Blueprint $table) {
            $table->unique(['kipish_id', 'user_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('kipish_user', function(Blueprint $table) {
            $table->dropUnique(['kipish_id', 'user_id']);
        });
    }
}
