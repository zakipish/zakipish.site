<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFinanceFieldsIntoClubsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clubs', function (Blueprint $table) {
            $table->string('entity')->nullable();
            $table->string('BIK')->nullable();
            $table->string('number_rs')->nullable();
            $table->string('NDS')->nullable();
            $table->string('company_name')->nullable();
            $table->string('INN')->nullable();
            $table->string('KPP')->nullable();
            $table->string('ORGN')->nullable();
            $table->string('entity_adress')->nullable();
            $table->string('registration_date')->nullable();
            $table->string('entity_boss_name')->nullable();
            $table->string('entity_boss_position')->nullable();
            $table->string('company_account')->nullable();
            $table->string('bank_name')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clubs', function (Blueprint $table) {
            $table->dropColumn('phone');
            $table->dropColumn('entity');
            $table->dropColumn('BIK');
            $table->dropColumn('number_rs');
            $table->dropColumn('NDS');
            $table->dropColumn('company_name');
            $table->dropColumn('INN');
            $table->dropColumn('KPP');
            $table->dropColumn('ORGN');
            $table->dropColumn('entity_adress');
            $table->dropColumn('registration_date');
            $table->dropColumn('entity_boss_name');
            $table->dropColumn('entity_boss_position');
            $table->dropColumn('company_account');
            $table->dropColumn('bank_name');
        });
    }
}
