<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveDurationFromGamesAndAddDateAndIntervalToGames extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('games', function (Blueprint $table) {
            $table->dropColumn('duration');
            $table->date('date')->nullable();

            DB::statement("
                ALTER TABLE games ADD COLUMN interval timetzrange;
            ");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('games', function (Blueprint $table) {
            DB::statement("
                ALTER TABLE games ADD COLUMN duration tstzrange NOT NULL DEFAULT tstzrange('-infinity', 'infinity', '[)');
            ");
            $table->dropColumn('date');
            $table->dropColumn('interval');
        });
    }
}
