<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewFieldsToKipishes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('kipishes', function (Blueprint $table) {
            $table->unsignedInteger('game_level_id');
            $table->foreign('game_level_id')
                  ->references('id')
                  ->on('game_levels')
                  ->onDelete('set null');
            $table->unsignedInteger('min_players')->nullable();
            $table->renameColumn('players_count', 'max_players');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('kipishes', function (Blueprint $table) {
            $table->dropForeign('kipishes_game_level_id_foreign');
            $table->dropColumn('game_level_id');
            $table->dropColumn('min_players');
            $table->renameColumn('max_players', 'players_count');
        });
    }
}
