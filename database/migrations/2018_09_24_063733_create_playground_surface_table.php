<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlaygroundSurfaceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('playground_surface', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('playground_id');
            $table->unsignedInteger('surface_id');
            $table->timestamps();

            $table->foreign('playground_id')
                ->references('id')
                ->on('playgrounds')
                ->onDelete('cascade');
            $table->foreign('surface_id')
                ->references('id')
                ->on('surfaces')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('playground_surface');
    }
}
