<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Laravel CORS
    |--------------------------------------------------------------------------
    |
    | allowedOrigins, allowedHeaders and allowedMethods can be set to array('*')
    | to accept any value.
    |
    */

    'supportsCredentials' => true,
    'allowedOrigins' => [
        'http://zakipish.local',
        'http://zakipish.local:8081',
        'http://localhost:8080',
        'http://localhost:8081',
        'http://localhost:8082',
        'http://front.zakipish.local:8081',
        'http://crm.zakipish.local:8082',
        'http://admin.dev.zakipish.ru',
        'http://admin.devclub.zakipish.ru',
        'http://crm.devclub.zakipish.ru',
        'http://admin.zakipish.local:8080',
        'http://devclub.zakipish.ru',
        'https://admin.devclub.zakipish.ru',
        'https://devclub.zakipish.ru',
        'https://crm.devclub.zakipish.ru',
        'https://admin.zakipish.ru',
        'https://zakipish.ru',
        'https://crm.zakipish.ru',
    ],
    'allowedOriginsPatterns' => [],
    'allowedHeaders' => ['*'],
    'allowedMethods' => ['OPTIONS', 'GET', 'POST', 'PATCH', 'PUT', 'DELETE'],
    'exposedHeaders' => [],
    'maxAge' => 0,

];
