<?php

return [
    'user'          => env('AMO_CRM_USER',''),
    'hash'          => env('AMO_CRM_HASH', ''),
    'subdomain'     => env('AMO_CRM_SUBDOMAIN', ''),
    'cookiefile'    => storage_path('app/amo/cookie.txt'),
    'sync_enable'   => env('AMO_CRM_ENABLE', false)
];