<?php

namespace App\Jobs;

use App\Models\Club;
use App\Models\Tasks\ClubAmoCrmSyncTask;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;

class SyncAmoClub implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $task;
    protected $amoService;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($taskId)
    {
        $this->task = ClubAmoCrmSyncTask::find($taskId);
        $this->amoService = app('AmoCrmService');
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Log::info('start task');

        if(empty($this->task))
            return;

        $task = $this->task;
        $task->update(['status' => ClubAmoCrmSyncTask::STATUS_PROGRESS]);

        $club = $task->club;

        if(!in_array($club->rank, [
            Club::RANK_1,
            Club::RANK_2,
            Club::RANK_3
        ])){
            $task->update(['status' => ClubAmoCrmSyncTask::STATUS_NON_NEEDED]);
            Log::info('sync not needed');
        }else{
            $result = $this->amoService->syncClub($club);

            if($result){
                $task->update(['status' => ClubAmoCrmSyncTask::STATUS_DONE]);
                Log::info('task finished');
                usleep(143000);
            }else{
                $task->update(['status' => ClubAmoCrmSyncTask::STATUS_ERROR]);
                Log::error('task error, retry...');
                usleep(143000);
                static::dispatch($task->id)->onQueue($task->getQueueName())->delay(now()->addMinutes(1));
            }
        }


    }
}
