<?php

namespace App\Exceptions;

use App\Models\Utils\SluggableInterface;
use RuntimeException;

class NewSlugFoundException extends RuntimeException
{
    /** @var SluggableInterface $model */
    protected $model;

    /** @var string $slug */
    protected $slug;

    /**
     * Set the affected Eloquent model and new slug.
     * @param SluggableInterface $model
     * @param string $slug
     * @return $this
     */
    public function setModel(SluggableInterface $model, string $slug)
    {
        $this->model = $model;
        $className = get_class($model);

        $this->message = "New slug is found for model [$className]: $slug";

        return $this;
    }

    /**
     * Get the affected Eloquent model.
     *
     * @return SluggableInterface
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Get the affected Eloquent model IDs.
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }
}
