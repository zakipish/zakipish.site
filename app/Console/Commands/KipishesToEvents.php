<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class KipishesToEvents extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'set:kipishes_to_events';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Upgrade all kipishes morph-relations to events';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::statement("
            UPDATE busy_records SET busy_recordable_type = 'events' WHERE busy_recordable_type = 'kipishes';
        ");
        DB::statement("
            UPDATE chats SET chatable_type = 'events' WHERE chatable_type = 'kipishes';
        ");
    }
}
