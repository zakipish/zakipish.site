<?php

namespace App\Console\Commands;

use App\Models\Club;
use App\Models\Department;
use Illuminate\Console\Command;

class AddDepartment_id_into_clubs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'add:department_id';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Adding department_id field according to department in club';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Получаем список клубов
        $clubs = Club::all();
        foreach ($clubs as $club) {
            if ($club->department) {
                $department_id = Department::where('name','=',$club->department)->first()->id;
                $club->department_id = $department_id;
                $club->save();
            }
        }
    }
}
