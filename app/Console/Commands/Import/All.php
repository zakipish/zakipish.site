<?php

namespace App\Console\Commands\Import;

use App\Helpers\CityHelper;
use App\Helpers\ContactHelper;
use App\Models\Club;
use App\Models\ClubSource;
use App\Models\Contact;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class All extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:all';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    protected $phone_id;
    protected $email_id;
    protected $address_id;
    protected $site_id;
    protected $vk_id;
    protected $fb_id;

    protected $spb_id;

    protected $uploadService;

    public function __construct()
    {
        parent::__construct();
        $this->phone_id     = ContactHelper::getContactTypeIdByTitle(ContactHelper::TYPE_PHONE);
        $this->email_id     = ContactHelper::getContactTypeIdByTitle(ContactHelper::TYPE_EMAIL);
        $this->address_id   = ContactHelper::getContactTypeIdByTitle(ContactHelper::TYPE_ADDRESS);
        $this->site_id      = ContactHelper::getContactTypeIdByTitle(ContactHelper::TYPE_SITE);
        $this->vk_id        = ContactHelper::getContactTypeIdByTitle(ContactHelper::TYPE_VK);
        $this->fb_id        = ContactHelper::getContactTypeIdByTitle(ContactHelper::TYPE_FB);

        $this->spb_id       = CityHelper::getCityIdByTitle(CityHelper::SPB_NAME);

        $this->uploadService = app('UploadService');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('start');
        try{
            $filePath = resource_path('imports/all.xlsx');
            Excel::load($filePath, function($reader){
                $reader->each(function ($sheet, $i){
                    if($i == 0){
                        DB::beginTransaction();

                        $data = $sheet->all();
                        $this->info('parse clubs');
                        $bar = $this->output->createProgressBar(sizeof($data));
                        foreach ($data as $item){
                            try{
                                $source = $this->getSource($item[0], $item[1]);
                                list ($club, $contacts) = $this->parseItem($item);
                                $club->save();
                                $club->contacts()->saveMany($contacts);
                                $source->club_id = $club->id;
                                $source->save();
                            }catch (\Exception $e){
                                DB::rollBack();
                                $this->error($e->getTraceAsString());
                                dd($item);
                            }

                            $bar->advance();
                        }
                        DB::commit();
                        $bar->finish();
                        $this->info(PHP_EOL.PHP_EOL.'Finish parse items'.PHP_EOL.PHP_EOL);
                    }elseif($i == 1){
                        $data = $sheet->all();
                        $this->info('start parse photos');
                        $bar = $this->output->createProgressBar(sizeof($data));
                        foreach($data as $item){

                            try{
                                $source = ClubSource::where('remote_type', $item[1])
                                    ->where('remote_id', $item[0])
                                    ->first();
                                if(empty($source) || empty($source->club)){
                                    $this->info('empty source');
                                    continue;
                                }

                                $file = $this->uploadService->prepareRemoteImage($item[2]);
                                $image = $this->uploadService->uploadImage($file);
                                $club = $source->club;
                                $order = $club->images()->count();
                                $club->images()->attach($image->id, ['order' => $order]);
                                $bar->advance();
                            }catch (\Exception $e){
                                $this->error($e->getMessage());
                                $this->error($item[0] . ' ' . $item[1] . ' ' . $item[2]);
                            }

                        }

                        $bar->finish();
                    }
                });
            });

        }catch (\Exception $e){
            $this->error($e->getMessage());
            $this->error($e->getTraceAsString());
        }

    }

    private function parseItem($data)
    {
        $rankData = preg_replace('/\D/', '', $data[2]);
        if(empty($rankData))
            $rankData = Club::RANK_4;

        $name = $data[6];
        $desciption = $data[12];

        $club = new Club();
        $club->fill([
            'rank'          => $rankData,
            'name'          => trim($name),
            'description'   => trim($desciption),
            'city_id'       => $this->spb_id,
            'source_link'   => trim($data[3])
        ]);

        //PARSE COORDS
        if(!empty($data[4]) && !empty($data[5]))
            $club->coordinates = "(${data[4]},${data[5]})";

        $contacts = [];

        //PARSE ADDRESS
        if(!empty($this->address_id) && !empty($data[7])){
            $address = $this->generateContact($data[7], $this->address_id);
            $contacts[] = $address;
        }

        //PARSE PHONES
        if(!empty($this->phone_id) && !empty($data[8])) {
            $phones = mb_split(',', $data[8]);
            foreach ($phones as $phone){
                $_phone = $this->generateContact($phone, $this->phone_id);
                $contacts[] = $_phone;
            }
        }

        //PARSE_SITE
        if(!empty($this->site_id) && !empty($data[9])){
            $site = $this->generateContact($data[9], $this->site_id);
            $contacts[] = $site;
        }

        //PARSE_EMAIL
        if(!empty($this->email_id) && !empty($data[13])){
            $emails = mb_split(',', $data[13]);
            foreach ($emails as $email) {
                $_email = $this->generateContact($email, $this->email_id);
                $contacts[] = $_email;
            }
        }

        //PARSE VK
        if(!empty($this->vk_id) && !empty($data[14])){
            $vk = $this->generateContact($data[14], $this->vk_id);
            $contacts[] = $vk;
        }

        //PARSE FB
        if(!empty($this->fb_id) && !empty($data[15])){
            $fb = $this->generateContact($data[15], $this->fb_id);
            $contacts[] = $fb;
        }
        return [$club, $contacts];
    }

    private function generateContact($value, $type)
    {
        return new Contact([
            'value'             => trim($value),
            'contact_type_id'   => $type
        ]);
    }

    private function getSource($type, $id)
    {
        $source = new ClubSource();
        $source->fill([
            'remote_type'   => $type,
            'remote_id'     => (int)$id
        ]);
        return $source;
    }
}
