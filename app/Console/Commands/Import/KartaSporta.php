<?php

namespace App\Console\Commands\Import;

use App\Helpers\CityHelper;
use App\Helpers\ContactHelper;
use App\Models\Club;
use App\Models\ClubSource;
use App\Models\Contact;
use function GuzzleHttp\Psr7\str;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class KartaSporta extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:karta-sporta {path}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */

    protected $address_id;
    protected $phone_id;
    protected $email_id;
    protected $site_id;
    protected $spb_id;
    protected $msk_id;
    protected $uploadService;

    public function __construct()
    {
        parent::__construct();
        $this->address_id   = ContactHelper::getContactTypeIdByTitle(ContactHelper::TYPE_ADDRESS);
        $this->phone_id     = ContactHelper::getContactTypeIdByTitle(ContactHelper::TYPE_PHONE);
        $this->email_id     = ContactHelper::getContactTypeIdByTitle(ContactHelper::TYPE_EMAIL);
        $this->site_id      = ContactHelper::getContactTypeIdByTitle(ContactHelper::TYPE_SITE);
        $this->spb_id       = CityHelper::getCityIdByTitle(CityHelper::SPB_NAME);
        $this->msk_id       = CityHelper::getCityIdByTitle(CityHelper::MSK_NAME);
        $this->uploadService = app('UploadService');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('start');
        $path = $this->argument('path');

        DB::beginTransaction();
        try{
            $xml = new \SimpleXMLElement(file_get_contents($path));
            foreach ($xml->_club as $item){

                $club = new Club();
                $club->rank = Club::RANK_0;
                $club->source = 'KartaSporta';
                $club->name = str_replace('&amp', '&', (string)$item->_name);
                $club->source_link = str_replace('&amp', '&', (string)$item->_url);

                //PARSE DESCRIPTION
                $description = (string)$item->_description; //str_replace()
                $description = str_replace('[', '<', $description);
                $description = str_replace(']', '>', $description);
                $description = str_replace('|', '/', $description);
                $description = str_replace('&amp;', '&', $description);
                $club->description = $description;



                $contacts = [];

                //PARSE EMAIL
                $email = (string)$item->_email;
                if(!empty($email)){
                    $_email = $this->generateContact($email, $this->email_id);
                    $contacts[] = $_email;
                }

                //PARSE ADDRESS
                $address = (string)$item->_address;
                if(!empty($address)){
                    $_address = $this->generateContact($address, $this->address_id);
                    $contacts[] = $_address;

                    if(mb_strpos($address, 'Санкт-Петербург') !== false){
                        $club->city_id = $this->spb_id;
                    }elseif(mb_strpos($address, 'Москва') !== false){
                        $club->city_id = $this->msk_id;
                    }
                }


                //PARSE SITE
                $site = (string)$item->_site;
                if(!empty($site)){
                    $_site = $this->generateContact($site, $this->site_id);
                    $contacts[] = $_site;
                }

                //PARSE PHONES
                $phones = mbsplit(',', (string)$item->_phone);
                if(sizeof($phones)){
                    foreach ($phones as $phoneItem){
                        $_phone = $this->generateContact($phoneItem, $this->phone_id);
                        $contacts[] = $_phone;
                    }
                }
                $club->save();
                $club->contacts()->saveMany($contacts);

                $clubSource = new ClubSource([
                    'remote_type' => 'KartaSporta',
                    'remote_id'   => (int)$item->_id,
                    'club_id'     => $club->id
                ]);
                $clubSource->save();

                //SAVE PHOTOS
                $photoArr = get_object_vars($item->_photo);
                foreach ($photoArr as $k => $url){
                    try{
                        $file = $this->uploadService->prepareRemoteImage($url);
                        $image = $this->uploadService->uploadImage($file);
                        $order = $club->images()->count();
                        $club->images()->attach($image->id, ['order' => $order]);
                        $this->info('club ' . $club->id . ' add photo');
                    }catch (\Exception $e){
                        $this->error($e->getMessage());
                    }
                }

                $this->info('SUCCESS IMPORT CLUB ' . $club->id);
            }

            DB::commit();
            $this->info('success import file ' . $path);
        }catch (\Exception $e){
            $this->info($e->getMessage());
            DB::rollBack();
        }
    }

    private function generateContact($value, $type)
    {
        return new Contact([
            'value'             => trim($value),
            'contact_type_id'   => $type
        ]);
    }
}
