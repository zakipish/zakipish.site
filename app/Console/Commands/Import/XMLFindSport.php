<?php

namespace App\Console\Commands\Import;

use App\Events\ClubChanged;
use App\Helpers\CityHelper;
use App\Helpers\ContactHelper;
use App\Models\Club;
use App\Models\ClubSource;
use App\Models\Contact;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Orchestra\Parser\Xml\Facade as XmlParser;
use phpDocumentor\Reflection\DocBlock\Tags\Source;
use PHPUnit\Util\Xml;

class XMLFindSport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:find-sport {path}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */

    protected $address_id;
    protected $phone_id;
    protected $spb_id;
    protected $msk_id;
    protected $uploadService;

    public function __construct()
    {
        parent::__construct();
        $this->address_id   = ContactHelper::getContactTypeIdByTitle(ContactHelper::TYPE_ADDRESS);
        $this->phone_id     = ContactHelper::getContactTypeIdByTitle(ContactHelper::TYPE_PHONE);
        $this->spb_id       = CityHelper::getCityIdByTitle(CityHelper::SPB_NAME);
        $this->msk_id       = CityHelper::getCityIdByTitle(CityHelper::MSK_NAME);
        $this->uploadService = app('UploadService');

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('start');
        $path = $this->argument('path');

        try {
            $xml = new \SimpleXMLElement(file_get_contents($path));
            foreach ($xml->club as $item) {
                $club = new Club();

                $club->name = (string)$item->name;
                $club->description = (string)$item->description;
                $club->rank = Club::RANK_1;
                $club->source_link = (string)$item->url;
                $club->source = 'findsport';

                switch ((string)$item->city) {
                    case 'spb.':
                        $club->city_id = $this->spb_id;
                        break;

                    case 'msk.':
                        $club->city_id = $this->msk_id;
                        break;
                }

                $latHelp = mb_strlen((string)$item->latitude) - 2;
                $latHelp = pow(10, $latHelp);
                $lat = (int)$item->latitude / $latHelp;


                $longHelp = mb_strlen((string)$item->longitude) - 2;
                $longHelp = pow(10, $longHelp);
                $long = (int)$item->longitude / $longHelp;

                $club->coordinates = "(${lat},${long})";


                $contacts = [];
                //ADDRESS
                $address = $this->generateContact((string)$item->adress, $this->address_id);
                $contacts[] = $address;

                //PHONE
                $phones = get_object_vars($item->phone);
                foreach ($phones as $k => $_phone) {
                    $_p = $this->generateContact((string)$_phone, $this->phone_id);
                    $contacts[] = $_p;
                }
                $club->save();
                $club->contacts()->saveMany($contacts);
                $source = new ClubSource([
                    'remote_type' => 'findsport',
                    'remote_id' => (int)$item->id,
                    'club_id' => $club->id
                ]);
                $source->save();

                $photoArr = get_object_vars($item->photo);
                foreach ($photoArr as $k => $url) {
                    try{
                        $file = $this->uploadService->prepareRemoteImage($url);
                        $image = $this->uploadService->uploadImage($file);
                        $order = $club->images()->count();
                        $club->images()->attach($image->id, ['order' => $order]);
                    }catch(\Exception $e){
                        $this->error($e->getMessage());
                    }

                }
                event(new ClubChanged($club));

                $this->info('SUCCESS IMPORT');
            }
        }catch (\Exception $e){
            $this->error($e);
        }


    }

    private function generateContact($value, $type)
    {
        return new Contact([
            'value'             => trim($value),
            'contact_type_id'   => $type
        ]);
    }
}
