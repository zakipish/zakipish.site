<?php

namespace App\Console\Commands\Import;

use App\Models\ClubSource;
use Illuminate\Console\Command;

class HackSource extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:hack-source';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('start');
        ClubSource::orderBy('id')->chunk(1000, function($items){
            foreach($items as $item){
                $club = $item->club;
                if(empty($club->source)){
                    $club->update(['source' => $item->remote_type]);
                }
            }
        });
    }
}
