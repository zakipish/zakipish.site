<?php

namespace App\Console\Commands\Import;

use App\Helpers\ContactHelper;
use App\Models\City;
use App\Models\Club;
use App\Models\Contact;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class Gis extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:gis';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */

    protected $phone_id;
    protected $email_id;
    protected $address_id;
    protected $site_id;
    protected $vk_id;
    protected $fb_id;


    public function __construct()
    {
        parent::__construct();
        $this->phone_id     = ContactHelper::getContactTypeIdByTitle(ContactHelper::TYPE_PHONE);
        $this->email_id     = ContactHelper::getContactTypeIdByTitle(ContactHelper::TYPE_EMAIL);
        $this->address_id   = ContactHelper::getContactTypeIdByTitle(ContactHelper::TYPE_ADDRESS);
        $this->site_id      = ContactHelper::getContactTypeIdByTitle(ContactHelper::TYPE_SITE);
        $this->vk_id        = ContactHelper::getContactTypeIdByTitle(ContactHelper::TYPE_VK);
        $this->fb_id        = ContactHelper::getContactTypeIdByTitle(ContactHelper::TYPE_FB);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() : void
    {
        $this->info('start');

        DB::beginTransaction();
        try{
            $filePath = resource_path('imports/gis.xlsx');
            Excel::load($filePath, function($reader){

                $reader->each(function($sheet, $i){
                    if($i > 0)
                        return;
                    $this->info($i);
                    $data = $sheet->all();
                    $bar = $this->output->createProgressBar(sizeof($data));
                    foreach ($data as $item){
                        try{
                            $this->parseItem($item);
                        }catch (\Exception $e){
                            DB::rollBack();
                            $this->error($e->getMessage());
                            $this->error($e->getTraceAsString());
                            dd($item);
                        }
                        $bar->advance();
                    }
                    $bar->finish();

                });
            });
            DB::commit();
        }catch (\Exception $e){
            $this->error($e->getMessage());
            $this->error($e->getTraceAsString());
            DB::rollBack();
        }
    }


    protected function parseItem($data)
    {
        $rankData = preg_replace('/\D/', '', $data[0]);
        if(empty($rankData))
            $rankData = Club::RANK_4;

        if(!empty($data[2])){
            $dataName = mb_split(',', $data[2], 2);
            if(sizeof($dataName) === 1){
                $name = $dataName[0];
                $description = '';
            }else{
                $name = $dataName[0];
                $description = $dataName[1];
            }
        }else{
            $name = '';
            $description = '';
        }

        $cityId = null;
        if(!empty($data[3]))
            $cityId = $this->getCity($data[3])->id;
        $club = new Club();
        $club->fill([
            'rank'          => $rankData,
            'type_old'          => !empty($data[1]) ? $data[1] : '',
            'name'          => trim($name),
            'description'   => trim($description),
            'city_id'       => $cityId,
        ]);
        $contacts = [];

        //Parse address
        if(!empty($this->address_id)){
            $address = new Contact([
                'value'             => !empty($data[4]) ? trim($data[4]) : '',
                'contact_type_id'   => $this->address_id
            ]);
            $contacts[] = $address;
        }

        //Parse phones
        if(!empty($this->phone_id)){
            $stPhones = !empty($data[5]) ? mb_split(',', $data[5]) : [];
            $mobPhones = !empty($data[6]) ? mb_split(',', $data[6]) : [];
            $phones = array_merge($stPhones, $mobPhones);
            foreach ($phones as $phone){
                $_phone = new Contact([
                    'value'             => trim($phone),
                    'contact_type_id'   => $this->phone_id
                ]);
                $contacts[] = $_phone;
            }
        }

        //Parse emails
        if(!empty($this->email_id)){
            $emails1 = !empty($data[7]) ? mb_split(',', $data[7]) : [];
            $emails2 = !empty($data[8]) ? mb_split(',', $data[8]) : [];
            $emails = array_merge($emails1, $emails2);
            foreach ($emails as $email){
                $_email = new Contact([
                    'value'             => trim($email),
                    'contact_type_id'   => $this->email_id
                ]);
                $contacts[] = $_email;
            }
        }

        //PARSE SITE
        if(!empty($this->site_id) && !empty($data[9])){
            $site = new Contact([
                'value'             => trim($data[9]),
                'contact_type_id'  => $this->site_id
            ]);
            $contacts[] = $site;
        }

        //PARSE VK
        if(!empty($this->vk_id) && !empty($data[10])){
            $site = new Contact([
                'value'             => trim($data[10]),
                'contact_type_id'  => $this->vk_id
            ]);
            $contacts[] = $site;
        }
        //PARSE FB
        if(!empty($this->fb_id) && !empty($data[11])){
            $site = new Contact([
                'value'             => trim($data[11]),
                'contact_type_id'  => $this->fb_id
            ]);
            $contacts[] = $site;
        }

        //PARSE COORDS
        if(!empty($data[12]) && !empty($data[13])){
            $club->coordinates = "(${data[12]},${data[13]})";
        }

        $club->save();
        $club->contacts()->saveMany($contacts);
    }

    private function getCity($title)
    {
        return City::firstOrCreate(['name' => trim($title)]);
    }
}
