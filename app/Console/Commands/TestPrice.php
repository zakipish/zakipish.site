<?php

namespace App\Console\Commands;

use App\Models\Club;
use App\Models\ClubChain;
use App\Models\Playground;
use App\Models\Slug;
use App\Models\Sport;
use App\Rules\SlugUnique;
use App\Services\PriceService;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

class TestPrice extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:price';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $playground = Playground::find(29);
        $price = PriceService::getPriceForPeriod($playground, ['2018-10-26T13:00:00', '2018-10-26T16:00:00']);
        $this->info("Price: $price");
        $this->info("Done");
    }
}
