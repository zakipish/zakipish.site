<?php

namespace App\Console\Commands\Hack;

use App\Models\Club;
use Illuminate\Console\Command;

class FillTypes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'hack:fill-types';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('start');
        $clubs = Club::all();
        foreach ($clubs as $club){

            switch ($club->type_old){
                case 'зал':
                    $this->updateClub($club, Club::TYPE_ZAL);
                    break;

                case 'секция':
                    $this->updateClub($club, Club::TYPE_SECTION);
                    break;

                case 'секция и зал':
                    $this->updateClub($club, Club::TYPE_ZAL_AND_SECTION);
                    break;

                default:
                    $this->info('empty');
            }
        }
    }


    protected function updateClub($club, $value)
    {
        $club->update(['type' => $value]);
        $this->info("Club updated " . $club->id . " set type ${value}");
    }
}
