<?php

namespace App\Console\Commands;

use App\Models\Club;
use App\Models\Contact;
use App\Services\DuplicateIntoParrent;
use Illuminate\Console\Command;

class DeleteDuplicates extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'duplicates:delete';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Removes all duplicates from Clubs';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('start');
        $clubs = Club::where('parent_club_id', '!=', null)->orderBy('id', 'desc')->get();

        foreach ($clubs as $club) {
            $parent_club = Club::find($club->parent_club_id);
            $this->info('children:' . $club->id . ' '. $club->name . ' --- ' . 'parent:' . $parent_club->id . ' ' . $parent_club->name . PHP_EOL);
            foreach ($club->contacts as $current_contact) {
                foreach ($parent_club->contacts as $parent_contact) {
                    if (
                        ($current_contact->value == $parent_contact->value) and
                        ($current_contact->contact_type_id == $parent_contact->contact_type_id) and
                        ($current_contact->id != $parent_contact->id)
                    ) {

                        $this->info('i want to delete');
                        $count = $parent_club->contacts()
                            ->where('contact_type_id', $current_contact->contact_type_id)
                            ->where('value', $current_contact->value)
                            ->count();

                        if($count > 1){
                            $parent_club->contacts()->detach($parent_contact->id);
                            $this->info('deleted');
                        }else{
                            $this->info('soryan brat');
                        }

                    }

                }
            }
        }

        $this->info('end');
    }
}
