<?php

namespace App\Console\Commands;

use App\Models\Club;
use App\Models\ClubChain;
use App\Models\Slug;
use App\Models\Sport;
use App\Rules\SlugUnique;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

class TestSport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:sport';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // пример поиска клубов содержащих залы, относящиеся к указанному виду спорта
        $sport = Sport::query()->where('name', 'Футбол')->first();
        $clubs = Club::query()->whereHas('playgrounds', function (Builder $query) use ($sport) {
            $query->whereHas('sports', function (Builder $query) use ($sport) {
                $query->where('label', '>=', $sport->label);
            });
        })->get();
        $this->info("Clubs for '$sport->name': " . implode(', ', $clubs->pluck('name')->toArray()));

        // пример поиска клубов содержащих залы, относящиеся к указанным видам спорта
        $sports = Sport::query()->where('name', 'Футбол')->orWhere('name', 'Коньки')->pluck('label')->toArray();
        $labels = 'array[' . implode(', ', array_map(function ($label) {return "'$label'::ltree";}, $sports)) . ']';
        $clubs = Club::query()->whereHas('playgrounds', function (Builder $query) use ($labels) {
            $query->whereHas('sports', function (Builder $query) use ($labels) {
                $query->where('label', '<@', DB::raw($labels));
            });
        })->get();
        $this->info("Clubs for $labels: " . implode(', ', $clubs->pluck('name')->toArray()));
    }
}
