<?php

namespace App\Console\Commands;

use App\Services\UploadService;
use Illuminate\Console\Command;

class TestUploader extends Command
{

    protected $signature = 'test:uploader';

    protected $description = 'Command description';

    protected $uploadService;

    public function __construct()
    {
        parent::__construct();
        $this->uploadService = app('UploadService');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('start');
        $path ='https://camo.githubusercontent.com/c0083546776cef31955a67cd716c766a52dcd593/68747470733a2f2f7370617469652e6769746875622e696f2f696d6167652d6f7074696d697a65722f6578616d706c65732f6c6f676f2e706e67';
        $file = $this->uploadService->prepareRemoteImage($path);
        $result = $this->uploadService->uploadImage($file);
        dd($result);
    }
}
