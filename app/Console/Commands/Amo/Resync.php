<?php

namespace App\Console\Commands\Amo;

use App\Jobs\SyncAmoClub;
use App\Models\Tasks\ClubAmoCrmSyncTask;
use Illuminate\Console\Command;

class Resync extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'amo:resync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $this->info('start');

        $tasks = ClubAmoCrmSyncTask::whereIn('status', [
            ClubAmoCrmSyncTask::STATUS_PROGRESS,
            ClubAmoCrmSyncTask::STATUS_ERROR
        ])->get();

        foreach($tasks as $task){
            $task->update(['status' => ClubAmoCrmSyncTask::STATUS_READY]);
            SyncAmoClub::dispatch($task->id)->onQueue($task->getQueueName());

        }
    }
}
