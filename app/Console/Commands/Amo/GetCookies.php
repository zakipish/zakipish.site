<?php

namespace App\Console\Commands\Amo;

use App\Services\AmoCrmService;
use Illuminate\Console\Command;

class GetCookies extends Command
{
    protected $amoService;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'amo:get-cookies';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->amoService = app('AmoCrmService');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('start');
        $r = $this->amoService->auth();

        if(in_array($r['code'], [200, 204])){
            $this->info('success');
        }else{
            $this->error('error');
        }
    }
}
