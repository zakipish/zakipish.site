<?php

namespace App\Console\Commands\Amo;

use App\Models\Club;
use Illuminate\Console\Command;

class TestSync extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */


    protected $signature = 'amo:test-sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';


    protected $amoService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->amoService = app('AmoCrmService');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('start');
        $club = Club::whereNotNull('type')->orderBy('id','desc')->first();
        $r = $this->amoService->syncClub($club);
        $this->info($r);
    }
}
