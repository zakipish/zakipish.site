<?php

namespace App\Console\Commands\Amo;

use App\Events\ClubChanged;
use App\Models\Club;
use App\Models\ClubChain;
use App\Models\Tasks\ClubAmoCrmSyncTask;
use Illuminate\Console\Command;

class SyncAll extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'amo:sync-all';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('start');
        $clubs = Club::whereIn('rank', [Club::RANK_1, Club::RANK_2, Club::RANK_3])
            ->get();

        foreach ($clubs as $club){
            $this->info($club->id);
            event(new ClubChanged($club));
        }
    }
}
