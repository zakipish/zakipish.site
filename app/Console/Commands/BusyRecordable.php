<?php

namespace App\Console\Commands;

use App\Models\Game;

use App\Services\IntervalService;

use Illuminate\Console\Command;

class BusyRecordable extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'set:busy_recordable';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Upgrade games to busy_recordable system';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $games = Game::all();
        $games->each(function ($game) {
            if ($game->busyRecord)
                return true;

            $game->busyRecord()->create([
                'playground_id' => $game->oldParams['playground_id'],
                'interval' => IntervalService::getIntervalFromTimetzrange($game->oldParams['interval']),
                'date' => $game->oldParams['date']
            ]);
        });
    }
}
