<?php

namespace App\Console\Commands;

use App\Models\Event;
use App\Models\Game;
use Illuminate\Console\Command;

class DeleteAddressContacts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'address_contacts:delete';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete old contacts type of address';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $contactType = \App\Models\ContactType::where('name', 'Адрес')->first();
        if (!$contactType)
            return true;

        \App\Models\Contact::where('contact_type_id', $contactType->id)->delete();
        $contactType->delete();
    }
}

