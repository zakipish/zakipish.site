<?php

namespace App\Console\Commands;

use App\Models\Club;
use App\Services\SlugService;
use Illuminate\Console\Command;

class UpdateSlugs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:slugs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    protected $slugService;

    /**
     * Create a new command instance.
     *
     * @param SlugService $slugService
     */
    public function __construct(SlugService $slugService)
    {
        $this->slugService = $slugService;
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $query = Club::query();
        $bar = $this->output->createProgressBar($query->count());
        $query->each(function (Club $club) use ($bar) {
            $bar->advance();

            if (!empty($club->name)) {
                // Если у модели есть имя
                $this->slugService->generateSlug($club);
            }
        });
        $bar->finish();
        $this->info('');
        $this->info('Done');
    }

}
