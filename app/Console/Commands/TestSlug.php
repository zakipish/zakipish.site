<?php

namespace App\Console\Commands;

use App\Models\Club;
use App\Models\Slug;
use App\Rules\SlugUnique;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class TestSlug extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:slug';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::enableQueryLog();

        try {
            $club = new Club([
                'slug' => '1',
                'name' => '111',
            ]);
//            $club = Club::find(1);
//        $club->slugs()->associate(new Slug(['value' => 'a']));
            $club->save();

//            $club = Club::findBySlug('2');
            $data = ['slug' => '2'];
            $validator = validator($data, [
                'slug' => ['required',
                        'unique:slugs,value,1,sluggable_id'
//                    new SlugUnique
                ],
            ]);
            $validatedData = $validator->validate();
            dd($validatedData);

            $club->update($validatedData);
            dd($club);
        } catch (\Exception $e) {
            dd($e->getMessage());
        }

        dd(DB::getQueryLog());
    }
}
