<?php

namespace App\Console\Commands;

use App\Models\Playground;
use App\Models\PlaygroundSchedule;

use App\Services\ScheduleService;

use Illuminate\Console\Command;

class FlatSchedule extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'schedule:flat';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Upgrade schedule to flat system';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $playgrounds = Playground::query()
            ->with(['playground_schedules' => function ($q) {
                $q->orderBy('id');
            }])
            ->whereHas('playground_schedules')
            ->get();
        $playgrounds->each(function ($playground) {
            $schedules = $playground->playground_schedules->toArray();

            $playground->playground_schedules()->delete();

            foreach ($schedules as $schedule) {
                ScheduleService::create($playground, $schedule['state'], $schedule['date'], $schedule['interval'], $schedule['price']);
            }
        });
    }
}
