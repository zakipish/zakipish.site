<?php

namespace App\Console\Commands;

use App\Models\Club;
use App\Services\DuplicateIntoParrent;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class MoveDataToParent extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'duplicates:move-data {--parent=} {--child=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('start');


        $childId = $this->option('child');
        if(empty($childId)){
            $this->error('child id is empty');
            return false;
        }

        $club = Club::find($childId);
        if(empty($club)){
            $this->error('child club not found');
            return false;
        }



        $parentId = $this->option('parent');
        if(empty($parentId)){
            $this->error('parent id is empty');
            return false;
        }

        $parentClub = Club::find($parentId);
        if(empty($parentClub)){
            $this->error('parent club not found');
            return false;
        }

        if(!empty($club->parent_club) && $club->parent_club_id != $parentClub->id){
            $this->error('club already have parent');
            return false;
        }

        DB::beginTransaction();
        try{
            $club->update(['parent_club_id' => $parentClub->id]);
            $helper = new DuplicateIntoParrent($club);
            $helper->dupicate();
            DB::commit();
            $this->info('success import');
        }catch (\Exception $e){
            DB::rollBack();
            $this->error($e->getMessage());
        }
    }
}
