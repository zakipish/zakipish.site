<?php

namespace App\Console\Commands;

use App\Models\Department;
use App\Models\Club;
use Illuminate\Console\Command;

class FillDepartments extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fill:departments';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'fill departments from table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Берем все клубы
        $clubs = Club::all();
        $club_departments = $clubs->reduce(function($acc, $club) {
            $city_id = $club->city_id;
            if ($city_id && $club->department) {
                if (!isset($acc[$city_id]))
                    $acc[$city_id] = [];

                $acc[$city_id][] = $club->department;
            }

            return $acc;
        }, []);

        $department_keys = array_keys($club_departments);
        foreach ($department_keys as $club_city){
            $departments = array_unique($club_departments[$club_city]);
            foreach($departments as $department){
                //Создаем получается район в городе, огороде
                $new_department = new Department(['name'=>$department,'city_id'=>(int)$club_city]);
                $new_department->save();
                //Нужно привязать id только что добавленного района, ну это наверное будет короч отдельный скрипт
            }
        }
        print("<pre>".print_r("Получилось, смотри что в таблице",true)."</pre>");
    }
}
