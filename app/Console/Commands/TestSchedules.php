<?php

namespace App\Console\Commands;

use App\Models\Playground;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class TestSchedules extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:schedules';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('start');

        DB::table('playground_schedules')->truncate();

        $playgrounds = Playground::whereIn('id',[654, 6, 946, 11])->get();
        foreach ($playgrounds as $playground){

            $date = Carbon::now()->addDays(-10)->startOfDay();
            $dateEnd = Carbon::now()->addDays(10)->startOfDay();


            $dateI = date('Y-m-d', $date->timestamp);
            $dateIEnd = date('Y-m-d', $dateEnd->timestamp);

            while ($dateI <= $dateIEnd){
                if(empty($playground->min_interval))
                    $playground->min_interval = 30;

                $sch = $playground->playground_schedules()->create([
                    'date'      => $dateI,
                    'interval'  => ['07:00', '22:00']
                ]);
                DB::table('playground_schedules')
                    ->where('id', $sch->id)
                    ->update([
                        'price'         => 200,
                        //'interval'      => DB::raw("timetzrange('07:00:00+03', '23:00:00+03', '[)')")
                    ]);

                $dateI = date('Y-m-d', strtotime($dateI . ' + 1 days'));

            }

        }
    }
}
