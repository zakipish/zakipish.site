<?php

namespace App\Console\Commands;

use App\Models\Event;
use App\Models\Game;
use Illuminate\Console\Command;

class CreateChats extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'games:create_chats';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create chats for games';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $games = Game::all();
        $games->each(function ($game) {
            if ($game->chat)
                return true;

            $chat = $game->chat()->create();
            $game->chatMessages()->update(['chat_id' => $chat->id]);
        });

        $events = Event::all();
        $events->each(function ($event) {
            if ($event->chat || $event->chat_disabled)
                return true;

            $chat = $event->chat()->create();
        });
    }
}
