<?php

namespace App\Repositories;

use App\Models\Event;

use \DateTimeZone;
use Carbon\Carbon;
use App\Services\IntervalService;
use Illuminate\Support\Facades\Log;

class EventRepository
{
    public static function filterEvents(Array $params)
    // Params:
    // Int $city_id
    // Int $sport_id
    // Carbon $date
    // Int $requester_user_id
    // Array $interval
    // Int $perPage
    // Int $page
    {
        $events = Event::query();

        $date = array_get($params, 'date');
        if ($date) $events->whereDate($date);

        $city_id = array_get($params, 'city_id');
        if ($city_id) $events->whereCityId($city_id);


        $sport_id = array_get($params, 'sport_id');
        if ($sport_id) $events->whereSportId($sport_id);

//        $requester_user_id = array_get($params, 'requester_user_id');
//        if ($requester_user_id) $events->withoutUserId($requester_user_id);

        $page = array_get($params, 'page');
        $perPage = array_get($params, 'perPage');
        if ($page && $perPage) {
            $events->limit($perPage);
            $events->offset($perPage * ($page - 1));
        }

        $interval = array_get($params, 'interval');
        if ($interval) {
            $timezone = new DateTimeZone('Europe/Moscow');
            $timetzrange = IntervalService::getTimetzrange($interval, $timezone);
            $events->crossingWith($timetzrange);
        }

        return $events;
    }

    public static function getCount(Array $params)
    // Params:
    // Int $city_id
    // Int $sport_id
    // Carbon $date
    // Int $requester_user_id
    // Array $interval
    {
        return self::filterEvents($params)->count();
    }

    public static function getEvents(Array $params)
    // Params:
    // Int $city_id
    // Int $sport_id
    // Carbon $date
    // Int $requester_user_id
    // Array $interval
    // Int $perPage
    // Int $page
    {
        return self::filterEvents($params)->get();
    }
}
