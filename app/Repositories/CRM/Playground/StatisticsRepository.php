<?php

namespace App\Repositories\CRM\Playground;

use App\Models\Playground;
use \DateTime;
use Illuminate\Support\Facades\Log;

class StatisticsRepository
{
    public function dashboard(Playground $playground, DateTime $startDate, DateTime $endDate)
    {
        $games = $playground->games()
                            ->with(['users'])
                            ->whereHas('busyRecord', function($q) use ($startDate, $endDate) {
                                $q->select(['date', 'interval'])->whereBetween(
                                    'date', [$startDate->format('Y-m-d'), $endDate->format('Y-m-d')]
                                );
                            })
                            ->get(['games.id', 'min_players']);

        $schedules = $playground->playground_schedules()
                                ->whereBetween(
            'date', [$startDate->format('Y-m-d'), $endDate->format('Y-m-d')]
        )->get(['id', 'date', 'interval']);
        $gamesCount = $this->gamesCount($schedules, $games, $startDate, $endDate);
        $schedulesCount = $this->schedulesCount($playground, $startDate);
        return [
            'games' => $gamesCount,
            'schedules' => $schedulesCount
        ];

    }

    protected function gamesCount($schedules, $games, DateTime $startDate, DateTime $endDate)
    {
        $result = [];
        for($date = (clone $startDate); $date <= $endDate; $date->modify("+1 day")) {
            $dateStr = $date->format('Y-m-d');

            $dateGames = $games->filter(function ($game) use ($dateStr) {
                return $game->date === $dateStr;
            });

            $complete = $dateGames->filter(function ($game) {
                return $game->users->count() >= $game->min_players;
            })->count();

            $pending = $dateGames->count() - $complete;

            $dateSchedules = $schedules->filter(function ($schedule) use ($dateStr) {
                return $schedule->date === $dateStr;
            });

            $allHoursCount = $dateSchedules->reduce(function ($acc, $schedule) {
                return $acc + abs(($schedule->timeStart->getTimestamp() - $schedule->timeEnd->getTimestamp()) / 60);
            });

            if ($allHoursCount > 0) {
                $gamesHoursCount = $dateGames->reduce(function ($acc, $game) {
                    return $acc + abs(($game->timeStart->getTimestamp() - $game->timeEnd->getTimestamp()) / 60);
                });

                $booked = round($gamesHoursCount / $allHoursCount, 2) * 100;
            } else {
                $booked = 0;
            }

            $result[$dateStr] = [
                'booked' => $booked,
                'complete' => $complete,
                'pending' => $pending
            ];
        }

        return $result;
    }

    protected function schedulesCount(Playground $playground, DateTime $dateStart)
    {
        $schedules = $playground->playground_schedules()->where('date', '>=', $dateStart->format('Y-m-d'))->get();
        $days = $schedules->groupBy('date');
        $days = $days->count();

        $percents = $days / 365;

        return [
            'days' => $days,
            'percents' => $percents
        ];
    }
}
