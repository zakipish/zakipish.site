<?php

namespace App\Repositories\CRM\Playground;

use \DateTime;

use App\Models\Playground;

use Illuminate\Http\Request;

class PlannerRepository
{

    public function getPrices(Request $request, Playground $playground, $scheduleTime = false)
    {
        $dateStart = new DateTime($request->get('dateStart'));
        $dateEnd = new DateTime($request->get('dateEnd'));

        $dateStartStr = $dateStart->format('Y-m-d');
        $dateEndStr = $dateEnd->format('Y-m-d');

        $schedules = $playground->playground_schedules()
                                ->select('date', 'interval', 'price')
                                ->whereBetween(
                                    'date',
                                    [$dateStartStr, $dateEndStr]
                                )
                                ->orderByDesc('created_at')
                                ->get()
                                ->groupBy('date');

        $busyRecords = $playground->busyRecords()
                            ->select('date', 'interval')
                            ->whereBetween(
                                'busy_records.date',
                                [$dateStartStr, $dateEndStr]
                            )
                            ->get()
                            ->groupBy('date');

        $minInterval = $playground->min_interval;

        $dates = [];

        if ($scheduleTime) {
            $timeStart = $schedules->flatten()->min('timeStart');
            $timeEnd = $schedules->flatten()->max('timeEnd');

            if (!$timeStart || !$timeEnd) {
                $timeStart = new DateTime("00:00:00");
                $timeEnd = new DateTime("23:59:59");
            }
        } else {
            $timeStart = new DateTime("00:00:00");
            $timeEnd = new DateTime("23:59:59");
        }

        for ($date = clone($dateStart); $date <= $dateEnd; $date->modify('+1 day')) {
            $dateStr = $date->format('Y-m-d');
            $timeStart = $timeStart->setDate($date->format('Y'), $date->format('m'), $date->format('d'));
            $timeEnd = $timeEnd->setDate($date->format('Y'), $date->format('m'), $date->format('d'));

            $dateSchedules = $schedules->get($dateStr);
            $dateRecords = $busyRecords->get($dateStr);

            $times = [];

            for ($intervalStart = clone $timeStart; $intervalStart < $timeEnd; $intervalStart->modify("+{$minInterval} minutes")) {
                $intervalEnd = (clone $intervalStart)->modify("+{$minInterval} minutes");
                $state = 'open';
                $price = null;

                $showTimeStart = $intervalStart->format('H:i');
                $showTimeEnd = $intervalEnd->format('H:i');

                if ($showTimeEnd == '00:00')
                    $showTimeEnd = '24:00';

                if (!$dateSchedules) {
                    $times[] = [
                        'timeStart' => $showTimeStart,
                        'timeEnd' => $showTimeEnd,
                        'value' => null,
                        'state' => 'empty'
                    ];
                    continue;
                } else {
                    $intervalSchedulesPresent = $dateSchedules && $dateSchedules->contains(function($schedule) use ($intervalStart, $intervalEnd) { return $schedule->timeStart <= $intervalStart && $schedule->timeEnd >= $intervalEnd; });
                }

                if (!$intervalSchedulesPresent) {
                    $state = 'empty';
                    $price = null;
                } else {
                    $intervalRecordsPresent = $dateRecords && $dateRecords->contains(function($record) use ($intervalStart, $intervalEnd) { return $record->timeStart <= $intervalStart && $record->timeEnd >= $intervalEnd; });
                }

                if ($state == 'open' && $intervalRecordsPresent) {
                    $state = 'game';
                } else {
                    $firstIntervalSchedule = $dateSchedules->first(function($schedule) use ($intervalStart, $intervalEnd) { return $schedule->timeStart <= $intervalStart && $schedule->timeEnd >= $intervalEnd; });
                }

                if ($state == 'open' && $firstIntervalSchedule) {
                    $state = $firstIntervalSchedule->state;
                    $price = $firstIntervalSchedule->price;
                }

                $times[] = [
                    'timeStart' => $showTimeStart,
                    'timeEnd' => $showTimeEnd,
                    'value' => intval($price),
                    'state' => $state
                ];
            }

            unset($dates[$dateStr]);
            unset($busyRecords[$dateStr]);

            $dates[] = [ 'date' => $date->format('Y-m-d'), 'prices' => $times ];
        }

        $commonTimeStart = $timeStart->format('H:i');
        $commonTimeEnd = $timeEnd->format('H:i');

        if ($commonTimeEnd == '00:00')
            $commonTimeEnd = '24:00';


        return ['timeStart' => $commonTimeStart, 'timeEnd' => $commonTimeEnd, 'interval' => $playground->min_interval, 'dates' => $dates];
    }
}
