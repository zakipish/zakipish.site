<?php

namespace App\Repositories\CRM;

use App\Models\Playground;
use App\Models\PlaygroundSchedule;
use App\Services\PriceService;
use App\Services\ScheduleService;

class PlaygroundScheduleRepository
{
    public function create(Playground $playground, $params)
    {
        $schedules = [];
        foreach($params['list'] as $day) {
            foreach($day['durations'] as $interval) {
                $schedules[] = ScheduleService::create($playground, $params['state'], $day['date'], [$interval['from'], $interval['to']], $params['price']);
            }
        }

        return $schedules;
    }
}
