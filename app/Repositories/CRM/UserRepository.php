<?php

namespace App\Repositories\CRM;

use App\Models\User;

use Illuminate\Http\Request;

class UserRepository
{
    public function search(Request $request)
    {
        $users = User::query();

        if ($request->has('phone')) {
            $phoneRaw = $request->get('phone');
            $phoneRaw = preg_replace('/[^0-9\+.]/', '', $phoneRaw);
            preg_match('/(7|\+7|8)?(\d+)/', $phoneRaw, $matches);
            if ($matches) {
                $phone = $matches[2];
                $users->where('phone', 'LIKE', "$phone%");
            }
        }

        return $users->orderBy('id')->get();
    }
}
