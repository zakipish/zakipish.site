<?php

namespace App\Repositories;

use App\Services\IntervalService;

use App\Models\Club;
use App\Models\Playground;

use \DateTime;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Carbon\CarbonInterval;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class PlaygroundRepository
{
    const DAYS_COUNT_FOR_FILTER = 10;

    public function getDates(Array $params)
    // Params:
    // Carbon start
    // Int city_id
    // Int sport_id
    {
        $start = $params['start'];
        $end = $start->copy()->addDays(self::DAYS_COUNT_FOR_FILTER);
        $period = new CarbonPeriod($start, $end);

        $startStr = $start->toDateString();
        $endStr = $end->toDateString();

        $playgrounds = Playground::query()
            ->with(['playground_schedules' => function ($q) use ($startStr, $endStr) {
                $q->whereBetween('date', [$startStr, $endStr]);
            }])
            ->with(['busyRecords' => function($q) use ($startStr, $endStr) {
                $q->whereBetween('date', [$startStr, $endStr]);
            }]);
        $playgrounds->whereNotNullCoordinates();

        $city_id = array_get($params, 'city_id');
        if ($city_id) $playgrounds->whereCityId($city_id);

        $sport_id = array_get($params, 'sport_id');
        if ($sport_id) $playgrounds->whereSportId($sport_id);

        $playgrounds = $playgrounds->get();

        $dates = [];

        foreach ($period as $date) {
            $dateStr = $date->toDateString();

            $count = $playgrounds->filter(function ($playground) use ($date, $dateStr) {
                $dateSchedules = $playground->playground_schedules->filter(function ($schedule) use ($dateStr) {
                    return $schedule->date == $dateStr;
                });

                //Log::debug('Расписаний: ' . $dateSchedules->count());

                if ($dateSchedules->count() == 0)
                    return true;

                $dateBusyRecords = $playground->busyRecords->filter(function ($busyRecord) use ($dateStr) {
                    return $busyRecord->date == $dateStr;
                });
                //Log::debug('Записей: ' . $dateBusyRecords->count());

                $now = new DateTime();

                $freeMinutes = $dateSchedules->reduce(function ($sum, $schedule) use ($date, $now) {
                    $start = $schedule->timeStart;
                    $end = $schedule->timeEnd;

                    if ($date->isCurrentDay()) {
                        //Log::debug('Уткнулись в сегодня');
                        if ($end < $now)
                            return $sum;

                        if ($end > $now && $start < $now) {
                            $start = $now;
                        }
                    }

                    $minutes = ($end->getTimestamp() - $start->getTimestamp()) / 60;
                    $sum += $minutes;

                    return $sum;
                }, 0);
                //Log::debug('Свободных минут сегодня: ' . $freeMinutes);

                $busyMinutes = $dateBusyRecords->reduce(function ($sum, $busyRecord) use ($date, $now) {
                    $start = $busyRecord->timeStart;
                    $end = $busyRecord->timeEnd;

                    if ($date->isCurrentDay()) {
                        if ($end < $now)
                            return $sum;

                        if ($end > $now && $start < $now) {
                            $start = $now;
                        }
                    }

                    $minutes = ($end->getTimestamp() - $start->getTimestamp()) / 60;
                    $sum += $minutes;

                    return $sum;
                }, 0);

                //Log::debug('Занятых минут сегодня: ' . $busyMinutes);
                //Log::debug('Разница: ' . ($freeMinutes - $busyMinutes));

                return ($freeMinutes - $busyMinutes) > 0;
            })->count();

            $dates[] = [
                'date' => $dateStr,
                'count' => $count
            ];
        }

        return $dates;
    }

    public function getTimes(Request $request)
    {
        $date = new DateTime($request->get('date'));
        $dateStr = $date->format('Y-m-d');

        $playgrounds = Playground::query()
            ->whereRaw("
                (SELECT
                    COALESCE(
                        EXTRACT('epoch' FROM
                            SUM((date + upper(interval)) - (date + lower(interval)))
                        )
                    , 0)
                FROM playground_schedules WHERE
                    playground_id = playgrounds.id AND
                    date = '$dateStr'
                ) -
                (SELECT
                    COALESCE(
                        EXTRACT('epoch' FROM
                            SUM((date + upper(interval)) - (date + lower(interval)))
                        )
                    , 0)
                FROM busy_records WHERE
                    playground_id = playgrounds.id AND
                    date = '$dateStr'
                ) > 0
            ")
            ->with(['playground_schedules' => function ($q) use ($date) {
                $q->where(['date' => $date]);
            }])
            ->with(['busyRecords' => function ($q) use ($date) {
                $q->where(['date' => $date]);
            }]);

        $playgrounds->whereHas('club', function($q) {
            $q->whereNotNull('coordinates');
        });

        if ($request->has('city_id')) {
            $cityId = $request->get('city_id');
            $playgrounds->whereHas('club', function($q) use($cityId) {
                $q->where('city_id', $cityId);
            });
        }

        if ($request->has('sport_id')) {
            $sportId = $request->get('sport_id');
            $playgrounds->whereHas('sports', function($q) use ($sportId){
                $q->where('playground_sport.sport_id', $sportId);
            });
        }

        $playgrounds = $playgrounds->get();

        $minTimeStart = (new DateTime())->setTime(0,0,0)->format('H:i');
        $maxTimeEnd = (new DateTime())->setTime(23, 59, 59)->format('H:i');
        $minInterval = 60;

        if ($playgrounds->count() > 0) {
            $minInterval = $playgrounds->min('min_interval');
            $schedules = $playgrounds->pluck('playground_schedules')->flatten();

            $minTimeStart = $schedules->min('timeStart')->format('H:i');
            $maxTimeEnd = $schedules->max('timeEnd')->format('H:i');
        }

        $schedules = $playgrounds->pluck('playground_schedules')->flatten();

        $timeStart = new DateTime($date->format('Y-m-d') . $minTimeStart);
        $timeEnd = new DateTime($date->format('Y-m-d') . $maxTimeEnd);

        $times = [];
        for ($intervalStart = clone $timeStart; $intervalStart < $timeEnd; $intervalStart->modify("+{$minInterval} minutes")) {
            $intervalEnd = (clone $intervalStart)->modify("+{$minInterval} minutes");
            $state = true;

            $now = new DateTime();
            if ($dateStr == $now->format('Y-m-d'))
                if ($intervalStart <= $now)
                    $state = false;

            $price = 0;

            if ($playgrounds->count() == 0) {
                $times[] = [
                    'start' => $intervalStart->format('c'),
                    'end' => $intervalEnd->format('c'),
                    'status' => $state ? 'available' : 'unavailable',
                    'price' => $price
                ];

                continue;
            }

            if ($state) {
                $intervalSchedules = $schedules->filter(function ($schedule) use ($intervalStart, $intervalEnd) {
                    return $intervalStart <= $schedule->timeEnd && $intervalEnd >= $schedule->timeStart;
                });

                $state = !!$intervalSchedules;
                $price = $intervalSchedules->min('price');
            }


            if ($state)
                $state = $playgrounds->contains(function ($playground) use ($intervalStart, $intervalEnd) {
                    return !$playground->busyRecords->contains(function ($busyRecord) use ($intervalStart, $intervalEnd) {
                        return $intervalStart < $busyRecord->timeEnd && $intervalEnd > $busyRecord->timeStart;
                    });
                });

            $times[] = [
                'start' => $intervalStart->format('c'),
                'end' => $intervalEnd->format('c'),
                'status' => $state ? 'available' : 'unavailable',
                'price' => $price
            ];
        }

        return $times;
    }

    public function getPlaygrounds(Array $params)
    // Params:
    // Carbon date
    // Int city_id
    // Int sport_id
    // Int type
    // Int department_id
    // Int surface_id
    // String search
    // Array<String (00:00)> interval
    {
        $date = array_get($params, 'date');
        $interval = array_get($params, 'interval');

        $playgrounds = Playground::query()
            ->with([
                'surfaces',
                'images',
                'activeSlugs',
                'club',
                'club.images',
                'playground_schedules' => function ($scheduleQuery) use ($date, $interval) {
                    if ($date) {
                        $scheduleQuery->whereDate('date', $date);
                        if ($date->isCurrentDay() && $interval && sizeof(array_filter($interval)) > 0) {
                            $scheduleQuery->intervalCrossing($interval);
                        }
                    }
                },
            ]);

        $city_id = array_get($params, 'city_id');
        if ($city_id) $playgrounds->whereCityId($city_id);

        $sport_id = array_get($params, 'sport_id');
        if ($sport_id) $playgrounds->whereSportId($sport_id);

        $department_id = array_get($params, 'department_id');
        if ($department_id) $playgrounds->whereDepartmentId($department_id);

        $type = array_get($params, 'type');
        if ($type) $playgrounds->whereType($type);

        $surface_id = array_get($params, 'surface_id');
        if ($surface_id) $playgrounds->whereHasSurfaceId($surface_id);

        $search = array_get($params, 'search');
        if ($search) $playgrounds->searchByName($search);

        $playgrounds->whereNotNullCoordinates();

        if ($date) {
            $playgroundsWithoutSchedule = clone $playgrounds;
            $playgroundsWithSchedule = clone $playgrounds;
            unset($playgrounds);

            // HANDLE PLAYGROUNDS WITHOUT SCHEDULE
            $playgroundsWithoutSchedule->whereDoesntHasSchedules($date);
            $playgroundsWithoutSchedule = $playgroundsWithoutSchedule->get();
            $playgroundsWithoutSchedule->each(function ($playground) {
               $playground['hasSchedule'] = false;
            });

            // HANDLE PLAYGROUNDS WITH SCHEDULE
            if ($interval && sizeof(array_filter($interval)) > 0) {
                $playgroundsWithSchedule->whereHasSchedulesForInterval($date, $interval);
                $playgroundsWithSchedule->whereDoesntHasBusyRecordsForInterval($date, $interval);
            } else {
                $playgroundsWithSchedule->whereHasFreeTime($date);
            }

            $playgroundsWithSchedule = $playgroundsWithSchedule->get();
            $playgroundsWithSchedule->each(function ($playground) {
               $playground['hasSchedule'] = true;
            });

            $playgrounds = $playgroundsWithSchedule->merge($playgroundsWithoutSchedule);
        } else {
            $playgrounds = $playgrounds->get();
        }

        $playgrounds->each(function ($playground){
           $playground['min_price'] = $playground->playground_schedules->min('price');
        });

        return $playgrounds;
    }

    public function groupPlaygroundsByClub($playgrounds)
    {
        $groupedPlaygrounds = $playgrounds->groupBy('club_id');
        $clubIds = array_keys($groupedPlaygrounds->all());
        $clubs = Club::with([
            'images', 'activeSlugs', 'logo'
        ])
        ->whereIn('id', $clubIds)->get();

        $clubs->each(function ($club) use ($groupedPlaygrounds) {
            $club['playgrounds'] = $groupedPlaygrounds[$club->id];
            $club['playgrounds'] = $club['playgrounds']->sortByDesc('hasSchedule');
        });

        $clubs = $clubs->sortByDesc(function ($club) {
            return $club['playgrounds']->filter(function($playground) {
                return $playground->hasSchedule;
            })->count();
        });

        return $clubs;
    }

    function getPlaygroundDates($request, $playground)
    {
        $start = $request->has('date') ? (new Carbon($request->date))->startOfDay() : (new Carbon())->startOfDay();
        $end = $start->copy()->addDays(10);
        $period = new CarbonPeriod($start, $end);
        $schedules = $playground->playground_schedules()->whereBetween('date', [$start, $end])->get();
        $busyRecords = $playground->busyRecords()->whereBetween('date', [$start, $end])->get();

        $dates = [];
        foreach($period as $date) {
            $targetSchedules = $schedules->filter(function ($schedule) use ($date) {
                return new Carbon($schedule->date) == $date;
            })->all();

            if (sizeof($targetSchedules) == 0) {
                $dates[] = [
                    'date' => $date,
                    'count' => 1
                ];

                continue;
            }

            $targetBusyRecords = $busyRecords->filter(function ($busyRecord) use ($date) {
                return new Carbon($busyRecord->date) == $date;
            })->all();

            $schedulesIntervalSize = array_reduce(
                $targetSchedules,
                function ($size, $schedule) {
                    return $size + (new Carbon($schedule['interval'][0]))->diffInMinutes(new Carbon($schedule['interval'][1]));
                },
                0
            );

            $busyRecordsIntervalSize = array_reduce(
                $targetBusyRecords,
                function ($size, $busyRecord) {
                    return $size + (new Carbon($busyRecord['interval'][0]))->diffInMinutes(new Carbon($busyRecord['interval'][1]));
                },
                0
            );

            if ($schedulesIntervalSize > $busyRecordsIntervalSize) {
                $count = 1;
            } else {
                $count = 0;
            }

            $dates[] = [
                'date' => $date,
                'count' => $count
            ];
        }

        return collect($dates);
    }

    function getPlaygroundTimes($request, $playground)
    {
        $date = new Carbon($request->get('date'));
        $dateStr = $date->format('Y-m-d');
        $schedules = $playground->playground_schedules()->where(['date' => $date])->get();
        $busyRecords = $playground->busyRecords()->where(['date' => $date])->get();

        $schedules->each(function($schedule) {
            $schedule['start'] = new Carbon($schedule->interval[0]);
            $schedule['end'] = new Carbon($schedule->interval[1]);
        });

        $start = (new Carbon())->setTime(0, 0, 0);
        $end = (new Carbon())->setTime(23, 59, 59);
        $min_interval = 60;

        if ($schedules->count() > 0) {
            $start = new Carbon($schedules->min('start'));
            $end = new Carbon($schedules->max('end'));
            $min_interval = $playground->min_interval;
        }

        $times = [];
        for ($time = $start->copy(); $time->copy()->addMinutes($min_interval)->lte($end); $time->addMinutes($min_interval)) {
            $timeStart = $time->copy();
            $timeEnd = $time->copy()->addMinutes($min_interval);
            $status = 'available';
            $price = 0;

            if ($schedules->count() == 0) {
                $times[] = [
                    'start' => $date->copy()->setTimeFrom($timeStart),
                    'end' => $date->copy()->setTimeFrom($timeEnd),
                    'status' => $status,
                    'price' => $price
                ];

                continue;
            }

            $targetSchedules = $schedules->filter(function($schedule) use ($timeStart, $timeEnd) {
                $scheduleStart = new Carbon($schedule->interval[0]);
                $scheduleEnd = new Carbon($schedule->interval[1]);
                return $scheduleStart <= $timeStart && $scheduleEnd >= $timeEnd;
            });

            if ($targetSchedules->count() < 1) {
                $status = 'unavailable';
                $price = 0;
            } else {
                $price = $targetSchedules->min('price');
            }

            $now = new DateTime();
            if ($dateStr == $now->format('Y-m-d')) {
                if ($timeStart <= $now) {
                    $status = 'closed';
                }
            }

            $targetBusyRecords = $busyRecords->filter(function($busyRecord) use ($timeStart, $timeEnd) {
                $recordStart = new Carbon($busyRecord->interval[0]);
                $recordEnd = new Carbon($busyRecord->interval[1]);
                return $recordStart <= $timeStart && $recordEnd >= $timeEnd;
            });

            if ($targetBusyRecords->count() > 0) {
                $status = 'booked';
            }

            $times[] = [
                'start' => $date->copy()->setTimeFrom($timeStart),
                'end' => $date->copy()->setTimeFrom($timeEnd),
                'status' => $status,
                'price' => $price
            ];
        }

        return collect($times);
    }
}
