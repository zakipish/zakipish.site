<?php

namespace App\Repositories;

use App\Services\IntervalService;
use App\Models\Game;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use \DateTime;

class GameRepository
{
    const DAYS_COUNT_FOR_FILTER = 10;

    public function getDates(Array $params)
    // Params:
    // Int $city_id
    // Int $sport_id
    // Int $requester_user_id
    // Carbon $dateStart
    {
        $dateStart = array_get($params, 'dateStart');
        if (empty($dateStart)) $dateStart = new Carbon();
        $dateEnd = $dateStart->copy()->addDays(self::DAYS_COUNT_FOR_FILTER);

        $games = Game::query();
        $games->select([
            DB::raw('COUNT(*)'),
            'date'
        ]);
        $games->groupBy('date');

        $city_id = array_get($params, 'city_id');
        if ($city_id) $games->whereCityId($city_id);

        $games->whereDateBetween($dateStart, $dateEnd);

        $sport_id = array_get($params, 'sport_id');
        if ($sport_id) $games->whereSportId($sport_id);

        $requester_user_id = array_get($params, 'requester_user_id');
        if ($requester_user_id) $games->withoutUserId($requester_user_id);

        $games = $games->get();

        $games = $games->reduce(function ($acc, $game) {
            $acc[$game['date']] = $game['count'];
            return $acc;
        }, []);

        $datesPeriod = new CarbonPeriod($dateStart, $dateEnd);

        $dates = [];
        foreach ($datesPeriod as $date) {
            $dateStr = $date->toDateString();

            if (isset($games[$dateStr])) {
                $dates[] = [
                    'date' => $dateStr,
                    'count' => $games[$dateStr]
                ];
            } else {
                $dates[] = [
                    'date' => $dateStr,
                    'count' => 0
                ];
            }
        }

        return $dates;
    }

    public function getTimes(Array $params)
    // Params:
    // Int $city_id
    // Int $sport_id
    // Carbon $date
    // Int $requester_user_id
    {
        $games = Game::query();

        $city_id = array_get($params, 'city_id');
        if ($city_id) $games->whereCityId($city_id);

        $sport_id = array_get($params, 'sport_id');
        if ($sport_id) $games->whereSportId($sport_id);

        $date = array_get($params, 'date');
        if ($date) $games->whereDate('date', $date);

        $requester_user_id = array_get($params, 'requester_user_id');
        if ($requester_user_id) $games->withoutUserId($requester_user_id);

        $games = $games->get();

        if ($games->count() < 1)
            return collect([]);

        $playgrounds = $games->pluck('playground');
        $minInterval = $playgrounds->min('min_interval');

        $timeStart = new DateTime($date->format('Y-m-d') . $games->min('timeStart')->format('H:i'));
        $timeEnd = new DateTime($date->format('Y-m-d') . $games->max('timeEnd')->format('H:i'));

        $times = [];
        for ($intervalStart = clone $timeStart; $intervalStart < $timeEnd; $intervalStart->modify("+{$minInterval} minutes")) {
            $intervalEnd = (clone $intervalStart)->modify("+{$minInterval} minutes");
            $state = true;

            $now = new DateTime();

            $dateStr = $date->format('Y-m-d');

            if ($dateStr == $now->format('Y-m-d')) {
                if ($intervalStart <= $now) {
                    $state = false;
                }
            }

            $price = 0;
            if ($state)
                $price = $games->filter(function ($game) use ($intervalStart, $intervalEnd) {
                    return $game->timeStart < $intervalEnd && $game->timeEnd > $intervalStart;
                })->min('playerPrice');

            $times[] = [
                'start' => $intervalStart->format('c'),
                'end' => $intervalEnd->format('c'),
                'status' => $state ? 'available' : 'unavailable',
                'price' => $price
            ];
        }

        return $times;
    }

    public function filterGames(Array $params)
    // Params:
    // Int $city_id
    // Int $sport_id
    // Carbon $date
    // Array $interval
    // Int $requester_user_id
    // $perPage
    // $page
    {
        $interval = array_get($params, 'interval');

        $games = Game::query()->with(['playground', 'playground.club', 'users', 'sport']);

        $requester_user_id = array_get($params, 'requester_user_id');
        if ($requester_user_id) $games->withoutUserId($requester_user_id);

        $city_id = array_get($params, 'city_id');
        if ($city_id) $games->whereCityId($city_id);

        $sport_id = array_get($params, 'sport_id');
        if ($sport_id) $games->whereSportId($sport_id);

        $date = array_get($params, 'date');
        if ($date) $games->whereDate('date', $date);

        if ($interval) {
            $timezone = new \DateTimeZone('Europe/Moscow');
            $timetzrange = IntervalService::getTimetzrange($interval, $timezone);
            $games->crossingWith($timetzrange);
        }

        $page = array_get($params, 'page');
        $perPage = array_get($params, 'perPage');
        if ($page && $perPage) {
            $games->limit($perPage);
            $games->offset($perPage * ($page - 1));
        }

        return $games;
    }

    public function getCount(Array $params)
    // Params:
    // Int $city_id
    // Int $sport_id
    // Carbon $date
    // Array $interval
    // Int $requester_user_id
    {
        return self::filterGames($params)->count();
    }

    public function getGames(Array $params)
    // Params:
    // Int $city_id
    // Int $sport_id
    // Carbon $date
    // Array $interval
    // Int $requester_user_id
    // $perPage
    // $page
    {
        return self::filterGames($params)->get();
    }
}
