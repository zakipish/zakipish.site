<?php

namespace App\Observers;

use App\Models\Game;
use App\Models\Transaction;
use \DateTime;

/**
 * Class PlaygroundObserver
 * @package App\Observers
 */
class GameObserver
{

    public function creating(Game $game)
    {
        $this->setName($game);
    }

    public function updating(Game $game)
    {
        $this->setName($game);
    }

    public function created(Game $game)
    {
//        get users from Game and save it when model was created
        $users = $game->getUsersToSave();
        if ($users){
            $game->users()->sync($users);
        }
    }

    protected function setName(Game $game)
    {
        if (empty($game->name) && !$game->booking) {
            $datetime = new DateTime($game->date);
            $weekdaynum = intval($datetime->format('w'));

            switch($weekdaynum) {
                case 1:
                    $weekday = 'в понедельник';
                    break;
                case 2:
                    $weekday = 'во вторник';
                    break;
                case 3:
                    $weekday = 'в среду';
                    break;
                case 4:
                    $weekday = 'в четверг';
                    break;
                case 5:
                    $weekday = 'в пятницу';
                    break;
                case 6:
                    $weekday = 'в субботу';
                    break;
                case 0:
                    $weekday = 'в воскресенье';
                    break;
            }

            $game->name = $this->mb_ucfirst(strtolower($game->sport->name . ' ' . $weekday));
        }
    }

    protected function mb_ucfirst($text) {
        return mb_strtoupper(mb_substr($text, 0, 1)) . mb_substr($text, 1);
    }
}
