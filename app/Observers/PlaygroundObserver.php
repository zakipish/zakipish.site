<?php

namespace App\Observers;

use App\Models\Playground;
use App\Models\Surface;
use App\Models\Service;
use App\Models\Equipment;
use App\Models\Sport;
use App\Models\Slug;
use App\Models\Utils\SluggableInterface;
use Clockwork\Request\Log;
use Illuminate\Support\Facades\DB;

/**
 * Class PlaygroundObserver
 * @package App\Observers
 */
class PlaygroundObserver
{

    public function created(Playground $playground)
    {
        $this->syncSurfaces($playground);
        $this->syncServices($playground);
        $this->syncEquipments($playground);
        $this->syncSports($playground);
    }

    public function saving(Playground $playground)
    {
        if($playground->id){
            $this->syncSurfaces($playground);
            $this->syncServices($playground);
            $this->syncEquipments($playground);
            $this->syncSports($playground);
        }
    }

    protected function syncSurfaces(Playground $playground)
    {
        $value = $playground->getSurfacesToSave();
        if (!$value)
            return;

        if (is_array($value) && sizeof($value) > 0 && is_array($value[0])) {
            $surface_ids = [];
            foreach($value as $surface) {
                if ($surface['id']) {
                    $surface_ids[] = $surface['id'];
                } else {
                    $newSurface = Surface::create($surface);
                    $surface_ids[] = $newSurface['id'];
                }
            }

            $playground->surfaces()->sync($surface_ids);
        } else {
            $playground->surfaces()->sync($value);
        }
    }

    protected function syncServices(Playground $playground)
    {
        $value = $playground->getServicesToSave();
        if (!$value)
            return;

        $oldServices = [];
        $newServices = [];
        foreach ($value as $service) {
            if (isset($service['id']) && !empty($service['id'])) {
                $oldServices[] = $service;
            } else {
                $newServices[] = $service;
            }
        }

        $playground->services()->sync(array_reduce($oldServices, function($acc, $data) {
            $price = isset($data['price']) ? $data['price'] : null;
            $unit = isset($data['unit']) ? $data['unit'] : null;
            $payable = isset($data['payable']) ? $data['payable'] : null;

            $acc[$data['id']] = ['price' => $price, 'unit' => $unit, 'payable'=>$payable];

            return $acc;
        }, []));

        $newCreatedServices = [];
        foreach ($newServices as $data) {
            $price = isset($data['price']) ? $data['price'] : null;
            $unit = isset($data['unit']) ? $data['unit'] : null;
            $payable = isset($data['payable']) ? $data['payable'] : null;

            $c = Service::create(['name' => $data['name']]);
            $newCreatedServices[$c->id] = [
                'price' => $price,
                'unit' => $unit,
                'payable'=>$payable
            ];
        }

        $playground->services()->attach($newCreatedServices);
        $playground->load('services');
    }

    protected function syncEquipments(Playground $playground)
    {
        $value = $playground->getEquipmentsToSave();
        if (!$value)
            return;

        $oldEquipments = [];
        $newEquipments = [];
        foreach ($value as $service) {
            if (isset($service['id']) && !empty($service['id'])) {
                $oldEquipments[] = $service;
            } else {
                $newEquipments[] = $service;
            }
        }

        $playground->equipments()->sync(array_reduce($oldEquipments, function($acc, $data) {
            $price = isset($data['price']) ? $data['price'] : null;
            $unit = isset($data['unit']) ? $data['unit'] : null;
            $payable = isset($data['payable']) ? $data['payable'] : null;

            $acc[$data['id']] = ['price' => $price, 'unit' => $unit,'payable'=>$payable];

            return $acc;
        }, []));

        $newCreatedEquipments = [];
        foreach ($newEquipments as $data) {
            $price = isset($data['price']) ? $data['price'] : null;
            $unit = isset($data['unit']) ? $data['unit'] : null;
            $payable = isset($data['payable']) ? $data['payable'] : null;

            $c = Equipment::create(['name' => $data['name']]);
            $newCreatedEquipments[$c->id] = [
                'price' => $price,
                'unit' => $unit,
                'payable'=>$payable
            ];
        }

        $playground->equipments()->attach($newCreatedEquipments);
        $playground->load('services');
    }

    protected function syncSports(Playground $playground)
    {
        $value = $playground->getSportsToSave();
        if (!$value)
            return;

        $labels = [];
        $sports = Sport::query()->whereIn('id', $value)->orderBy(DB::raw('nlevel(label)'))->get();
        $value = [];
        foreach ($sports as $sport) {
            $labels[] = $sport->label;
            $value[] = $sport->id;
        }

        $playground->sports()->sync($value);
    }
}
