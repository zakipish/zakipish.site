<?php

namespace App\Observers;

use App\Models\BusyRecord;
use App\Models\Utils\BusyRecordableInterface;
use Illuminate\Support\Facades\Log;

use Illuminate\Validation\ValidationException;

class BusyRecordableObserver
{
    public function saving(BusyRecordableInterface $busyRecordable) {
        $this->checkCrossing($busyRecordable);
    }

    public function saved(BusyRecordableInterface $busyRecordable)
    {
        Log::debug('Block schedule: ');
        Log::debug($busyRecordable->blockSchedule);

        // создать или обновить busy record
        if ($busyRecordable->blockSchedule) {
            $this->createOrUpdateBusyRecord($busyRecordable);
        } else {
            $this->removeBusyRecord($busyRecordable);
        }
    }

    protected function createOrUpdateBusyRecord(BusyRecordableInterface $busyRecordable)
    {
        if ($busyRecordable->busyRecord) {
            $busyRecordable->busyRecord->save();
        } else {
            $busyRecordable->busyRecord()->create([
                'playground_id' => $busyRecordable->playground_id,
                'date' => $busyRecordable->date,
                'interval' => $busyRecordable->interval
            ]);
        }
    }

    protected function removeBusyRecord(BusyRecordableInterface $busyRecordable)
    {
        if ($busyRecordable->busyRecord)
            $busyRecordable->busyRecord->delete();
    }

    protected function checkCrossing(BusyRecordableInterface $busyRecordable)
    {
        if (!$busyRecordable->blockSchedule)
            return true;

        $crossingBusyRecords = BusyRecord::query()
            ->where('date', $busyRecordable->date)
            ->where('interval', '&&', $busyRecordable->postgresInterval)
            ->where('playground_id', '=', $busyRecordable->playground_id);

        if ($busyRecordable->exists) {
            $crossingBusyRecords->where('busy_recordable_id', '!=', $busyRecordable->id);
            $crossingBusyRecords->where('busy_recordable_type', '!=', $busyRecordable->getMorphClass());
        }

        if ($crossingBusyRecords->exists()) {
            throw ValidationException::withMessages([
                'interval' => ['Зал занят на это время.']
            ]);
        }
    }
}
