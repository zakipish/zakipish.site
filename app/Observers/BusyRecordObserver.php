<?php

namespace App\Observers;

use Carbon\Carbon;

use App\Models\BusyRecord;

use Illuminate\Validation\ValidationException;

class BusyRecordObserver
{
    public function saving(BusyRecord $busyRecord)
    {
        $this->checkIntervalMultiplicity($busyRecord);
        $this->checkCrossing($busyRecord);
    }

    protected function checkIntervalMultiplicity(BusyRecord $busyRecord)
    {
        $duration = ($busyRecord->timeEnd->getTimestamp() - $busyRecord->timeStart->getTimestamp()) / 60;
        $minInterval = $busyRecord->playground->min_interval;

        if (!$minInterval)
            throw ValidationException::withMessages([
                'interval' => ['У зала № ' . $busyRecord->playground_id . ' "' . $busyRecord->playground->name . '" не указан временной интервал.']
            ]);

        if (fmod($duration, $minInterval) != 0)
            throw ValidationException::withMessages([
                'interval' => ['Время должно быть кратно ' . $minInterval . ' мин.']
            ]);
    }

    protected function checkCrossing(BusyRecord $busyRecord)
    {
        $crossingBusyRecords = BusyRecord::query()
            ->where('date', $busyRecord->date)
            ->where('interval', '&&', $busyRecord->postgresInterval)
            ->where('id', '!=', $busyRecord->id)
            ->where('playground_id', '=', $busyRecord->playground_id)
            ->first();

        if ($crossingBusyRecords) {
            throw ValidationException::withMessages([
                'interval' => ['Зал занят на это время.']
            ]);
        }
    }
}
