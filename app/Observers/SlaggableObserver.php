<?php

namespace App\Observers;

use App\Models\Slug;
use App\Models\Utils\SluggableInterface;
use App\Services\SlugService;
use Illuminate\Support\Facades\Log;

/**
 * Обзервер для сохранения слагов. Основные принципы:
 * слаг должен быть уникальным для своего класса;
 * все новые значения слагов сохраняем отдельными записями, чтобы со старых слагов редиректить на новые;
 * если новое значение слага уже записано в таблице, то меняем этой записи активность,
 * предыдущую активную делаем неактивной.
 *
 * Class SlaggableObserver
 * @package App\Observers
 */
class SlaggableObserver
{

    /** @var SlugService $slugService */
    protected $slugService;

    public function __construct()
    {
        $this->slugService = app(SlugService::class);
    }

    /**
     * Handle the user "created" event.
     *
     * @param SluggableInterface $slaggable
     * @return void
     */
    public function saved(SluggableInterface $sluggable)
    {
        if (!empty($sluggable->name) || !empty($sluggable->title)) {
            // Если у модели есть имя
            $this->slugService->generateSlug($sluggable);
        }
    }

}
