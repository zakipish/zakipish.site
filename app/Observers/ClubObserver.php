<?php

namespace App\Observers;

use App\Models\Club;

/**
 * Class PlaygroundObserver
 * @package App\Observers
 */
class ClubObserver
{
    public function created(Club $club)
    {
        $infrastructures = $club->getInfrastructuresToSave();
        if ($infrastructures) {
            $club->infrastructures()->sync($infrastructures);
        }
        $sports = $club->getSportsToSave();
        if ($sports) {
            $club->sports()->sync($sports);
        }
    }
}
