<?php

namespace App\Observers;

use App\Models\User;
use Illuminate\Support\Facades\Log;

class UserObserver
{
    public function created(User $user)
    {
        $sports = $user->getSportsToSave();
        if ($sports) {
            $user->sports()->sync($sports);
        }
    }

    public function updated(User $user)
    {
        $sports = $user->getSportsToSave();
        if ($sports) {
            $user->sports()->sync($sports);
        }
    }
}
