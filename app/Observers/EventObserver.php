<?php

namespace App\Observers;

use App\Models\Event;
use App\Models\Transaction;
use Illuminate\Support\Facades\Log;

use Illuminate\Validation\ValidationException;

class EventObserver
{
    public function saved(Event $event)
    {
        $this->checkGameChat($event);
        $this->checkPlace($event);
        $this->syncImages($event);
        $this->syncUsers($event);
    }

    public function created(Event $event)
    {
        $this->createPayment($event);
    }

    protected function createPayment(Event $event)
    {
        $club = $event->club;
        $club->transactions()->create([
            'type' => Transaction::TYPE_EVENT_PAYMENT,
            'value' => -1 * $club->event_price
        ]);
    }

    protected function checkGameChat(Event $event)
    {
        if ($event->type == Event::TYPE_GAME)
            $event->chat_disabled = false;
    }

    protected function checkPlace(Event $event)
    {
        if ($event->type == Event::TYPE_GAME) {
            if (!$event->playground_id) {
                throw ValidationException::withMessages([
                    'playground_id' => ['Обязательное поле']
                ]);
            }
        }

        if ($event->type == Event::TYPE_MEETING) {
            if (!$event->playground_id && !$event->coordinates) {
                throw ValidationException::withMessages([
                    'place' => ['Обязательное поле']
                ]);
            }
        }
    }

    protected function syncImages(Event $event)
    {
        $images = $event->getImagesForSave();
        $images = array_sort($images, function ($image) {
            return $image['order'];
        });

        if ($images) {
            $syncImages = array_reduce($images, function ($acc, $image) {
                $lastImage = array_last($acc);
                $order = $lastImage ? $lastImage['order'] + 1 : 1;
                $acc[$image['id']] = [
                    'order' => $order
                ];

                return $acc;
            }, []);
            $event->images()->sync($syncImages);
            $event->load('images');
        }
    }

    protected function syncUsers(Event $event)
    {
        $users = $event->getUsersToSync();
        $event->users()->sync($users);
        $event->load('users');
    }
}
