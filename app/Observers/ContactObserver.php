<?php

namespace App\Observers;

use App\Models\Contact;

class ContactObserver
{
    public function saving(Contact $contact)
    {
        $matches = [];
        preg_match('/(https?:\/\/)?(.*)/', $contact->value, $matches);
        if (sizeof($matches) > 0) {
            $contact->value = $matches[2];
        }
    }
}
