<?php

namespace App\Observers;

use App\Models\Chat;
use App\Models\Utils\ChatableInterface;

class ChatableObserver
{
    public function saved(ChatableInterface $chatable)
    {
        $this->createChat($chatable);
    }

    protected function createChat(ChatableInterface $chatable)
    {
        if ($chatable->needToCreateChat && !$chatable->chat) {
            $chatable->chat()->create();
        }
    }
}
