<?php

namespace App\Listeners;

use App\Events\ClubChanged;
use App\Jobs\SyncAmoClub;
use App\Models\Club;
use App\Models\Tasks\ClubAmoCrmSyncTask;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class ClubChangedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ClubChanged  $event
     * @return void
     */
    public function handle(ClubChanged $event)
    {
        if(config('amo.sync_enable') == false)
            return;

        $club = $event->club;

        if(!in_array($club->rank, [Club::RANK_1, Club::RANK_2, Club::RANK_3]))
            return;

        $task = ClubAmoCrmSyncTask::create([
            'club_id'   => $club->id
        ]);
        Log::info($task->id . ' ' . $task->getQueueName());
        SyncAmoClub::dispatch($task->id)->onQueue($task->getQueueName());
    }
}
