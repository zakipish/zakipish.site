<?php

namespace App\Http\Resources\CRM;


use Illuminate\Http\Resources\Json\JsonResource;

class UserSearchResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var BookingType $resource */
        $resource = $this->resource;
        return [
            'id'    => $resource->id,
            'phone' => $resource->phone,
            'name'  => $resource->name,
            'lastName'  => $resource->lastName,
            'midName'  => $resource->midName,
        ];
    }
}
