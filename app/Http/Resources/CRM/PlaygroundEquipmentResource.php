<?php

namespace App\Http\Resources\CRM;

use Illuminate\Http\Resources\Json\JsonResource;
use Jenssegers\Date\Date;

class PlaygroundEquipmentResource extends JsonResource
{
    public function toArray($request)
    {
        $resource = $this->resource;
        return [
            'id' => $resource->pivot->id,
            'equipment_id' => $resource->id,
            'name' => $resource->name,
            'unit' => $resource->pivot->unit,
            'price' => $resource->pivot->price
        ];
    }
}
