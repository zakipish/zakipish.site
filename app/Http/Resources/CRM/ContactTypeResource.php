<?php

namespace App\Http\Resources\CRM;

use App\Models\ContactType;
use Illuminate\Http\Resources\Json\JsonResource;

class ContactTypeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var ContactType $resource */
        $resource = $this->resource;
        return [
            'id' => $resource->id,
            'slug' => $resource->slug,
            'name' => $resource->name,
        ];
    }
}
