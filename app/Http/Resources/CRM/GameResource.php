<?php

namespace App\Http\Resources\CRM;

use App\Models\Game;
use Illuminate\Http\Resources\Json\JsonResource;
use \DateTime;

class GameResource extends JsonResource
{
    public function toArray($request)
    {
        $resource = $this->resource;

        return [
            'id' => $resource->id,
            'name' => $resource->name,
            'description' => $resource->description,

            'price' => $resource->price,

            'date' => $resource->date ? (new DateTime($resource->date))->format('Y-m-d') : null,
            'interval' => $resource->interval,

            'owner' => new OwnerResource($resource->owner),
            'owner_id' => $resource->owner_id,
            'sport_id' => $resource->sport_id,
        ];
    }
}
