<?php

namespace App\Http\Resources\CRM;

use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;

class PlaygroundTinyResource extends JsonResource
{
    public function toArray($request)
    {
        $resource = $this->resource;
        return [
            'id' => $resource->id,
            'name' => $resource->name,
        ];
    }
}
