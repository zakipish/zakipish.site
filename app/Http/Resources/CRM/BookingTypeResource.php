<?php

namespace App\Http\Resources\CRM;


use App\Models\BookingType;
use Illuminate\Http\Resources\Json\JsonResource;

class BookingTypeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var BookingType $resource */
        $resource = $this->resource;
        return [
            'id'    => $resource->id,
            'type'  => $resource->type,
            'status'  => $resource->status,
            'comment'  => $resource->comment,
        ];
    }
}
