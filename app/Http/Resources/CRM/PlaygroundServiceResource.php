<?php

namespace App\Http\Resources\CRM;

use App\Models\Service;
use Illuminate\Http\Resources\Json\JsonResource;

class PlaygroundServiceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var Service $resource */
        $resource = $this->resource;
        return [
            'id' => $resource->pivot->id,
            'service_id' => $resource->id,
            'price' => $resource->pivot->price,
            'payable' => $resource->pivot->payable,
            'unit' => $resource->pivot->unit,
            'name' => $resource->name,
            'description' => $resource->description,
        ];
    }
}
