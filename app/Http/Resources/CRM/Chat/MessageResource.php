<?php

namespace App\Http\Resources\CRM\Chat;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;
use Jenssegers\Date\Date;

class MessageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $resource = $this->resource;
        return [
            'id'        => $resource->id,
            'message'   => $resource->message,
            'type'      => $resource->type,
            'own'       => Auth::check() && Auth::id() == $resource->user_id,
            'time'      => Date::parse($this->created_at)->format('H:i'),
            'date'      => Date::parse($this->created_at)->format('d.m.Y'),
            'user'      => !empty($resource->user) ? [
                'id'    => $resource->user->id,
                'name'  => $resource->user->name,
                'color' => $resource->color()
            ] : []
        ];
    }
}
