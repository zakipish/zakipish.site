<?php

namespace App\Http\Resources\CRM;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

class PlannerResource extends JsonResource
{
    public function toArray($request)
    {
        $resource = $this->resource;

        return [
            'timeStart' => isset($resource['timeStart']) ? $resource['timeStart']->format('H:i') : null,
            'timeEnd' => isset($resource['timeEnd']) ? $resource['timeEnd']->format('H:i') : null,
            'interval' => isset($resource['interval']) ? $resource['interval'] : null,
            'dates' => isset($resource['dates']) ? DatePriceResource::collection(collect($resource['dates'])) : null
        ];
    }
}
