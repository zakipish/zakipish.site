<?php

namespace App\Http\Resources\CRM;

use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;
use \DateTime;

class OwnerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var User $resource */
        $resource = $this->resource;
        return [
            'id'                => $resource->id,
            'email'             => $resource->email,
            'name'              => $resource->name,
            'avatar_url'        => $resource->avatarHeaderUrl
        ];
    }
}
