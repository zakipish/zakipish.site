<?php

namespace App\Http\Resources\CRM;

use Illuminate\Http\Resources\Json\JsonResource;
use Jenssegers\Date\Date;

class EquipmentResource extends JsonResource
{
    public function toArray($request)
    {
        $resource = $this->resource;
        return [
            'id' => $resource->id,
            'name' => $resource->name,
            'unit' => $resource->pivot ? $resource->pivot->unit : null,
            'price' => $resource->pivot ? $resource->pivot->price : null
        ];
    }
}
