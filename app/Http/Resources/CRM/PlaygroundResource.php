<?php

namespace App\Http\Resources\CRM;

use App\Http\Resources\CRM\Playground\LogoResource as PlaygroundLogoResource;
use App\Http\Resources\CRM\Playground\ImageResource as PlaygroundImageResource;
use Illuminate\Http\Resources\Json\JsonResource;
use Carbon\Carbon;

class PlaygroundResource extends JsonResource
{

    public function toArray($request)
    {
        $resource = $this->resource;

        return [
            'id' => $resource->id,
            'name' => $resource->name,
            'type' => $resource->type,
            'description' => $resource->description,
            'avatar' => new PlaygroundLogoResource($resource->avatar),
            'avatar_url' => $resource->avatarUrl,
            'surfaces' => SurfaceResource::collection($resource->surfaces),
            'services' => PlaygroundServiceResource::collection($resource->services),
            'images' => PlaygroundImageResource::collection($resource->images),
            'sport_ids' => $resource->sports->pluck('id'),
            'equipments' => PlaygroundEquipmentResource::collection($resource->equipments),
            'min_interval' => $resource->min_interval,
            'updated_at' => (new Carbon($resource->updated_at))->toIso8601String(),
            'club_id' => $resource->club_id,
            'isMinIntervalSetted' => $resource->isMinIntervalSetted,
            'hasFeatureSchedules'  => $resource->hasFeatureSchedules
            // 'club' => new ClubResource($resource->club)
        ];
    }
}
