<?php

namespace App\Http\Resources\CRM\Club;

use App\Models\Image;
use App\Services\UploadService;
use Illuminate\Http\Resources\Json\JsonResource;

class LogoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var Image $resource */
        $resource = $this->resource;
        $result = [
            'id' => $resource->id,
            'url' => $resource->getUrl('sq100')
        ];
        if ($resource->pivot) {
            $result['default'] = $resource->pivot->main;
            $result['order'] = $resource->pivot->order;
        }
        return $result;
    }
}
