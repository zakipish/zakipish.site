<?php

namespace App\Http\Resources\CRM;

use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;

class ClubTinyResource extends JsonResource
{
    public function toArray($request)
    {
        $resource = $this->resource;
        return [
            'id' => $resource->id,
            'name' => $resource->name,
            'address' => $resource->address,
            'coordinates' => $resource->coordinates,
            'logo' => new ImageResource($resource->logo),
            'playgrounds' => PlaygroundTinyResource::collection($resource->playgrounds),
            'balance' => $resource->balance,
            'sports' => SportResource::collection($resource->sports),
        ];
    }
}
