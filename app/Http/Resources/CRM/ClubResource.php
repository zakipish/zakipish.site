<?php

namespace App\Http\Resources\CRM;

use App\Models\Club;
use App\Http\Resources\CRM\Club\LogoResource as ClubLogoResource;
use App\Http\Resources\CRM\Club\ImageResource as ClubImageResource;
use Jenssegers\Date\Date;
use Illuminate\Http\Resources\Json\JsonResource;
use Carbon\Carbon;

class ClubResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var Club $resource */
        $resource = $this->resource;
        return [
            'id'                   => $resource->id,
            'owner'                => new UserResource($resource->owner),
            'name'                 => $resource->name,
            'updated_at'           => (new Carbon($resource->updated_at))->toIso8601String(),
            'description'          => $resource->description,

            'infrastructures'      => InfrastructureResource::collection($resource->infrastructures),
            'infrastructure_ids'   => $resource->infrastructures->pluck('id'),
            'contacts'             => ContactResource::collection($resource->contacts),
            'services'             => ServiceResource::collection($resource->services),
            'coordinates'          => $resource->coordinates,

            'address'              => $resource->address,
            'club_chain_id'        => $resource->club_chain_id,
            'club_chain'           => new ClubChainResource($this->clubChain),
            'rank'                 => $resource->rank,
            'source_link'          => $resource->source_link,
            'country'              => $resource->country,
            'region'               => $resource->region,
            'city'                 => new CityResource($resource->city),
            'city_id'              => $resource->city_id,
            'department'           => $resource->department,
            'logo'                 => new ClubLogoResource($resource->logo),
            'images'               => ClubImageResource::collection($resource->images),
            'type'                 => $resource->type,
            'source'               => $resource->source,
            'booking_types'        => BookingTypeResource::collection($resource->bookingTypes),
            'moderators'           => ClubUserResource::collection($resource->moderators),

            'company_name'         => $resource->company_name,
            'OGRN'                 => $resource->OGRN,
            'INN'                  => $resource->INN,
            'KPP'                  => $resource->KPP,
            'entity_address'        => $resource->entity_address,

            'post_index'           => $resource->post_index,
            'post_street'          => $resource->post_street,
            'post_building'        => $resource->post_building,
            'post_building_number' => $resource->post_building_number,

            'entity_boss_name'     => $resource->entity_boss_name,

            'registration_date'    => $resource->registration_date,
            'registration_certificate'    => $resource->registration_certificate,

            'bank_name'            => $resource->bank_name,
            'BIK'                  => $resource->BIK,
            'company_account'      => $resource->company_account,
            'number_rs'            => $resource->number_rs,
            'NDS'                  => $resource->NDS,

            //TODO wtf? + rename to address
            'entity'               => $resource->entity,
            'entity_boss_position' => $resource->entity_boss_position,

            'playgrounds'          => PlaygroundResource::collection($resource->playgrounds),
            'hasFeatureSchedules'  => $resource->hasFeatureSchedules

        ];
    }
}
