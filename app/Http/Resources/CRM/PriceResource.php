<?php

namespace App\Http\Resources\CRM;

use Illuminate\Http\Resources\Json\JsonResource;
use Carbon\Carbon;

class PriceResource extends JsonResource
{
    public function toArray($request)
    {
        $resource = $this->resource;

        return [
            'timeStart' => $resource['timeStart']->format("H:i"),
            'timeEnd' => $resource['timeEnd']->format("H:i"),
            'value' => $resource['value'],
            'state' => $resource['state'] ? 'open' : 'closed'
        ];
    }
}
