<?php

namespace App\Http\Resources\Front;

use App\Http\Resources\Front\Club\SliderImageResource as ClubSliderImageResource;
use App\Helpers\ContactHelper;
use Illuminate\Http\Resources\Json\JsonResource;

class ClubWithoutPlaygroundResource extends JsonResource
{
    public function toArray($request)
    {
        $resource = $this->resource;
        return [
            'id'            => $resource->id,
            'slug'          => $resource->slug,
            'name'          => $resource->name,
            'address'       => $resource->address,
            'coordinates'       => $resource->coordinates,
            'description'   => $resource->description,
            'avatar_url' => $resource->avatarUrl,
            //TODO: check wtf is type_id: 3
            'contacts'      => ContactResource::collection($resource->contacts),
            'services'      => ServiceResource::collection($resource->services),
            'logo'         => new ImageResource($resource->logo),
            'images' => ClubSliderImageResource::collection($resource->images),
            'sports'  => SportResource::collection($resource->sports),
            //TODO: check wtf is sportList
            'sportList'  => $resource->getSportsList(),
            'infrastructures' => InfrastructureResource::collection($resource->infrastructures),
            'avatar' => new ImageResource($resource->avatar),
            'logo_url' => $resource->logo? $resource->logo->getUrl('sq40'):null,
            'big_logo_url' => $resource->logo? $resource->logo->getUrl('sq160'):null,
            'weekSchedule' => $resource->thisWeekScheduleInterval,
            'weekDaysSchedule' => $resource->thisWeekScheduleWeekdaysInterval,
            'weekEndSchedule' => $resource->thisWeekScheduleWeekendInterval,
        ];
    }
}
