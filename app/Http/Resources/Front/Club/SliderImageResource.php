<?php

namespace App\Http\Resources\Front\Club;

use Illuminate\Http\Resources\Json\JsonResource;

class SliderImageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var Image $resource */
        $resource = $this->resource;
        $result = [
            'id' => $resource->id,
            'url' => $resource->getUrl('wl675')
        ];
        if ($resource->pivot) {
            $result['default'] = $resource->pivot->main;
            $result['order'] = $resource->pivot->order;
        }
        return $result;
    }
}
