<?php

namespace App\Http\Resources\Front;

use App\Http\Resources\Front\ImageResource;
use App\Http\Resources\Front\SportResource;
use App\Helpers\ContactHelper;
use App\Models\Playground;
use App\Models\Sport;
use Illuminate\Http\Resources\Json\JsonResource;
use Jenssegers\Date\Date;

class PlaygroundWithoutClubResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var Playground $resource */
        $resource = $this->resource;

        return [
            'id' => $resource->id,
            'slug' => $resource->slug,
            'name' => $resource->name,
            'type' => $resource->type,
            'description' => $resource->description,
            'avatar_url' => $resource->avatarUrl,
            'surfaces' => SurfaceResource::collection($resource->surfaces),
            'images' => ImageResource::collection($resource->images),
            'sports' => SportResource::collection($resource->sports),
            'equipments' => EquipmentResource::collection($resource->equipments)
        ];
    }
}
