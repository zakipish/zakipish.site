<?php

namespace App\Http\Resources\Front;

use Illuminate\Http\Resources\Json\JsonResource;
use Carbon\Carbon;

class DatePriceResource extends JsonResource
{
    public function toArray($request)
    {
        $resource = $this->resource;

        return [
            'date' => $resource['date'] ? $resource['date']->toDateString() : null,
            'prices' => PriceResource::collection(collect($resource['prices']))
        ];
    }
}
