<?php

namespace App\Http\Resources\Front\Event;

use App\Http\Resources\Front\UserSportResource;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    public function toArray($request)
    {
        /** @var User $resource */
        $resource = $this->resource;
        return [
            'id' => $resource->id,
            'name' => $resource->name,
            'type' => $resource->pivot->type,
            'lastName' => $resource->lastName,
            'avatar_url' => $resource->avatarUrl,
            'avatar_thumb_url' => $resource->avatarThumbUrl,
            'avatar_big_thumb_url' => $resource->avatarBigThumbUrl,
            'avatar_header_url' => $resource->avatarHeaderUrl,
            'sports' => UserSportResource::collection($resource->sports)
        ];
    }
}
