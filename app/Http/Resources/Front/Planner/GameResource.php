<?php

namespace App\Http\Resources\Front\Planner;

use App\Models\Game;
use Illuminate\Http\Resources\Json\JsonResource;
use \DateTime;

class GameResource extends JsonResource
{
    public function toArray($request)
    {
        $resource = $this->resource;

        return [
            'id' => $resource->id,
            'name' => $resource->name,
            'description' => $resource->description,

            'price' => $resource->price,
            'date' => $resource->date ? (new DateTime($resource->date))->format('Y-m-d') : null,
            'interval' => $resource->interval,

            'min_players' => $resource->min_players,
            'max_players' => $resource->max_players,
            'players_count' => $resource->players_count
        ];
    }
}
