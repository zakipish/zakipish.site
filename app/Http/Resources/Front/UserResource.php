<?php

namespace App\Http\Resources\Front;

use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;
use \DateTime;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var User $resource */
        $resource = $this->resource;
        return [
            'id'                => $resource->id,
            'email'             => $resource->email,
            'name'              => $resource->name,
            'phone'             => $resource->phone,
            'role'              => $resource->role,
            'lastName'          => $resource->lastName,
            'midName'           => $resource->midName,
            'birthDate'         => $resource->birthDate ? (new DateTime($resource->birthDate))->format(DateTime::ISO8601) : null,
            'city_id'           => $resource->city_id,
            'about'             => $resource->about,
            'height'            => $resource->height,
            'weight'            => $resource->weight,
            'gender'            => $resource->gender,
            'avatar_url'        => $resource->avatarUrl,
            'avatar_thumb_url'  => $resource->avatarThumbUrl,
            'avatar_header_url' => $resource->avatarHeaderUrl,
            'social_accounts'   => SocialAccountResource::collection($resource->socialAccounts),
            'sports'            => UserSportResource::collection($resource->sportsDesc),
            'friend_request'    => isset($resource->friendRequest) ? new FriendRequestResource($resource->friendRequest) : null,
            'friends'           => FriendResource::collection($resource->friends()),
            'tmp'               => $resource->tmp,
        ];
    }
}
