<?php

namespace App\Http\Resources\Front;

use App\Models\Playground;
use Illuminate\Http\Resources\Json\JsonResource;
use Jenssegers\Date\Date;

class SurfaceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var Playground $resource */
        $resource = $this->resource;
        return [
            'id' => $resource->id,
            'name' => $resource->name
        ];
    }
}
