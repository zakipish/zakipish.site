<?php

namespace App\Http\Resources\Front;

use Illuminate\Http\Resources\Json\JsonResource;
use Carbon\Carbon;

class PriceResource extends JsonResource
{
    public function toArray($request)
    {
        $resource = $this->resource;

        return [
            'timeStart' => $resource['startTime']->format("H:i"),
            'timeEnd' => $resource['endTime']->format("H:i"),
            'price' => $resource['price'],
            'state' => $resource['state'] ? 'open' : 'closed'
        ];
    }
}
