<?php

namespace App\Http\Resources\Front;

use App\Helpers\ContactHelper;
use Illuminate\Http\Resources\Json\JsonResource;

class FilterPlaygroundResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $resource = $this->resource;
        return [
            'id' => $resource->id,
            'slug' => $resource->slug,
            'name' => $resource->name,
            'club' => [
                'slug' => $resource->club->slug,
                'address' => $resource->club->address,
                'coordinates' => $resource->club->coordinates,
            ],
            'avatar_mid_url' => $resource->avatarMidUrl,
        ];
    }
}
