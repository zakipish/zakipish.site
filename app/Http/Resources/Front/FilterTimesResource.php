<?php

namespace App\Http\Resources\Front;

use Illuminate\Http\Resources\Json\JsonResource;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class FilterTimesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var Playground $resource */
        $resource = $this->resource;
        $start = $resource['start']->toIso8601String();
        $end = $resource['end']->toIso8601String();

        return [
            'start' => $start,
            'end' => $end,
            'status' => $resource['status'],
            'price' => $resource['price']
        ];
    }
}
