<?php

namespace App\Http\Resources\Front;

use App\Models\Service;
use Illuminate\Http\Resources\Json\JsonResource;

class ServiceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var Service $resource */
        $resource = $this->resource;
        return [
            'id' => $resource->id,
            'slugs' => SlugResource::collection($resource->slugs),
            'slug' => $resource->slug,
            'name' => $resource->name,
            'description' => $resource->description,
            'unit' => $resource->pivot->unit,
            'price' => $resource->pivot->price
        ];
    }
}
