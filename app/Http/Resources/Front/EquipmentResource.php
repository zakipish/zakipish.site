<?php

namespace App\Http\Resources\Front;

use App\Http\Resources\ImageResource;
use App\Http\Resources\SportResource;
use App\Models\Playground;
use App\Models\Sport;
use Illuminate\Http\Resources\Json\JsonResource;
use Jenssegers\Date\Date;

class EquipmentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var Playground $resource */
        $resource = $this->resource;
        return [
            'id' => $resource->id,
            'name' => $resource->name,
            'unit' => $resource->pivot->unit,
            'price' => $resource->pivot->price
        ];
    }
}
