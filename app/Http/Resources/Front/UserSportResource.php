<?php

namespace App\Http\Resources\Front;

use Illuminate\Http\Resources\Json\JsonResource;

class UserSportResource extends JsonResource
{
    public function toArray($request)
    {
        $resource = $this->resource;
        return [
            'id'          => $resource->id,
            'name'        => $resource->name,
            'description' => $resource->description,
            'label'       => $resource->label,
            'icon_url' => $resource->iconUrl,

            'game_level_id'  => $resource->pivot->game_level_id,
            'game_level'  => new GameLevelResource($resource->pivot->gameLevel),
            'game_exp'    => $resource->pivot->game_exp,
            'games_count' => $resource->pivot->games_count
        ];
    }
}
