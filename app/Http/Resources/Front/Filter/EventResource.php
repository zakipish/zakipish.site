<?php

namespace App\Http\Resources\Front\Filter;

use App\Http\Resources\Front\GameLevelResource;
use App\Http\Resources\Front\ImageResource;
use App\Http\Resources\Front\SportResource;

use Illuminate\Http\Resources\Json\JsonResource;

class EventResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $resource = $this->resource;

        return [
            'id'    => $resource->id,
            'name'  => $resource->name,
            'title' => $resource->title,
            'date'  => $resource->date,
            'interval'  => $resource->interval,
            'price' => $resource->price,
            'max_players'  => $resource->max_players,
            'min_players'  => $resource->min_players,
            'sport_id' => $resource->sport_id,
            'sport' => $resource->sport_id,
            'club' => new GameClubResource($resource->club),
            'gameLevel' => new GameLevelResource($resource->gameLevel),
            'sport' => new SportResource($resource->sport),
            'users' => new GameUserResource($resource->user),
            'images' => ImageResource::collection($resource->images),
            'address' => $resource->address,
            'coordinates' => $resource->coordinates,
            'slug' => $resource->slug
        ];
    }
}
