<?php

namespace App\Http\Resources\Front\Filter;

use Illuminate\Http\Resources\Json\ResourceCollection;

class GamesCollection extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $collection = $this->collection;

        return [
            'data' => GameResource::collection($this->collection['data']),
            'total' => $this->collection['total'],
        ];
    }
}
