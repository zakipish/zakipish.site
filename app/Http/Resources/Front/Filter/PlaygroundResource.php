<?php

namespace App\Http\Resources\Front\Filter;

use App\Http\Resources\Front\ImageResource;
use App\Helpers\ContactHelper;
use Illuminate\Http\Resources\Json\JsonResource;

class PlaygroundResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $resource = $this->resource;
        return [
            'id' => $resource->id,
            'slug' => $resource->slug,
            'avatar' => new ImageResource($resource->avatar),
            'min_price' => $resource['min_price'],
            'type' => $resource->type,
            'weekSchedule' => $resource->thisWeekScheduleInterval,
            'type' => $resource->type,
            'surfaces' => $resource->surfaces,
            'name' => $resource->name,
            'avatar_mid_url' => $resource->avatarMidUrl,
            'hasSchedule' => $resource->hasSchedule,
            'coordinates' => $resource->club->coordinates,
        ];
    }
}
