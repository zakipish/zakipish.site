<?php

namespace App\Http\Resources\Front\Filter;

use Illuminate\Http\Resources\Json\ResourceCollection;

class EventsCollection extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $collection = $this->collection;

        return [
            'data' => EventResource::collection($this->collection['data']),
            'total' => $this->collection['total'],
        ];
    }
}
