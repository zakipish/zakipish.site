<?php

namespace App\Http\Resources\Front\Filter;

use App\Http\Resources\Front\ImageResource;

use Illuminate\Http\Resources\Json\JsonResource;

class ClubResource extends JsonResource
{
    public function toArray($request)
    {
        $resource = $this->resource;
        return [
            'id'            => $resource->id,
            'slug'          => $resource->slug,
            'name'          => $resource->name,
            'address'       => $resource->address,
            'playgrounds'   => PlaygroundResource::collection($resource['playgrounds']),
            'logo' => new ImageResource($resource->logo),
        ];
    }
}
