<?php

namespace App\Http\Resources\Front\Filter;

use App\Http\Resources\Front\ImageResource;
use Illuminate\Http\Resources\Json\JsonResource;

class GameUserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $resource = $this->resource;

        return [
            'id' => $resource->id,
            'avatar_thumb_url' => $resource->avatar_thumb_url,
        ];
    }
}
