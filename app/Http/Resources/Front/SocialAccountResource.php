<?php

namespace App\Http\Resources\Front;

use App\Models\SocialAccount;
use Illuminate\Http\Resources\Json\JsonResource;

class SocialAccountResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var User $resource */
        $resource = $this->resource;
        return [
            'id'           => $resource->id,
            'provider'     => $resource->provider,
            'provider_id'  => $resource->provider_id,
            'social_email' => $resource->social_email,
            'profile_url'  => $resource->profileUrl()
        ];
    }
}
