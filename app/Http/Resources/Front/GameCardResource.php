<?php

namespace App\Http\Resources\Front;

use Illuminate\Http\Resources\Json\JsonResource;
use \DateTime;
use App\Http\Resources\Front\UserResource;

class GameCardResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var Playground $resource */
        $resource = $this->resource;
        return [
            'id' => $resource->id,
            'name' => $resource->name,
            'min_players' => $resource->min_players,
            'max_players' => $resource->max_players,
            'players_count' => $resource->playersCount,
            'price' => $resource->price,
            'date' => $resource->date ? (new DateTime($resource->date))->format('Y-m-d') : null,
            'interval' => $resource->interval,

            'users' => UserCardResource::collection($resource->users),
            'playground' => [
                'club' => [
                    'name' => $resource->playground->club->name,
                    'address' => $resource->playground->club->address
                ]
            ]
        ];
    }
}
