<?php

namespace App\Http\Resources\Front;

use App\Models\Game;
use Illuminate\Http\Resources\Json\JsonResource;
use Carbon\Carbon;
use App\Http\Resources\Front\UserResource;

class GameResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var Playground $resource */
        $resource = $this->resource;
        return [
            'id' => $resource->id,
            'name' => $resource->name,
            'description' => $resource->description,
            'min_players' => $resource->min_players,
            'max_players' => $resource->max_players,
            'age_start' => $resource->age_start,
            'age_end' => $resource->age_end,
            'game_level_id' => $resource->game_level_id,
            'price' => $resource->price,
            'users' => UserResource::collection($resource->users),
            'date' => $resource->date ? (new Carbon($resource->date))->toDateString() : null,
            'interval' => $resource->interval,
            'playground_id' => $resource->playground_id,
            'playground' => new PlaygroundResource($resource->playground),
            'owner_id' => $resource->owner_id,
            'chat_id' => $resource->chat ? $resource->chat->id : null,
        ];
    }
}
