<?php

namespace App\Http\Resources\Front;

use App\Models\ClubChain;
use Illuminate\Http\Resources\Json\JsonResource;

class ClubChainResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var ClubChain $resource */
        $resource = $this->resource;
        return [
            'id' => $resource->id,
            'slugs' => SlugResource::collection($resource->slugs),
            'slug' => $resource->slug,
            'name' => $resource->name,
            'owner' => new UserResource($resource->owner),
        ];
    }
}
