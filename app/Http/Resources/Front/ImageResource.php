<?php

namespace App\Http\Resources\Front;

use App\Models\Image;
use App\Services\UploadService;
use Illuminate\Http\Resources\Json\JsonResource;

class ImageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var Image $resource */
        $resource = $this->resource;
        $result = [
            'id' => $resource->id,
            'url' => $resource->getUrl()
        ];
        if ($resource->pivot) {
            $result['default'] = $resource->pivot->main;
            $result['order'] = $resource->pivot->order;
        }
        return $result;
    }
}
