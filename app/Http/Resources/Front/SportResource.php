<?php

namespace App\Http\Resources\Front;

use App\Models\Sport;
use Illuminate\Http\Resources\Json\JsonResource;

class SportResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var Sport $resource */
        $resource = $this->resource;
        return [
            'id' => $resource->id,
            'name' => $resource->name,
            'description' => $resource->description,
            'label' => $resource->label,
            'icon' => new ImageResource($resource->icon),
            'icon_url' => $resource->iconUrl,
            'photo_url' => $resource->photoUrl
        ];
    }
}
