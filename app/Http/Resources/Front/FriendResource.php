<?php

namespace App\Http\Resources\Front;

use Illuminate\Http\Resources\Json\JsonResource;

class FriendResource extends JsonResource
{
    public function toArray($request)
    {
        $resource = $this->resource;

        return [
            'id' => $resource->id,
            'name' => $resource->name,
            'lastName' => $resource->lastName,
            'midName' => $resource->midName,
            'avatar_thumb_url' => $resource->avatarThumbUrl,
            'avatar_big_thumb_url' => $resource->avatarBigThumbUrl
        ];
    }
}
