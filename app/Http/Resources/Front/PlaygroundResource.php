<?php

namespace App\Http\Resources\Front;

use App\Http\Resources\Front\ImageResource;
use App\Http\Resources\Front\Playground\SliderImageResource as PlaygroundSliderImageResource;
use App\Http\Resources\Front\SportResource;
use App\Helpers\ContactHelper;
use App\Models\Playground;
use App\Models\Sport;
use Illuminate\Http\Resources\Json\JsonResource;
use Jenssegers\Date\Date;

class PlaygroundResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $resource = $this->resource;
        return [
            'id' => $resource->id,
            'slug' => $resource->slug,
            'name' => $resource->name,
            'type' => $resource->type,
            'description' => $resource->description,
            'avatar_url' => $resource->avatarUrl,
            'surfaces' => SurfaceResource::collection($resource->surfaces),
            'services' => ServiceResource::collection($resource->services),
            'images' => PlaygroundSliderImageResource::collection($resource->images),
            'sports' => SportResource::collection($resource->sports),
            'equipments' => EquipmentResource::collection($resource->equipments),
            'club' => new ClubWithoutPlaygroundResource($resource->club),
            'weekSchedule' => $resource->thisWeekScheduleInterval,
            'weekDaysSchedule' => $resource->thisWeekScheduleWeekdaysInterval,
            'weekEndSchedule' => $resource->thisWeekScheduleWeekendInterval,
        ];
    }
}
