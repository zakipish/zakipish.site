<?php

namespace App\Http\Resources\Front;

use App\Helpers\ContactHelper;
use App\Http\Resources\Front\Club\ImageResource as ClubImageResource;
use App\Http\Resources\Front\Club\SliderImageResource as ClubSliderImageResource;
use App\Http\Resources\Front\ContactResource;
use App\Http\Resources\Front\ImageResource;
use App\Http\Resources\Front\ServiceResource;
use App\Http\Resources\Front\SlugResource;
use App\Http\Resources\Front\SportResource;
use App\Http\Resources\Front\UserResource;
use App\Models\Club;
use Illuminate\Http\Resources\Json\JsonResource;
use Jenssegers\Date\Date;

class ClubResource extends JsonResource
{
    public function toArray($request)
    {
        $resource = $this->resource;
        return [
            'id'            => $resource->id,
            'slug'          => $resource->slug,
            'name'          => $resource->name,
            'city'          => $resource->city ? $resource->city->name : '',
            'address'       => $resource->address,
            'coordinates'       => $resource->coordinates,
            'description'   => $resource->description,
            //TODO: check wtf is type_id: 3
            'contacts'      => ContactResource::collection($resource->contacts),
            'services'      => ServiceResource::collection($resource->services),
            'logo'         => new ImageResource($resource->logo),
            'images' => ClubSliderImageResource::collection($resource->images),
            'playgrounds'  => PlaygroundWithoutClubResource::collection($resource->playgrounds),
            'sports'  => SportResource::collection($resource->sports),
            //TODO: check wtf is sportList
            'sportList'  => $resource->getSportsList(),
            'infrastructures' => InfrastructureResource::collection($resource->infrastructures),
            'logo_url' => $resource->logo? $resource->logo->getUrl('sq160'):null,
            'photo_url' => $resource->logo? $resource->logo->getUrl('wl295'):null,
            'weekSchedule' => $resource->thisWeekScheduleInterval,
            'weekDaysSchedule' => $resource->thisWeekScheduleWeekdaysInterval,
            'weekEndSchedule' => $resource->thisWeekScheduleWeekendInterval,
        ];
    }
}
