<?php

namespace App\Http\Resources\Front;

use Illuminate\Http\Resources\Json\JsonResource;

class UserCardResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var User $resource */
        $resource = $this->resource;
        return [
            'id'                => $resource->id,
            'avatar_thumb_url'  => $resource->avatarThumbUrl,
            'sports'            => UserSportResource::collection($resource->sportsDesc),
        ];
    }
}
