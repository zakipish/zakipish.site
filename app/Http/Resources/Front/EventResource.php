<?php

namespace App\Http\Resources\Front;

use App\Http\Resources\Front\Event\UserResource as EventUserResource;
use App\Http\Resources\Front\Event\ImageResource as EventImageResource;
use Illuminate\Http\Resources\Json\JsonResource;

class EventResource extends JsonResource
{

    public function toArray($request)
    {
        $resource = $this->resource;
        return [
            'id' => $resource->id,
            'slug' => $resource->slug,
            'owner_id' => $resource->owner_id,
            'club_id' => $resource->club_id,
            'club' => new ClubWithoutPlaygroundResource($resource->club),
            'playground_id' => $resource->playground_id,
            'playground' => new PlaygroundWithoutClubResource($resource->playground),
            'sport_id' => $resource->sport_id,
            'sport' => new SportResource($resource->sport),
            'game_level_id' => $resource->game_level_id,
            'gameLevel' => new GameLevelResource($resource->gameLevel),
            'chat_disabled' => $resource->chat_disabled,
            'chat_id' => $resource->chat ? $resource->chat->id : null,
            'users' => EventUserResource::collection($resource->users),
            'max_players' => $resource->max_players,
            'min_players' => $resource->min_players,
            'age_start' => $resource->age_start,
            'age_end' => $resource->age_end,
            'title' => $resource->title,
            'description' => $resource->description,
            'price' => $resource->price,
            'type' => $resource->type,
            'coordinates' => $resource->coordinates,
            'images' => EventImageResource::collection($resource->images),
            'interval' => $resource->interval,
            'date' => $resource->date,
        ];
    }
}
