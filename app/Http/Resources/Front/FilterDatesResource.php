<?php

namespace App\Http\Resources\Front;

use Illuminate\Http\Resources\Json\JsonResource;
use Carbon\Carbon;

class FilterDatesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var Playground $resource */
        $resource = $this->resource;

        return [
            'date' => $resource['date'] ? $resource['date']->toDateString() : null,
            'count' => $resource['count']
        ];
    }
}
