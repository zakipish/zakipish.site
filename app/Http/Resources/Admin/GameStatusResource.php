<?php

namespace App\Http\Resources\Admin;

use App\Models\GameStatus;
use Illuminate\Http\Resources\Json\JsonResource;

class GameStatusResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var GameStatus $resource */
        $resource = $this->resource;
        return [
            'id' => $resource->id,
            'slug' => $resource->slug,
            'name' => $resource->name,
        ];
    }
}
