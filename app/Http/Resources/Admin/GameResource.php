<?php

namespace App\Http\Resources\Admin;

use App\Models\Game;
use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;

class GameResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var Game $resource */
        $resource = $this->resource;
        return [
            'id' => $resource->id,
            'slug' => $resource->slug,
            'name' => $resource->name,
            'description' => $resource->description,
            'min_players' => $resource->min_players,
            'max_players' => $resource->max_players,
            'playground' => $resource->playground,
            'playground_id' => $resource->playground_id,
            'sport' => $resource->sport,
            'sport_id' => $resource->sport_id,
            'game_status' => $resource->gameStatus,
            'game_status_id' => $resource->game_status_id,
            'game_level' => $resource->gameLevel,
            'game_level_id' => $resource->game_level_id,
            'users' =>  GameUserResource::collection($resource->users),
            'price'=> $resource->price,
            'interval' => $resource->interval,
            'date' => $resource->date,
            'city_id' => $resource->playground && $resource->playground->club ? $resource->playground->club->city_id : null,
        ];
    }
}
