<?php

namespace App\Http\Resources\Admin;

use App\Http\Resources\Admin\Playground\PlaygroundEquipmentResource;
use App\Http\Resources\Admin\Playground\PlaygroundServiceResource;
use App\Models\Playground;
use Illuminate\Http\Resources\Json\JsonResource;
use Jenssegers\Date\Date;
class PlaygroundResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var Playground $resource */
        $resource = $this->resource;
        return [
            'id' => $resource->id,
            'slugs' => SlugResource::collection($resource->slugs),
            'slug' => $resource->slug,
            'name' => $resource->name,
            'club' => new ClubResource($resource->club),
            'equipments' => PlaygroundEquipmentResource::collection($resource->equipments),
            'services' => PlaygroundServiceResource::collection($resource->services),
            'sports' => $resource->sports()->pluck('sports.id'),
            'surfaces' => $resource->surfaces()->pluck('surfaces.id'),
            'type'  => $resource->type,
            'description'   => $resource->description,
            'deleted_at'   =>$resource->deleted_at?Date::parse( $resource->deleted_at)->format('j F Y H:i'):$resource->deleted_at,
            'images'    => ImageResource::collection($resource->images),
            'min_interval' => $resource->min_interval,
            'avatar_url' => $resource->avatarUrl,
            'avatar_mid_url' => $resource->avatarMidUrl,
            'min_price' => $resource->min_price,
        ];
    }
}
