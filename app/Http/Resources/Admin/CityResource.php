<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class CityResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $resource = $this->resource;

        return [
            'id'    => $resource->id,
            'slugs' => SlugResource::collection($resource->slugs),
            'slug'  => $resource->slug,
            'name'  => $resource->name
        ];
    }
}
