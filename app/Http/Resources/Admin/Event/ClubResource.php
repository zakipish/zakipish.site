<?php

namespace App\Http\Resources\Admin\Event;

use Illuminate\Http\Resources\Json\JsonResource;

class ClubResource extends JsonResource
{
    public function toArray($request)
    {
        $resource = $this->resource;
        return [
            'id' => $resource->id,
            'name' => $resource->name,
            'playgrounds' => PlaygroundResource::collection($resource->playgrounds),
        ];
    }
}
