<?php

namespace App\Http\Resources\Admin\Event;

use Illuminate\Http\Resources\Json\JsonResource;

class PlaygroundResource extends JsonResource
{
    public function toArray($request)
    {
        $resource = $this->resource;
        return [
            'id' => $resource->id,
            'name' => $resource->name
        ];
    }
}
