<?php

namespace App\Http\Resources\Admin;

use App\Models\GameLevel;
use Illuminate\Http\Resources\Json\JsonResource;

class GameLevelResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var GameLevel $resource */
        $resource = $this->resource;
        return [
            'id' => $resource->id,
            'slug' => $resource->slug,
            'name' => $resource->name,
            'engName' => $resource->engName,
            'color' => $resource->color
        ];
    }
}
