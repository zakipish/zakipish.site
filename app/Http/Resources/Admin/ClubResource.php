<?php

namespace App\Http\Resources\Admin;

use App\Models\Club;
use Jenssegers\Date\Date;
use Illuminate\Http\Resources\Json\JsonResource;

class ClubResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var Club $resource */
        $resource = $this->resource;
        return [
            'id'            => $resource->id,
            'slugs'         => SlugResource::collection($resource->slugs),
            'slug'          => $resource->slug,
            'name'          => $resource->name,
            'event_price'  => $resource->event_price,
            'address'       => $resource->address,
            'description'   => $resource->description,
            'owner'         => new UserResource($resource->owner),
            'club_chain_id' => $resource->club_chain_id,
            'club_chain'    => $resource->club_chain_id? new ClubChainResource($this->clubChain):"",
            'last_update'    => Date::parse($resource->updated_at)->format('j F Y H:i'),
            'coordinates'   => $resource->coordinates,
            'contacts'      => ContactResource::collection($resource->contacts),
            'services'      => ServiceResource::collection($resource->services),
            'rank'          => $resource->rank,
            'source_link'   => $resource->source_link,
            'country'       => $resource->country,
            'region'        => $resource->region,
            'city'       => $resource->city ? $resource->city->name : '',
            'city_id'       => $resource->city_id,
            'department'    => $resource->department,
            'department_id'    => $resource->department_id,
            'logo'         => new ImageResource($resource->logo),
            'images' => ImageResource::collection($resource->images),
            'type'          => $resource->type,
            'infrastructures' => $resource->infrastructures()->pluck('infrastructures.id'),
            'amo_id'            => $resource->amo_id,
            'amo_updated_at'    => $resource->amo_updated_at,
            'source'            => $resource->source,
            'playground_count'  => $resource->playgrounds()->count(),
            'sportList'  => $resource->getSportsList(),
            'sports' => $resource->sports()->pluck('sports.id'),
            'parent_club_id' => $resource->parent_club_id,
            'parent_club' => $resource->parent_club ? [
                'id' => $resource->parent_club->id,
                'name' => '#' . $resource->parent_club_id . ' ' . $resource->parent_club->name,
            ] : null,
            'booking_types' => BookingTypeResource::collection($resource->bookingTypes),
            'moderators' => ClubUserResource::collection($resource->moderators),
            'entity' => $resource->entity?$resource->entity:"",
            'BIK' => $resource->BIK?$resource->BIK:"",
            'number_rs' => $resource->number_rs?$resource->number_rs:"",
            'NDS' => $resource->NDS?$resource->NDS:"",
            'company_name' => $resource->company_name?$resource->company_name:"",
            'INN' => $resource->INN?$resource->INN:"",
            'KPP' => $resource->KPP?$resource->KPP:"",
            'ORGN' => $resource->ORGN?$resource->ORGN:"",
            'entity_adress' => $resource->entity_adress?$resource->entity_adress:"",
            'registration_date' => $resource->registration_date?$resource->registration_date:"",
            'entity_boss_name' => $resource->entity_boss_name?$resource->entity_boss_name:"",
            'entity_boss_position' => $resource->entity_boss_position?$resource->entity_boss_position:"",
            'company_account' => $resource->company_account?$resource->company_account:"",
            'bank_name' => $resource->bank_name?$resource->bank_name:"",
            'post_index' => $resource->post_index?$resource->post_index:"",
            'post_street' => $resource->post_street?$resource->post_street:"",
            'post_building' => $resource->post_building?$resource->post_building:"",
            'post_building_number' => $resource->post_building_number?$resource->post_building_number:"",
        ];
    }
}
