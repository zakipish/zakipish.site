<?php

namespace App\Http\Resources\Admin;

use App\Models\PlaygroundSchedule;
use Illuminate\Http\Resources\Json\JsonResource;

class PlaygroundScheduleResource extends JsonResource
{



    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var PlaygroundSchedule $resource */
        $resource = $this->resource;
        return [
            'id' => $resource->id,
            'price' => $resource->price,
            'date' => $resource->date,
            'interval' => $resource->interval,
            'playground_id' => $resource->playground_id,
        ];
    }
}
