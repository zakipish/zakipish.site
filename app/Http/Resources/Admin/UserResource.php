<?php

namespace App\Http\Resources\Admin;

use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;
use \DateTime;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var User $resource */
        $resource = $this->resource;
        return [
            'id'                => $resource->id,
            'email'             => $resource->email,
            'name'              => $resource->name,
            'role'              => $resource->role,
            'moderated_clubs'   => ClubUserResource::collection($resource->moderatedClubs),
        ];
    }
}
