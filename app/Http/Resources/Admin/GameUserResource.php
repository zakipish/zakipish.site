<?php

namespace App\Http\Resources\Admin;

use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Log;

class GameUserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @param int $gameId
     * @return array
     */
    public function toArray($request)
    {
        /** @var User $resource */
        $resource = $this->resource;
        Log::info($resource);
        return [
            'game_user' => $resource->pivot,
            'email' => $resource->email,
            'logo' => $resource
        ];
    }
}
