<?php

namespace App\Http\Resources\Admin;

use App\Models\Surface;
use Illuminate\Http\Resources\Json\JsonResource;

class SurfaceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var Surface $resource */
        $resource = $this->resource;
        return [
            'id' => $resource->id,
            'slugs'         => SlugResource::collection($resource->slugs),
            'slug' => $resource->slug,
            'name' => $resource->name,
        ];
    }
}
