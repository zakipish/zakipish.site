<?php

namespace App\Http\Resources\Admin;

use App\Models\PlayerStatus;
use Illuminate\Http\Resources\Json\JsonResource;

class PlayerStatusResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var PlayerStatus $resource */
        $resource = $this->resource;
        return [
            'id' => $resource->id,
            'slug' => $resource->slug,
            'name' => $resource->name,
        ];
    }
}
