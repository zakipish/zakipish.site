<?php

namespace App\Http\Resources\Admin;

use App\Models\Sport;
use Illuminate\Http\Resources\Json\JsonResource;

class SportResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var Sport $resource */
        $resource = $this->resource;
        return [
            'id' => $resource->id,
            'slug' => $resource->slug,
            'icon' => new ImageResource($resource->icon),
            'name' => $resource->name,
            'description' => $resource->description,
            'rules' => $resource->rules,
            'min_players' => $resource->min_players,
            'max_players' => $resource->max_players,
            'label' => $resource->label,
            'equipments' => EquipmentResource::collection($resource->equipments),
            'icon_url' => $resource->iconUrl,
            'photo' => new ImageResource($resource->photo),
            'photo_url' => $resource->photoUrl
        ];
    }
}
