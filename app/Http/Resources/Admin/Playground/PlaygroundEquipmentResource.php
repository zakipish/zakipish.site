<?php

namespace App\Http\Resources\Admin\Playground;

use App\Models\Service;
use Illuminate\Http\Resources\Json\JsonResource;

class PlaygroundEquipmentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var Service $resource */
        $resource = $this->resource;
        return [
            'id' => $resource->id,
            'name' => $resource->name,
            'price' => $resource->pivot->price,
            'payable' => $resource->pivot->payable,
            'unit' => $resource->pivot->unit,
        ];
    }
}
