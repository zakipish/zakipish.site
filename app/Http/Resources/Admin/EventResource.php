<?php

namespace App\Http\Resources\Admin;

use App\Http\Resources\Admin\Event\UserResource as EventUserResource;
use App\Http\Resources\Admin\Event\ClubResource as EventClubResource;
use App\Http\Resources\Admin\Event\PlaygroundResource as EventPlaygroundResource;
use App\Http\Resources\Admin\Event\SportResource as EventSportResource;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class EventResource extends JsonResource
{

    public function toArray($request)
    {
        $resource = $this->resource;
        return [
            'id' => $resource->id,
            'slug' => $resource->slug,
            'owner_id' => $resource->owner_id,
            'club_id' => $resource->club_id,
            'club' => new EventClubResource($resource->club),
            'playground_id' => $resource->playground_id,
            'playground' => new EventPlaygroundResource($resource->playground),
            'sport_id' => $resource->sport_id,
            'sport' => new EventSportResource($resource->sport),
            'game_level_id' => $resource->game_level_id,
            'gameLevel' => new GameLevelResource($resource->gameLevel),
            'chat_disabled' => $resource->chat_disabled,
            'max_players' => $resource->max_players,
            'min_players' => $resource->min_players,
            'title' => $resource->title,
            'description' => $resource->description,
            'price' => $resource->price,
            'type' => $resource->type,
            'coordinates' => $resource->coordinates,
            'blockSchedule' => $resource->blockSchedule,
            'interval' => $resource->interval,
            'date' => $resource->date,
            'age_start' => $resource->age_start,
            'age_end' => $resource->age_end,
            'updated_at' => (new Carbon($resource->updated_at))->toIso8601String(),
            'images' => ImageResource::collection($resource->images),
            'address' => $resource->address,
            'users' => EventUserResource::collection($resource->users),
            'city_id' => $resource->club ? $resource->club->city_id : null,
            'place_type' => $resource->place_type,
        ];
    }
}
