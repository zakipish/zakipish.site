<?php

namespace App\Http\Resources\Admin;

use App\Models\Contact;
use App\Models\ContactType;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Log;

class ContactResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var Contact $resource */
        $resource = $this->resource;
        return [
            'id' => $resource->id,
            'value' => $resource->value,
            'contact_type_id' => $resource->contact_type_id,
            'contact_type' => new ContactTypeResource($resource->contact_type),
        ];
    }
}
