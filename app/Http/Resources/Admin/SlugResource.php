<?php

namespace App\Http\Resources\Admin;

use App\Models\Slug;
use App\Models\Sport;
use Illuminate\Http\Resources\Json\JsonResource;

class SlugResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var Slug $resource */
        $resource = $this->resource;
        return [
            'value' => $resource->value,
            'is_active' => $resource->is_active,
        ];
    }
}
