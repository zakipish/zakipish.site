<?php

namespace App\Http\Resources\Admin;

use App\Models\Infrastructure;
use Illuminate\Http\Resources\Json\JsonResource;

class InfrastructureResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var Infrastructure $resource */
        $resource = $this->resource;

        return [
            'id'    => $resource->id,
            'slugs' => SlugResource::collection($resource->slugs),
            'slug'  => $resource->slug,
            'name'  => $resource->name,
            'icon' => new ImageResource($resource->icon),
            'icon_url' => $resource->iconUrl(),
        ];
    }
}
