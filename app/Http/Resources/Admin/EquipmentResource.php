<?php

namespace App\Http\Resources\Admin;

use App\Models\Equipment;
use Illuminate\Http\Resources\Json\JsonResource;

class EquipmentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var Equipment $resource */
        $resource = $this->resource;
        return [
            'id' => $resource->id,
            'slugs' => SlugResource::collection($resource->slugs),
            'slug' => $resource->slug,
            'name' => $resource->name,
        ];
    }
}
