<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PlaygroundCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'string|required',
            'description' => 'string|nullable',
            'slug' => 'string|nullable|regex:/^[a-z0-9\-]+$/',
            'club_id' => 'integer|required',
            'services' => 'array',
            'surfaces' => 'array',
            'equipments' => 'array',
            'sports' => 'array',
            'sports.*' => 'integer',
            'type'  => 'integer|required',
            'min_interval'  => 'integer|nullable',
        ];
    }
}
