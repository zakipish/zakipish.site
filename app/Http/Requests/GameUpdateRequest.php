<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GameUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'game_level_id' => 'integer|nullable',
            'age_start' => 'integer|nullable',
            'age_end' => 'integer|nullable',
            'name' => 'string|nullable',
            'max_players' => 'integer|nullable',
            'min_players' => 'integer|nullable',
        ];
    }
}
