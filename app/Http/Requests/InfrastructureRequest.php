<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InfrastructureRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->file('icon')){
            return [
                'icon' => 'required|file',
            ];
        }
        if ($this->action == "delete_logo" || $this->action == "delete_logo"){
            return ['action'=>'string','id'=>'string'];
        }
        return [
            'name' => 'string|required',
            'slug' => 'string|nullable|regex:/^[a-z0-9\-]+$/'
        ];
    }
}
