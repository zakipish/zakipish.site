<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GameCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'playground_id' => 'required|integer',
            'game_level_id' => 'integer|nullable',
            'booking' => 'boolean|nullable',
            'age_start' => 'integer|nullable',
            'age_end' => 'integer|nullable',
            'name' => 'string|nullable',
            'date' => 'required|date',
            'interval' => 'required|array',
            'interval.*' => 'required|string',
            'max_players' => 'integer|nullable',
            'min_players' => 'integer|nullable',
            'sport_id' => 'required|integer'
        ];
    }
}
