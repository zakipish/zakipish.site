<?php

namespace App\Http\Requests;

use App\Models\PlaygroundSchedule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Contracts\Validation\Factory as ValidationFactory;

class PlaygroundScheduleCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function validator(ValidationFactory $factory)
    {
        $validator = $factory->make(
            $this->validationData(), $this->rules(),
            $this->messages(), $this->attributes()
        );
        return $validator;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'playground_id' => 'integer|required',
            'interval' => [
                'required',
                'array',
            ],
            'interval.*' => [
                'required',
                'regex:/^(([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]|24:00)$/',
            ],
//            'time' => 'string|required|regex:/^(0[0-9]|[10:23]):00$/',
            'date' => 'string|nullable',
            'price' => 'numeric|nullable',
        ];
    }
}
