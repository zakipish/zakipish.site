<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Log;

class SportUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->file('icon')){
            return [
                'icon' => 'required',
            ];
        }
        if($this->file('photo')){
            return [
                'photo' => 'mimes:jpeg,bmp,png,svg',
            ];
        }

        return [
            'name' => 'string|nullable',
            'slug' => 'string|nullable|regex:/^[a-z0-9\-]+$/',
            'label' => 'string|nullable',
            'order' => 'integer|nullable',
        ];
    }
}
