<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GameRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'slug' => 'string|nullable|regex:/^[a-z0-9\-]+$/',
            'name' => 'string|required',
            'description' => 'string|nullable',
            'min_players' => 'integer|nullable',
            'max_players' => 'integer|nullable',
            'playground_id' => 'integer|required',
            'sport_id' => 'integer|required',
            'game_status_id' => 'integer|nullable',
            'game_level_id' => 'integer|required',
            'date' => 'date|required',
            'interval' => [
                'required',
                'array',
            ],
            'interval.*' => [
                'required',
                'regex:/^(([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]|24:00)$/',
            ],
            'users' => 'array|nullable',
            'users.*.game_user.user_id' => 'integer|required',
            'users.*.game_user.player_status_id' => 'integer|required',
            'price' => 'numeric|nullable',
        ];
    }
}
