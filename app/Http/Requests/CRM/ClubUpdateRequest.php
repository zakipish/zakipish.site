<?php

namespace App\Http\Requests\CRM;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Log;

class ClubUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->input('imageUrl')){
            return [
                'imageUrl' => 'string|active_url'
            ];
        }
        if ($this->file('image')) {
            return [
                'image[]' => 'image|mimes:jpeg,bmp,png,svg',
            ];
        }
        if ($this->file('logo')) {
            return [
                'logo' => 'image|mimes:jpeg,bmp,png,svg',
            ];
        }
        if ($this->action == "update_images_order"){
            return ['action'=>'string','image[]'=>'string','image_ids[]'=>'integer'];
        }
        if ($this->action == "delete_image" || $this->action == "delete_logo"){
            return ['action'=>'string','id'=>'string'];
        }
        return [
            'name' => 'string|nullable',
            'address' => 'string|nullable',
            'coordinates' => 'string|nullable',
            'description' => 'string|nullable',
            'region' => 'string|nullable',
            'city_id' => 'integer|nullable',
            'contacts' => 'array',
            'infrastructures' => 'array',
            'BIK' => 'string|nullable',
            'number_rs' => 'string|nullable',
            'NDS' => 'integer|nullable',
            'company_name' => 'string|nullable',
            'INN' => 'string|nullable',
            'KPP' => 'string|nullable',
            'OGRN' => 'string|nullable',
            'entity_address' => 'string|nullable',
            'registration_date' => 'string|nullable',
            'registration_certificate'=> 'string|nullable',
            'entity_boss_name' => 'string|nullable',
            'entity_boss_position' => 'string|nullable',
            'company_account' => 'string|nullable',
            'bank_name' => 'string|nullable',
            'post_index'=> 'string|nullable',
            'post_street'=> 'string|nullable',
            'post_building'=> 'string|nullable',
            'post_building_number'=> 'string|nullable',
        ];
    }
}
