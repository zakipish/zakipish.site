<?php

namespace App\Http\Requests\CRM;

use Illuminate\Foundation\Http\FormRequest;

class PlaygroundCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'string|nullable',
            'description' => 'string|nullable',
            'type'  => 'integer|nullable',
            'services'  => 'array',
            'equipments'  => 'array',
            'sport_ids' => 'array|nullable',
            'sport_ids.*' => 'integer|nullable',
            'surfaces' => 'array|nullable',
            'surfaces.id' => 'integer|nullable',
            'surfaces.name' => 'string',
            'min_interval'  => 'integer|nullable',
        ];
    }
}
