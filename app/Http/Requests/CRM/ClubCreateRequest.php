<?php

namespace App\Http\Requests\CRM;

use Illuminate\Foundation\Http\FormRequest;

class ClubCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'string|required',
            'address' => 'string|nullable',
            'description' => 'string|nullable',
            'slug' => 'string|nullable|regex:/^[a-z0-9\-]+$/',
            'club_chain_id' => 'integer|nullable',
            'sport_id' => 'integer|nullable',
            'country' => 'string|nullable',
            'region' => 'string|nullable',
            'city_id' => 'integer|nullable',
            'department' => 'string|nullable',
            'department_id' => 'integer|nullable',
            'source_link' => 'string|nullable',
            'rank' => 'integer|nullable',
            'contacts' => 'array',
            'infrastructures' => 'array',
            'type'  => 'integer|nullable',
            'coordinates' => 'string|nullable',
            'source'   => 'string|nullable',
            'parent_club_id'=>'integer|nullable',
            'booking_types'=>'array',
        ];
    }
}
