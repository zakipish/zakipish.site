<?php

namespace App\Http\Requests\CRM\Event;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'club_id' => 'integer|required',
            'playground_id' => 'integer|nullable',
            'sport_id' => 'integer|required',
            'game_level_id' => 'integer|nullable',
            'chat_disabled' => 'boolean|required',
            'max_players' => 'integer|required',
            'min_players' => 'integer|required',
            'title' => 'string|required',
            'description' => 'string|nullable',
            'price' => 'integer|nullable',
            'type' => 'integer|required',
            'coordinates' => 'array|nullable',
            'coordinates.*' => 'numeric|required',
            'blockSchedule' => 'boolean|nullable',
            'interval' => 'array|required',
            'interval.*' => 'string',
            'date' => 'string|required',
            'age_start' => 'integer|nullable',
            'age_end' => 'integer|nullable',
            'images' => 'array|required',
            'images.*.id' => 'integer|required',
            'images.*.order' => 'integer|nullable',
            'address' => 'string|nullable',
        ];
    }
}
