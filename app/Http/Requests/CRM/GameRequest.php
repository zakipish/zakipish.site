<?php

namespace App\Http\Requests\CRM;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Log;

class GameRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'date' => 'date|required',
            'interval' => 'array|required',
            'interval.*' => 'regex:/^\d\d?:\d\d?$/|required',
            'sport_id' => 'integer|required',
            'phone' => 'string|nullable',
            'name' => 'string|nullable',
            'description' => 'string|nullable',
            'price' => 'numeric|nullable|min:0',
            'owner_id' => 'integer|nullable|required_without:owner',
            'owner' => 'array|nullable|required_without:owner_id',
        ];

        return $rules;
    }
}
