<?php

namespace App\Http\Requests\CRM;

use Illuminate\Foundation\Http\FormRequest;

class PlaygroundScheduleCreateRequest extends FormRequest
{
    /*
    public function authorize()
    {
        return true;
    }
     */

    public function rules()
    {
        return [
            'state' => 'string|required',
            'price' => 'integer|required',
            'list' => 'array|required',
            'list.*.date' => 'date|required',
            'list.*.durations' => 'array|required',
            'list.*.durations.*.from' => 'regex:/^\d\d?:\d\d?$/|required',
            'list.*.durations.*.to' => 'regex:/^\d\d?:\d\d?$/|required',
        ];
    }
}
