<?php

namespace App\Http\Requests\CRM;

use Illuminate\Foundation\Http\FormRequest;

class PlaygroundUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->input('imageUrl')){
            return [
                'imageUrl' => 'string|active_url'
            ];
        }
        if ($this->file('avatar')) {
            return [
                'avatar' => 'image|mimes:jpeg,bmp,png,svg',
            ];
        }
        if ($this->file('image')) {
            return [
                'image[]' => 'image|mimes:jpeg,bmp,png,svg',
            ];
        }
        if ($this->action == "update_images_order"){
            return ['action'=>'string','image[]'=>'string','image_ids[]'=>'integer'];
        }
        if (in_array($this->action ,["delete_image","delete_logo",'delete_playground'])){
            return ['action'=>'string','id'=>'string'];
        }
        return [
            'name' => 'string|nullable',
            'description' => 'string|nullable',
            'type'  => 'integer|nullable',
            'services'  => 'array',
            'equipments'  => 'array',
            'sport_ids' => 'array|nullable',
            'sport_ids.*' => 'integer|nullable',
            'surfaces' => 'array|nullable',
            'surfaces.id' => 'integer|nullable',
            'surfaces.name' => 'string',
            'min_interval'  => 'integer|nullable',
            'isMinIntervalSetted'  => 'boolean|nullable',
        ];
    }
}
