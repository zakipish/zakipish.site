<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->file('image')) {
            return [
                'image' => 'image|mimes:jpeg,png',
            ];
        }
        return [
            'email' => ['required_without_all:phone', 'nullable', 'email', Rule::unique('users')->ignore(Auth::id())],
            'phone' => ['required_without_all:email', 'nullable', Rule::unique('users')->ignore(Auth::id()), 'digits:10'],
            'name' => 'filled|string',
            'lastName' => 'string|nullable',
            'midName' => 'string|nullable',
            'birthDate' => 'string|nullable',
            'city_id' => 'integer|nullable',
            'about' => 'string|nullable',
            'height' => 'integer|nullable',
            'weight' => 'integer|nullable',
            'gender' => 'integer|nullable',
            'sports' => 'array',
            'sports.game_level' => 'integer',
            'sports.game_exp' => 'integer'
        ];
    }
}
