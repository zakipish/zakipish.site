<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PlaygroundUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->input('imageUrl')){
            return [
                'imageUrl' => 'string|active_url'
            ];
        }
        if ($this->file('image')) {
            return [
                'image[]' => 'image|mimes:jpeg,bmp,png,svg',
            ];
        }
        if ($this->action == "update_images_order"){
            return ['action'=>'string','image[]'=>'string','image_ids[]'=>'integer'];
        }
        if (in_array($this->action ,["delete_image","delete_logo",'delete_playground'])){
            return ['action'=>'string','id'=>'string'];
        }
        return [
            'name' => 'string|required',
            'description' => 'string|nullable',
            'slug' => 'string|nullable|regex:/^[a-z0-9\-]+$/',
            'club_id' => 'integer|nullable',
            'services' => 'array',
            'surfaces' => 'array',
            'equipments' => 'array',
            'sports' => 'array',
            'sports.*' => 'integer',
            'type'  => 'integer|required',
            'min_interval'  => 'integer|nullable',
        ];
    }
}
