<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Log;
use Closure;

class CheckTmpSession
{
    public function handle($request, Closure $next)
    {
        if ($request->session()->has('tmp') && $request->session()->get('tmp') == 1)
            return response()->json(['errors' => 'Forbidden'], 403);

        return $next($request);
    }
}
