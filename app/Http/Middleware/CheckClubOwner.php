<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Log;
use Closure;

class CheckClubOwner
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->route('club') && !$request->user()->moderatedClubs()->where('clubs.id', $request->route('club')->id)->exists()) {
            return response()->json(['errors' => 'Forbidden'], 403);
        }

        if ($request->route('playground') && !$request->user()->moderatedPlaygrounds()->where('playgrounds.id', $request->route('playground')->id)->exists()) {
            return response()->json(['errors' => 'Forbidden'], 403);
        }

        if ($request->route('event') && $request->route('event')->id &&
            !$request->user()->moderatedEvents()->where('events.id', $request->route('event')->id)->exists()) {
            return response()->json(['errors' => 'Forbidden'], 403);
        }

        return $next($request);
    }
}
