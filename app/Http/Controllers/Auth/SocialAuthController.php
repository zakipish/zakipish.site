<?php

namespace App\Http\Controllers\Auth;

use App\Models\SocialAccount;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Http\UploadedFile;
use App\Services\UploadService;
use Illuminate\Support\Facades\Log;

class SocialAuthController extends Controller
{
    public function redirect(Request $request, $provider){
        if(!in_array($provider, SocialAccount::AVAILABLE_PROVIDERS))
            abort(404);

        if($request->has('target')) {
            $target = $request->get('target');
            $request->session()->put('target_after_oauth', $target);
        }

        return Socialite::with($provider)->redirect();
    }

    public function unlink(Request $request, $provider) {
        if(!in_array($provider, SocialAccount::AVAILABLE_PROVIDERS))
            abort(404);

        if(!Auth::check())
            about(401);

        $social_account = SocialAccount::where('user_id', Auth::id())->where('provider', $provider)->firstOrFail();
        $social_account->delete();

        return back();
    }

    public function callback(Request $request, $provider){
        if(!in_array($provider, SocialAccount::AVAILABLE_PROVIDERS))
            abort(404);

        if($request->has('error')){
            return redirect('https://devclub.zakipish.ru');
        }

        $socialiteUser = Socialite::driver($provider);

        if($provider == SocialAccount::PROVIDER_VK){
            $socialiteUser = $socialiteUser->fields([
                'photo_max_orig',
                'photo_200'
            ]);
        }
        $socialiteUser = $socialiteUser->user();
        //dd($socialiteUser);

        $socialId = $socialiteUser->id;
        $socialAccount = SocialAccount::where('provider', $provider)
            ->where('provider_id', $socialId)
            ->first();

        if(!empty($socialAccount)){
            if(!Auth::check() || $socialAccount->user_id == Auth::id()) {
                $user = $socialAccount->user;

                if($user->avatars()->count() === 0) {
                    $user->save();
                    $this->attach_avatar($user, $provider, $socialiteUser);
                }
                Auth::login($user, true);
            }

            if (Auth::check() && $socialAccount->user_id != Auth::id()) {
                $error = 'Учётная запись уже занята';
            }
        }else{
            $socialAccount = new SocialAccount();
            $socialAccount->provider = $provider;
            $socialAccount->provider_id = $socialId;
            $socialAccount->social_email = $socialiteUser->email;
            $socialAccount->token = $socialiteUser->token;

            switch($provider) {
                case SocialAccount::PROVIDER_MAIL:
                    $socialAccount->mailru_profile_url = $socialiteUser->user['link'];
                    break;
                case SocialAccount::PROVIDER_INSTAGRAM:
                    $socialAccount->instagram_nickname = $socialiteUser->nickname;
                    break;
            }

            if(Auth::check()){
                $currentUser = Auth::user();
                $socialAccount->user_id = $currentUser->id;

                $socialAccount->save();

                if($currentUser->avatars()->count() === 0) {
                    $currentUser->save();
                    $this->attach_avatar($currentUser, $provider, $socialiteUser);
                }
            } else {
                $existUser = User::where('email', $socialAccount->social_email)->first();
                if($socialAccount->social_email && !empty($existUser)){
                    $socialAccount->user_id = $existUser->id;
                    $socialAccount->save();
                    $existUser->save();
                    $this->attach_avatar($existUser, $provider, $socialiteUser);
                    Auth::login($existUser, true);
                }else{
                    $user = new User();
                    $name = $socialiteUser->name;
                    $splittedName = explode(' ', $name);

                    if(count($splittedName) > 1) {
                        $user->fill([
                            'lastName' => array_splice($splittedName, -1, 1)[0],
                            'name' => join(' ', $splittedName)
                        ]);
                    } else {
                        $user->fill([
                            'name'  => $socialiteUser->name,
                        ]);
                    }
                    $user->fill([
                        'email' => $socialiteUser->email
                    ]);
                    $user->save();
                    $socialAccount->user_id = $user->id;
                    $socialAccount->save();
                    $this->attach_avatar($user, $provider, $socialiteUser);
                    Auth::login($user, true);
                }
            }
        }

        if ($request->session()->has('target_after_oauth')) {
            $target = $request->session()->get('target_after_oauth');
            $request->session()->forget('target_after_oauth');

            if (isset($error)) {
                $errorMessage = http_build_query(['error' => $error]);
                $target = "$target?$errorMessage";
            }

            return redirect($target);
        } else {
            // TODO redirect to env(APP_URL)
            return redirect('/');
        }

    }

    private function get_avatar_url($provider, $socialiteUser)
    {
        switch($provider) {
            case SocialAccount::PROVIDER_FB:
                return $socialiteUser->avatar_original;
                break;
            case SocialAccount::PROVIDER_VK:
                if(!empty($socialiteUser->user['photo_max_orig'])){
                    return $socialiteUser->user['photo_max_orig'];
                    break;
                }
                if(!empty($socialiteUser->user['photo_200'])){
                    return $socialiteUser->user['photo_200'];
                    break;
                }
                return $socialiteUser->avatar;
                break;
            case SocialAccount::PROVIDER_OK:
            case SocialAccount::PROVIDER_MAIL:
            case SocialAccount::PROVIDER_INSTAGRAM:
                return $socialiteUser->avatar;
                break;
        }

        return null;
    }

    private function attach_avatar($user, $provider, $socialiteUser)
    {
        $avatar_url = $this->get_avatar_url($provider, $socialiteUser);
        if($avatar_url) {
            $uploadService = new UploadService();
            $tmpfname = tempnam("/tmp", "UL_IMAGE");
            file_put_contents($tmpfname, fopen($avatar_url, 'r'));
            $originalFilename = basename(parse_url($avatar_url)['path']);
            $file = new UploadedFile($tmpfname, $originalFilename, 'image/jpeg');

            $image = $uploadService->uploadImage($file);
            $user->avatars()->attach($image->id);
            $user->save();
            unlink($tmpfname);
        }
    }
}
