<?php

namespace App\Http\Controllers;

use App\Models\Image;
use App\Services\ResizeService;
use Illuminate\Http\Request;

class ResizeController extends Controller
{
    protected $resizer;

    public function __construct(ResizeService $resizeService)
    {
        $this->resizer = $resizeService;
    }

    public function process($mode, $code){
        $image = Image::where('code', $code)->firstOrFail();
        return $this->resizer->process($image, $mode);
    }
}
