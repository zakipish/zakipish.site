<?php

namespace App\Http\Controllers;

use App\Models\Image;
use App\Services\ResizeService;
use Illuminate\Http\Request;

class PageController extends Controller
{
    Public function index()
    {
        $image = Image::first();
        dd($image->getUrl(), $image->getUrl(ResizeService::RULE_WALL_400));
    }

    public function terms()
    {
        return view('terms');
    }

    public function privacy()
    {
        return view('privacy');
    }
}
