<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Resources\Admin\DepartmentResource;
use App\Http\Requests\DepartmentRequest;
use App\Models\Department;
use App\Models\Utils\Paginator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DepartmentController extends Controller
{
    use Paginator;

    public function index(Request $request)
    {
        $city_id = (int)$request->get('city_id');
        if($city_id){
            $departments = Department::orderBy('id')->where('city_id','=',$city_id);
        }else{
            $departments = Department::orderBy('id');
        }
        $departments = $this->getAllOrPaginate($departments, $request);

        return DepartmentResource::collection($departments);
    }

    public function store(DepartmentRequest $request)
    {
        $model = new Department($request->validated());
        $model->save();
        return new DepartmentResource($model);
    }

    public function show_by_city_id(Request $request)
    {
        $city_id = (int)$request->get('city_id');
        $departments = Department::orderBy('id')->where("city_id",'=',$city_id)->get();
        $departments = $this->getAllOrPaginate($departments, $request);

        return DepartmentResource::collection($departments);
    }

    public function show(Department $department)
    {
        return new DepartmentResource($department);
    }


    public function update(DepartmentRequest $request, Department $department)
    {
        $department->update($request->validated());
        return new DepartmentResource($department);
    }

    public function destroy(Department $department)
    {
        $department->delete();
        return [];
    }
}
