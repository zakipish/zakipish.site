<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\SportCreateRequest;
use App\Http\Requests\SportUpdateManyRequest;
use App\Http\Requests\SportUpdateRequest;
use App\Http\Resources\Admin\SportResource;
use App\Models\Sport;
use App\Services\UploadService;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Log;

class SportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $sports = Sport::query()->with(['icon', 'activeSlugs', 'equipments', 'photo'])->get();
        return SportResource::collection($sports);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param SportCreateRequest $request
     * @return SportResource
     */
    public function store(SportCreateRequest $request)
    {
        $data = $request->validated();
        $label = $data['label'];
        unset($data['label']);
        $model = new Sport($data);
        $model->save();
        $model->label = ($label ? $label . '.' : '') . $model->id;
        $model->save();
        return new SportResource($model);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Sport $sport
     * @return SportResource
     */
    public function show(Sport $sport)
    {
        return new SportResource($sport);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Sport $sport
     * @return void
     */
    public function edit(Sport $sport)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param SportUpdateRequest $request
     * @param  \App\Models\Sport $sport
     * @return SportResource
     */
    public function update(SportUpdateRequest $request, Sport $sport, UploadService $uploadService)
    {
        if ($request->action == "delete_logo") {
            $sport->icon()->delete();
            $sport->save();
            return 1;
        } elseif ($request->action == "delete_photo") {
            $sport->photo()->delete();
            $sport->save();
            return 1;
        } else {
            if ($request->has('imageUrl') && $request->has('field')) {
                $file = $uploadService->prepareRemoteImage($request->imageUrl);

                $image = $uploadService->uploadImage($file);

                if ($request->get('field') == 'icon') {
                    $sport->icon()->associate($image);
                    $sport->save();
                }

                if ($request->get('field') == 'photo') {
                    $sport->photo()->associate($image);
                    $sport->save();
                }
            }

            if ($file = $request->file('icon')) {
                $image = $uploadService->uploadImage($file);
                $sport->icon()->associate($image);
                $sport->save();
            }

            if ($file = $request->file('photo')) {
                $image = $uploadService->uploadImage($file);
                $sport->photo()->associate($image);
                $sport->save();
            }

            $sport->update($request->validated());
            return new SportResource($sport);
        }
    }

    /**
     * @param SportUpdateManyRequest $request
     */
    public function updateMany(SportUpdateManyRequest $request)
    {
        $data = $request->get('data');
        foreach ($data as $item) {
            $sport = Sport::find($item['id']);
            if ($sport) {
                $sport->update($item);
            }
        }
        return [];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Sport $sport
     * @return void
     * @throws \Exception
     */
    public function destroy(Sport $sport)
    {
        $sport->delete();
        return [];
    }
}
