<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Resources\Admin\BookingTypeResource;
use App\Models\BookingType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Utils\Paginator;

class BookingTypeController extends Controller
{
    use Paginator;
    public function index(Request $request){
        $bookingTypes = BookingType::orderBy('id');
        $bookingTypes = $this->getAllOrPaginate($bookingTypes, $request);
        return BookingTypeResource::collection($bookingTypes);
    }
}
