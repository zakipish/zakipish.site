<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ContactRequest;
use App\Http\Resources\Admin\ContactResource;
use App\Models\Contact;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return ContactResource::collection(Contact::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ContactRequest $request
     * @return ContactResource
     */
    public function store(ContactRequest $request)
    {
        $model = new Contact($request->validated());
        $model->save();
        return new ContactResource($model);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Contact $surface
     * @return ContactResource
     */
    public function show(Contact $surface)
    {
        return new ContactResource($surface);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Contact $surface
     * @return void
     */
    public function edit(Contact $surface)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ContactRequest $request
     * @param  \App\Models\Contact $surface
     * @return ContactResource
     */
    public function update(ContactRequest $request, Contact $surface)
    {
        $surface->update($request->validated());
        return new ContactResource($surface);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Contact $surface
     * @return void
     * @throws \Exception
     */
    public function destroy(Contact $surface)
    {
        $surface->delete();
        return [];
    }
}
