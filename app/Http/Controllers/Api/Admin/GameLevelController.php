<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\GameLevelRequest;
use App\Http\Resources\Admin\GameLevelResource;
use App\Models\GameLevel;

class GameLevelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return GameLevelResource::collection(GameLevel::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param GameLevelRequest $request
     * @return GameLevelResource
     */
    public function store(GameLevelRequest $request)
    {
        $model = new GameLevel($request->validated());
        $model->save();
        return new GameLevelResource($model);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\GameLevel  $gameLevel
     * @return GameLevelResource
     */
    public function show(GameLevel $gameLevel)
    {
        return new GameLevelResource($gameLevel);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\GameLevel $gameLevel
     * @return void
     */
    public function edit(GameLevel $gameLevel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param GameLevelRequest $request
     * @param  \App\Models\GameLevel $gameLevel
     * @return GameLevelResource
     */
    public function update(GameLevelRequest $request, GameLevel $gameLevel)
    {
        $gameLevel->update($request->validated());
        return new GameLevelResource($gameLevel);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\GameLevel $gameLevel
     * @return void
     * @throws \Exception
     */
    public function destroy(GameLevel $gameLevel)
    {
        $gameLevel->delete();
        return [];
    }
}
