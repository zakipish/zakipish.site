<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\EquipmentRequest;
use App\Http\Resources\Admin\EquipmentResource;
use App\Models\Equipment;
use App\Models\Utils\Paginator;
use Illuminate\Http\Request;

class EquipmentController extends Controller
{
    use Paginator;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $equipments = Equipment::orderBy('id');
        $equipments = $this->getAllOrPaginate($equipments, $request);
        return EquipmentResource::collection($equipments);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param EquipmentRequest $request
     * @return EquipmentResource
     */
    public function store(EquipmentRequest $request)
    {
        $model = new Equipment($request->validated());
        $model->save();
        return new EquipmentResource($model);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Equipment  $equipment
     * @return EquipmentResource
     */
    public function show(Equipment $equipment)
    {
        return new EquipmentResource($equipment);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Equipment $equipment
     * @return void
     */
    public function edit(Equipment $equipment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param EquipmentRequest $request
     * @param  \App\Models\Equipment $equipment
     * @return EquipmentResource
     */
    public function update(EquipmentRequest $request, Equipment $equipment)
    {
        $equipment->update($request->validated());
        return new EquipmentResource($equipment);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Equipment $equipment
     * @return void
     * @throws \Exception
     */
    public function destroy(Equipment $equipment)
    {
        $equipment->delete();
        return [];
    }
}
