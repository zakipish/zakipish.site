<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\PlaygroundTypeRequest;
use App\Http\Resources\Admin\PlaygroundTypeResource;
use App\Models\PlaygroundType;
use App\Models\Utils\Paginator;
use Illuminate\Http\Request;

class PlaygroundTypeController extends Controller
{
    use Paginator;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $playgroundTypes = PlaygroundType::orderBy('id');
        $playgroundTypes = $this->getAllOrPaginate($playgroundTypes, $request);
        return PlaygroundTypeResource::collection($playgroundTypes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param PlaygroundTypeRequest $request
     * @return PlaygroundTypeResource
     */
    public function store(PlaygroundTypeRequest $request)
    {
        $model = new PlaygroundType($request->validated());
        $model->save();
        return new PlaygroundTypeResource($model);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PlaygroundType $playgroundType
     * @return PlaygroundTypeResource
     */
    public function show(PlaygroundType $playgroundType)
    {
        return new PlaygroundTypeResource($playgroundType);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PlaygroundType $playgroundType
     * @return void
     */
    public function edit(PlaygroundType $playgroundType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param PlaygroundTypeRequest $request
     * @param  \App\Models\PlaygroundType $playgroundType
     * @return PlaygroundTypeResource
     */
    public function update(PlaygroundTypeRequest $request, PlaygroundType $playgroundType)
    {
        $playgroundType->update($request->validated());
        return new PlaygroundTypeResource($playgroundType);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PlaygroundType $playgroundType
     * @return void
     * @throws \Exception
     */
    public function destroy(PlaygroundType $playgroundType)
    {
        $playgroundType->delete();
        return [];
    }
}
