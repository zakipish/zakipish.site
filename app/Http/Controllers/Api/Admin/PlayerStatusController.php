<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\PlayerStatusRequest;
use App\Http\Resources\Admin\PlayerStatusResource;
use App\Models\PlayerStatus;

class PlayerStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return PlayerStatusResource::collection(PlayerStatus::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param PlayerStatusRequest $request
     * @return PlayerStatusResource
     */
    public function store(PlayerStatusRequest $request)
    {
        $model = new PlayerStatus($request->validated());
        $model->save();
        return new PlayerStatusResource($model);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PlayerStatus $playerStatus
     * @return PlayerStatusResource
     */
    public function show(PlayerStatus $playerStatus)
    {
        return new PlayerStatusResource($playerStatus);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PlayerStatus $playerStatus
     * @return void
     */
    public function edit(PlayerStatus $playerStatus)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param PlayerStatusRequest $request
     * @param  \App\Models\PlayerStatus $playerStatus
     * @return PlayerStatusResource
     */
    public function update(PlayerStatusRequest $request, PlayerStatus $playerStatus)
    {
        $playerStatus->update($request->validated());
        return new PlayerStatusResource($playerStatus);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PlayerStatus $playerStatus
     * @return void
     * @throws \Exception
     */
    public function destroy(PlayerStatus $playerStatus)
    {
        $playerStatus->delete();
        return [];
    }
}
