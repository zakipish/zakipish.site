<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserCreateRequest;
use App\Http\Requests\UserRequest;
use App\Http\Resources\Admin\UserResource;
use App\Models\User;
use App\Models\Utils\Paginator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class UserController extends Controller
{
    use Paginator;

    public function index(Request $request)
    {
        $perPage = (int)$request->get('per_page');
        Log::info('index user', $request->all());

//        check search param (debounce and search of users)
        $search = null;
        if ($request->get('search')) {
            $search = $request->get('search');
        }

//       check role param form return only one role users
        $role = null;
        if ($request->has('role')) {
            $role = $request->get('role');
        }


        $users = User::where(function ($query) use ($search) {
            if (!empty($search)){
                $query->where('name', 'ILIKE', "%$search%")
                    ->orWhere('email', 'ILIKE', "%$search%");
            }
        });

        if($role != null){
            $users = $users->where('role', $role);
        };

        $users = $this->getAllOrPaginate($users, $request);

        return UserResource::collection($users);
    }

    /**
     * Display the specified resource.
     * @param User $user
     * @return UserResource
     */
    public function show(User $user)
    {
        return new UserResource($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UserRequest $request
     * @param User $user
     * @return UserResource
     */
    public function update(UserRequest $request, User $user)
    {
        $user->update($request->validated());
        $password = $request->input('password');
        if ($password && (!empty($password))) {
            $user->update(['password' => Hash::make($password)]);
        }
        return new UserResource($user);

    }

    public function store(UserCreateRequest $request)
    {
        $user = new User($request->validated());
        $password = $request->input('password');
        $user->password = Hash::make($password);
        $user->save();
        return new UserResource($user);
    }
}
