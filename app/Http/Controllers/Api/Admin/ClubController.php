<?php

namespace App\Http\Controllers\Api\Admin;

use App\Events\ClubChanged;
use App\Http\Requests\ClubCreateRequest;
use App\Http\Requests\ClubUpdateRequest;
use App\Http\Resources\Admin\ClubResource;
use App\Models\Club;
use App\Http\Controllers\Controller;
use App\Models\Sport;
use App\Models\Tasks\ClubAmoCrmSyncTask;
use App\Services\UploadService;
use App\Services\DuplicateIntoParrent;
use App\Services\GeoPointModifierService;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ClubController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $perPage = (int) $request->get('per_page');

        if ($perPage < 1 or $perPage > 1000) {
            $perPage = config('database.paginate');
        }

        if ($request->all)
            $perPage = null;

        if ($request->get('sorted_field')) {
            $order_by = $request->get('sorted_field');
        } else {
            $order_by = 'id';
        }
        if ($request->get('sorted_order')) {
            $order_type = $request->get('sorted_order');
        } else {
            $order_type = 'asc';
        }
        $clauses = [];
        $orclauses = [];
        if ($request->get('rank')) {
            $rank = $request->get('rank');
            $clauses[] = ['rank', '=', $rank];
        }
        if ($request->get('city_id')) {
            $city = $request->get('city_id');
            $clauses[] = ['city_id', '=', $city];
        }
        if ($request->get('name')) {
            $name = $request->get('name');
            $clauses[] = ['name', 'ilike', "%$name%"];
            if ($request->get('id') && (int)($request->get('id'))) {
                $id = $request->get('id');
                $orclauses[] = ['id', '=', $id];
            }
        }

        if ($request->search) {
            $clauses[] = ['name', 'ilike', "%$request->search%"];
        }


        if ($request->get('source')) {
            $source = $request->get('source');
            $clauses[] = ['source', 'ilike', "%$source%"];
        }
        if ($request->get('club_chain_id')) {
            $club = $request->get('club_chain_id');
            $clauses[] = ['club_chain_id', '=', $club];
        }

        $clubs = Club::orderBy($order_by, $order_type)->where($clauses)->orWhere($orclauses);

        if ($request->get('sport_id')) {
            $sport = Sport::where('id', $request->get('sport_id'))->first();
            $clubs = $clubs->where(function (Builder $query) use ($sport) {
                $query->whereHas('playgrounds', function (Builder $query) use ($sport) {
                    $query->whereHas('sports', function (Builder $query) use ($sport) {
                        $query->where('label', '>=', $sport->label);
                    });
                })->orWhereHas('sports', function (Builder $query) use ($sport) {
                    $query->where('label', '>=', $sport->label);
                });
            });


        }

        if ($perPage) {
            $clubs = $clubs->paginate($perPage);
        } else {
            $clubs = $clubs->get();
        }

        return ClubResource::collection($clubs);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ClubCreateRequest $request
     * @return ClubResource
     */
    public function store(ClubCreateRequest $request)
    {
        $model = new Club($request->validated());
        $model->save();
        event(new ClubChanged($model));
        if ($model->parent_club_id){
            $helper = new DuplicateIntoParrent($model);
            $helper->dupicate($model);
        }
        return new ClubResource($model);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Club $club
     * @return ClubResource
     */
    public function show(Club $club)
    {
        return new ClubResource($club);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Club $club
     */
    public function edit(Club $club)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ClubUpdateRequest $request
     * @param  \App\Models\Club $club
     * @param UploadService $uploadService
     * @return ClubResource
     */
    public function update(ClubUpdateRequest $request, Club $club, UploadService $uploadService, GeoPointModifierService $modifier)
    {
        if ($file = $request->file('logo')) {
            $image = $uploadService->uploadImage($file);
            $club->logo()->associate($image);

            $modifier->modify($club);

            $club->save();
        } else if ($files = $request->file('image')) {
            $i = $club->images()->count();
            $out = [];
            foreach ($files as $file) {
                $image = $uploadService->uploadImage($file);
                $club->images()->attach($image->id, ['order' => $i++]);
                $out[] = ["id" => $image->id, 'order' => $i++];
            }
            Log::info('images', $out);
            $club->save();
            if ($club->parent_club_id){
                $helper = new DuplicateIntoParrent($club);
                $helper->duplicate_images();
            }
            return $out;
        } else if ($request->action == "update_images_order") {
            $i = 0;
            $images = $request->images;
            $image_ids = $request->image_ids;
            foreach ($images as $id => $image) {
                $club->images()->updateExistingPivot((int)$image_ids[$id], ['order' => $i]);
                $i++;
            }
            $club->save();
        } else if ($request->action == "delete_image") {
            $club->images()->find((int)$request->id)->delete();
            $club->save();
            return 1;
        } else if ($request->action == "delete_logo") {
            $club->logo()->dissociate();
            $club->save();
            return 1;
        } else if ($request->input('imageUrl')) {
//            upload image from url
            Log::info('CLUB CONTROLLER');
            $path = $request->input('imageUrl');
            $file = $uploadService->prepareRemoteImage($path);
            $result = $uploadService->uploadImage($file);
            $order = $club->images()->count() + 1;
            $out = [];
            if ($request->input('field') === 'image[]') {
                $club->images()->attach($result->id, ['order' => $order]);
            } else if ($request->input('field') === 'logo') {
                $club->logo()->associate($result->id);
            }
            $out[] = ["id" => $result->id, "order" => $order, "preview" => asset('/storage/source/' . $result->code)];
            $club->save();
            return $out;
        } else {
            $club->update($request->validated());
        }
        event(new ClubChanged($club));
        if ($club->parent_club_id){
            $helper = new DuplicateIntoParrent($club);
            $helper->dupicate($club);
        }
//        $club->load('contacts');
        return new ClubResource($club);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Club $club
     * @throws \Exception
     */
    public function destroy(Club $club)
    {
        $club->delete();
        return [];
    }
}
