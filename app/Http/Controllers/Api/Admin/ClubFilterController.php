<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Resources\Admin\ClubFilterResource;
use App\Http\Resources\Admin\ClubChainResource;
use App\Http\Resources\Admin\CityResource;
use App\Http\Resources\Admin\SportResource;
use App\Http\Resources\Admin\ClubSourceResource;
use App\Http\Controllers\Controller;
use App\Models\Club;
use App\Models\ClubChain;
use App\Models\City;
use App\Models\Sport;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class ClubFilterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return array
     */
    public function index()
    {
        return [
            'club_chains' => ClubChainResource::collection(ClubChain::all()),
            'cities' => CityResource::collection(City::all()),
            'sports' => SportResource::collection(Sport::all()),
            'sources' => ClubSourceResource::collection(Club::distinct('source')->select('source')->get()),
        ];
    }
}
