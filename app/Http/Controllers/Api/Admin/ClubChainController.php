<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ClubChainRequest;
use App\Http\Resources\Admin\ClubChainResource;
use App\Models\ClubChain;
use App\Models\Utils\Paginator;
use App\Services\UploadService;
use Illuminate\Http\Request;

class ClubChainController extends Controller
{
    use Paginator;
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $clubChains = ClubChain::orderBy('id');
        $clubChains = $this->getAllOrPaginate($clubChains, $request);
        return ClubChainResource::collection($clubChains);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ClubChainRequest $request
     * @return ClubChainResource
     */
    public function store(ClubChainRequest $request)
    {
        $model = new ClubChain($request->validated());
        $model->save();
        return new ClubChainResource($model);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ClubChain $clubChain
     * @return ClubChainResource
     */
    public function show(ClubChain $clubChain)
    {
        return new ClubChainResource($clubChain);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ClubChain  $clubChain
     */
    public function edit(ClubChain $clubChain)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ClubChainRequest $request
     * @param  \App\Models\ClubChain $clubChain
     * @return ClubChainResource
     */
    public function update(ClubChainRequest $request, ClubChain $clubChain, UploadService $uploadService)
    {
        if ($file = $request->file('logo')) {
            $image = $uploadService->uploadImage($file);
            $clubChain->logo()->associate($image);
            $clubChain->save();
        } else {
            $clubChain->update($request->validated());
        }
        return new ClubChainResource($clubChain);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ClubChain $clubChain
     * @throws \Exception
     */
    public function destroy(ClubChain $clubChain)
    {
        $clubChain->delete();
        return [];
    }
}
