<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\PlaygroundCreateRequest;
use App\Http\Requests\PlaygroundUpdateRequest;
use App\Http\Resources\Admin\PlaygroundResource;
use App\Models\Playground;
use App\Services\UploadService;
use Illuminate\Http\Request;

class PlaygroundController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $perPage = (int) $request->get('per_page');

        if ($perPage < 1 or $perPage > 1000)
            $perPage = config('database.paginate');

        $clubId = (int)$request->get('club');

        $playgrounds = Playground::query();
        $playgrounds->with([
            'slugs', 'activeSlugs', 'club',
            'club.slugs', 'club.activeSlugs', 'club.owner', 'club.contacts', 'club.services', 'club.images', 'club.bookingTypes', 'club.moderators',
            'equipments', 'services', 'sports', 'surfaces', 'images',
        ]);
        $playgrounds->withTrashed();

        if ($clubId) {
            $playgrounds->where('club_id', $clubId);
        }

        if ($request->search)
            $playgrounds->where('name', 'ILIKE', "%$request->search%");

        if ($request->city_id)
            $playgrounds->whereCityId($request->city_id);

        if ($request->sport_id)
            $playgrounds->whereSportId($request->sport_id);

        return PlaygroundResource::collection($playgrounds->paginate($perPage));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param PlaygroundCreateRequest $request
     * @return PlaygroundResource
     */
    public function store(PlaygroundCreateRequest $request)
    {
        $model = new Playground($request->validated());
        $model->save();
        return new PlaygroundResource($model);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Playground $playground
     * @return PlaygroundResource
     */
    public function show(Playground $playground)
    {
        return new PlaygroundResource($playground);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Playground $playground
     * @return void
     */
    public function edit(Playground $playground)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param PlaygroundCreateRequest $request
     * @param  \App\Models\Playground $playground
     * @return PlaygroundResource
     */
    public function update(PlaygroundUpdateRequest $request, Playground $playground, UploadService $uploadService)
    {
//        return $request;
        if($files = $request->file('image')){
            $i = $playground->images()->count();
            $out = [];
            foreach ($files as $file){
                $image = $uploadService->uploadImage($file);
                $playground->images()->attach($image->id, ['order' => $i++]);
                $out[] = ["id"=>$image->id, 'order'=>$i++];
            }
            $playground->save();
            return $out;
        }else if ($request->action == "update_images_order"){
            $i = 0;
            $images = $request->images;
            $image_ids = $request->image_ids;
            foreach ($images as $id=>$image){
                $playground->images()->updateExistingPivot((int)$image_ids[$id],['order'=>$i]);
                $i++;

            }
            $playground->save();
        }else if ($request->action == "delete_image") {
            $playground->images()->find((int)$request->id)->delete();
            $playground->save();
            return 1;
        }else if ($request->input('imageUrl')) {
//            upload image from url
            $path = $request->input('imageUrl');
            $file = $uploadService->prepareRemoteImage($path);
            $result = $uploadService->uploadImage($file);
            $order = $playground->images()->count()+1;
            $out = [];
            if ($request->input('field') === 'image[]'){
                $playground->images()->attach($result->id, ['order' => $order]);
            }
            $out[] = ["id"=>$result->id, "order"=>$order, "preview" => asset('/storage/source/' . $result->code)];
            $playground->save();
            return $out;
        }

        else{
            $playground->update($request->validated());
        }

        return new PlaygroundResource($playground);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Playground $playground
     * @return void
     * @throws \Exception
     */
    public function destroy(Playground $playground)
    {
        $playground->delete();
        return [];
    }
}
