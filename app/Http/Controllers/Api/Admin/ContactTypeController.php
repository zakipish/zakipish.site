<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ContactTypeRequest;
use App\Http\Resources\Admin\ContactTypeResource;
use App\Models\ContactType;

class ContactTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return ContactTypeResource::collection(ContactType::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ContactTypeRequest $request
     * @return ContactTypeResource
     */
    public function store(ContactTypeRequest $request)
    {
        $model = new ContactType($request->validated());
        $model->save();
        return new ContactTypeResource($model);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ContactType $contactType
     * @return ContactTypeResource
     */
    public function show(ContactType $contactType)
    {
        return new ContactTypeResource($contactType);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ContactType $contactType
     * @return void
     */
    public function edit(ContactType $contactType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ContactTypeRequest $request
     * @param  \App\Models\ContactType $contactType
     * @return ContactTypeResource
     */
    public function update(ContactTypeRequest $request, ContactType $contactType)
    {
        $contactType->update($request->validated());
        return new ContactTypeResource($contactType);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ContactType $contactType
     * @return void
     * @throws \Exception
     */
    public function destroy(ContactType $contactType)
    {
        $contactType->delete();
        return [];
    }
}
