<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\SurfaceRequest;
use App\Http\Resources\Admin\SurfaceResource;
use App\Models\Surface;
use App\Models\Utils\Paginator;
use Illuminate\Http\Request;

class SurfaceController extends Controller
{
    use Paginator;
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection|\Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $surfaces = Surface::orderBy('id');
        $surfaces = $this->getAllOrPaginate($surfaces, $request);
        return SurfaceResource::collection($surfaces);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param SurfaceRequest $request
     * @return SurfaceResource
     */
    public function store(SurfaceRequest $request)
    {
        $model = new Surface($request->validated());
        $model->save();
        return new SurfaceResource($model);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Surface $surface
     * @return SurfaceResource
     */
    public function show(Surface $surface)
    {
        return new SurfaceResource($surface);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Surface $surface
     * @return void
     */
    public function edit(Surface $surface)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param SurfaceRequest $request
     * @param  \App\Models\Surface $surface
     * @return SurfaceResource
     */
    public function update(SurfaceRequest $request, Surface $surface)
    {
        $surface->update($request->validated());
        return new SurfaceResource($surface);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Surface $surface
     * @return void
     * @throws \Exception
     */
    public function destroy(Surface $surface)
    {
        $surface->delete();
        return [];
    }
}
