<?php

namespace App\Http\Controllers\Api\Admin;

use App\Services\UploadService;

use App\Http\Resources\Admin\ImageResource;

use App\Http\Requests\Admin\ImageUploadRequest;
use App\Http\Requests\Admin\ImageMultipleUploadRequest;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ImageController extends Controller
{
    public function store(ImageUploadRequest $request, UploadService $uploadService)
    {
        $file = $request->file('image');
        $image = $uploadService->uploadImage($file);
        return new ImageResource($image);
    }

    public function multiple(ImageMultipleUploadRequest $request, UploadService $uploadService)
    {
        $files = $request->images;

        $images = array_map(function ($file) use ($uploadService) {
            return $uploadService->uploadImage($file);
        }, $files);

        return ImageResource::collection(collect($images));
    }
}
