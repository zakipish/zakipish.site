<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ServiceRequest;
use App\Http\Resources\Admin\ServiceResource;
use App\Models\Service;
use App\Models\Utils\Paginator;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    use Paginator;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $services = Service::orderBy('id');
        $services = $this->getAllOrPaginate($services, $request);
        return ServiceResource::collection($services);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ServiceRequest $request
     * @return ServiceResource
     */
    public function store(ServiceRequest $request)
    {
        $model = new Service($request->validated());
        $model->save();
        return new ServiceResource($model);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Service $service
     * @return ServiceResource
     */
    public function show(Service $service)
    {
        return new ServiceResource($service);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Service $service
     * @return void
     */
    public function edit(Service $service)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ServiceRequest $request
     * @param  \App\Models\Service $service
     * @return ServiceResource
     */
    public function update(ServiceRequest $request, Service $service)
    {
        $service->update($request->validated());
        return new ServiceResource($service);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Service $service
     * @return void
     * @throws \Exception
     */
    public function destroy(Service $service)
    {
        $service->delete();
        return [];
    }
}
