<?php

namespace App\Http\Controllers\Api\Admin;

use App\Models\Event;
use App\Models\Utils\Paginator;

use App\Http\Resources\Admin\EventResource;
use App\Http\Resources\Admin\Event\UserResource as EventUserResource;

use App\Http\Requests\Admin\Event\CreateRequest;
use App\Http\Requests\Admin\Event\UpdateRequest;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EventController extends Controller
{
    use Paginator;

    public function index(Request $request)
    {
        $events = Event::query()
            ->with(['slugs', 'activeSlugs', 'club', 'club.playgrounds', 'playground', 'sport', 'gameLevel', 'gameLevel', 'gameLevel.activeSlugs', 'images', 'busyRecord', 'users']);
        $events = $this->getAllOrPaginate($events, $request);
        return EventResource::collection($events);
    }

    public function show(Request $request, Event $event)
    {
        $event->load(['slugs', 'activeSlugs', 'club', 'club.playgrounds', 'playground', 'sport', 'gameLevel', 'gameLevel', 'gameLevel.activeSlugs', 'images', 'busyRecord', 'users']);
        return new EventResource($event);
    }

    public function store(CreateRequest $request)
    {
        $event = new Event($request->validated());
        $event->owner_id = Auth::id();
        $event->save();
        return new EventResource($event);
    }

    public function update(UpdateRequest $request, Event $event)
    {
        $event->update($request->validated());
        return new EventResource($event);
    }
}
