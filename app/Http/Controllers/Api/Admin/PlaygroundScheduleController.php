<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\PlaygroundScheduleCreateRequest;
use App\Http\Requests\PlaygroundScheduleListRequest;
use App\Http\Requests\PlaygroundScheduleUpdateManyRequest;
use App\Http\Requests\PlaygroundScheduleUpdateRequest;
use App\Http\Resources\Admin\PlaygroundScheduleResource;
use App\Models\PlaygroundSchedule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;

class PlaygroundScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param PlaygroundScheduleListRequest $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(PlaygroundScheduleListRequest $request)
    {
        $perPage = (int)$request->get('per_page');
        if ($perPage < 1 or $perPage > 1000) {
            $perPage = config('database.paginate');
        }
        $playgroundId = $request->get('playground_id');
        $query = PlaygroundSchedule::where('playground_id', $playgroundId)->orderBy('date', 'interval');
        return PlaygroundScheduleResource::collection($query->paginate($perPage));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param PlaygroundScheduleCreateRequest $request
     * @return PlaygroundScheduleResource
     */
    public function store(PlaygroundScheduleCreateRequest $request)
    {
        $model = new PlaygroundSchedule($request->validated());
        $model->save();
        return new PlaygroundScheduleResource($model);
    }

    /**
     * Display the specified resource.
     *
     * @param PlaygroundSchedule $playgroundSchedule
     * @return PlaygroundScheduleResource
     * @internal param \App\Models\Game $game
     */
    public function show(PlaygroundSchedule $playgroundSchedule)
    {
        return new PlaygroundScheduleResource($playgroundSchedule);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param PlaygroundScheduleUpdateRequest $request
     * @param PlaygroundSchedule $playgroundSchedule
     * @return PlaygroundScheduleResource
     */
    public function update(PlaygroundScheduleUpdateRequest $request, PlaygroundSchedule $playgroundSchedule)
    {
        $playgroundSchedule->update($request->validated());
        return new PlaygroundScheduleResource($playgroundSchedule);
    }

    protected function scheduleValidator()
    {
        return validator([
            'price' => 'float|nullable',
        ]);
    }

    /**
     * @param PlaygroundScheduleUpdateManyRequest $request
     */
    public function updateMany(PlaygroundScheduleUpdateManyRequest $request)
    {
        $data = $request->get('data');
        $validator = $this->scheduleValidator();
        foreach ($data as $item) {
            Log::info('$item', $item);
            $validated = $validator->validate($item);
            Log::info('$validated', $validated);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param PlaygroundSchedule $playgroundSchedule
     * @return array
     * @throws \Exception
     */
    public function destroy(PlaygroundSchedule $playgroundSchedule)
    {
        $playgroundSchedule->delete();
        return [];
    }
}
