<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\GameRequest;
use App\Http\Resources\Admin\GameResource;
use App\Models\Game;
use App\Models\Utils\Paginator;
use App\Services\PriceService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class GameController extends Controller
{
    use Paginator;

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $games = Game::orderBy('id');
        $games = $this->getAllOrPaginate($games, $request);

        return GameResource::collection($games);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param GameRequest $request
     * @return GameResource
     */
    public function store(GameRequest $request)
    {
        $game = new Game($request->validated());
        $game->save();
        return new GameResource($game);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Game $game
     * @return GameResource
     */
    public function show(Game $game)
    {
        return new GameResource($game);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Game $game
     * @return void
     */
    public function edit(Game $game)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param GameRequest $request
     * @param  \App\Models\Game $game
     * @return GameResource
     */
    public function update(GameRequest $request, Game $game)
    {
        $game = $game->fill($request->validated());
        $game->save();
        return new GameResource($game);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Game $game
     * @return void
     * @throws \Exception
     */
    public function destroy(Game $game)
    {
        $game->delete();
        return [];
    }
}
