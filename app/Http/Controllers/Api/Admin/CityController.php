<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Requests\CityRequest;
use App\Http\Resources\Admin\CityResource;
use App\Models\City;
use App\Models\Utils\Paginator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CityController extends Controller
{
    use Paginator;

    public function index(Request $request)
    {

        $cities = City::orderBy('id');
        $cities->with(['slugs', 'activeSlugs']);
        $cities = $this->getAllOrPaginate($cities, $request);

        return CityResource::collection($cities);
    }

    public function store(CityRequest $request)
    {
        $model = new City($request->validated());
        $model->save();
        return new CityResource($model);
    }

    public function show(City $city)
    {
        return new CityResource($city);
    }


    public function update(CityRequest $request, City $city)
    {
        $city->update($request->validated());
        return new CityResource($city);
    }

    public function destroy(City $city)
    {
        $city->delete();
        return [];
    }
}
