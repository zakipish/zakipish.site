<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\GameStatusRequest;
use App\Http\Resources\Admin\GameStatusResource;
use App\Models\GameStatus;

class GameStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return GameStatusResource::collection(GameStatus::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param GameStatusRequest $request
     * @return GameStatusResource
     */
    public function store(GameStatusRequest $request)
    {
        $model = new GameStatus($request->validated());
        $model->save();
        return new GameStatusResource($model);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\GameStatus $gameStatus
     * @return GameStatusResource
     */
    public function show(GameStatus $gameStatus)
    {
        return new GameStatusResource($gameStatus);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\GameStatus $gameStatus
     * @return void
     */
    public function edit(GameStatus $gameStatus)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param GameStatusRequest $request
     * @param  \App\Models\GameStatus $gameStatus
     * @return GameStatusResource
     */
    public function update(GameStatusRequest $request, GameStatus $gameStatus)
    {
        $gameStatus->update($request->validated());
        return new GameStatusResource($gameStatus);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\GameStatus $gameStatus
     * @return void
     * @throws \Exception
     */
    public function destroy(GameStatus $gameStatus)
    {
        $gameStatus->delete();
        return [];
    }
}
