<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Requests\InfrastructureRequest;
use App\Http\Resources\Admin\InfrastructureResource;
use App\Models\Infrastructure;
use App\Models\Utils\Paginator;
use Illuminate\Http\Request;
use App\Services\UploadService;
use App\Http\Controllers\Controller;

class InfrastructureController extends Controller
{
    use Paginator;

    public function index(Request $request)
    {
        $infrastructures = Infrastructure::orderBy('id');
        $infrastructures = $this->getAllOrPaginate($infrastructures, $request);
        return InfrastructureResource::collection($infrastructures);
    }

    public function store(InfrastructureRequest $request)
    {
        $model = new Infrastructure($request->validated());
        $model->save();
        return new InfrastructureResource($model);
    }

    public function show(Infrastructure $infrastructure)
    {
        return new InfrastructureResource($infrastructure);
    }

    public function update(InfrastructureRequest $request, Infrastructure $infrastructure,UploadService $uploadService)
    {
        if ($file = $request->file('icon')) {
            $image = $uploadService->uploadImage($file);
            $infrastructure->icon()->associate($image);
            $infrastructure->save();
        } else if ($request->action == "delete_logo") {
            $infrastructure->icon()->dissociate();
            $infrastructure->save();
            return 1;
        }
        $infrastructure->update($request->validated());
        return new InfrastructureResource($infrastructure);
    }

    public function destroy(Infrastructure $infrastructure)
    {
        $infrastructure->delete();
        return [];
    }
}
