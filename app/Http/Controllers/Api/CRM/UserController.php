<?php

namespace App\Http\Controllers\Api\CRM;

use App\Repositories\CRM\UserRepository;

use App\Http\Resources\CRM\UserSearchResource;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    protected $repository;

    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $users = $this->repository->search($request);
        return UserSearchResource::collection($users);
    }
}
