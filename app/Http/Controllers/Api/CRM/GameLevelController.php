<?php

namespace App\Http\Controllers\Api\CRM;

use App\Models\GameLevel;
use App\Http\Resources\CRM\GameLevelResource;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class GameLevelController extends Controller
{
    public function index(Request $request)
    {
        $gameLevels = GameLevel::all();
        return GameLevelResource::collection($gameLevels);
    }
}
