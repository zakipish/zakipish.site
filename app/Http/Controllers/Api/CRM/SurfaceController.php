<?php

namespace App\Http\Controllers\Api\CRM;

use App\Http\Resources\CRM\SurfaceResource;
use App\Models\Surface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SurfaceController extends Controller
{
    public function index(Request $request)
    {

        $surfaces = Surface::orderBy('id')->get();

        return SurfaceResource::collection($surfaces);
    }
}
