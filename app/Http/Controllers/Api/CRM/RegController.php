<?php

namespace App\Http\Controllers\Api\CRM;

use App\Models\User;
use App\Http\Resources\Front\UserResource;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Auth\Events\Registered;

class RegController extends Controller
{
    use RegistersUsers;

    public function __construct()
    {
        $this->middleware('guest');
    }

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();
        event(new Registered($user = $this->create($request->all())));
        $this->guard()->login($user);
        return $this->registered($request, $user)
                ?: new UserResource($user);
    }

    protected function create(array $data)
    {
        $phone = array_get($data, 'phone');
        $phone = $phone ? trim(strtolower($phone)) : null;

        $email = array_get($data, 'email');
        $email = $email ? trim(strtolower($email)) : null;

        return User::create([
            'name' => $data['name'],
            'phone' => $phone,
            'email' => $email,
            'password' => Hash::make($data['password']),
        ]);
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'surname' => 'required|string|max:255',
            'patronymic' => 'required|string|max:255',
            'company' => 'string|max:255',
            'phone' => 'required_without_all:email|nullable|string|digits:10|unique:users',
            'email' => 'required_without_all:phone|nullable|string|email|max:255|unique:users',
//            'password' => 'required|string|min:6',
        ]);
    }
}
