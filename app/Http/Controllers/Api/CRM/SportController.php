<?php

namespace App\Http\Controllers\Api\CRM;

use App\Http\Resources\CRM\SportResource;
use App\Models\Sport;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SportController extends Controller
{
    public function index(Request $request)
    {
        $sports = Sport::orderBy('id')->with(['activeSlugs', 'icon']);
        if ($request->has('root') && $request->get('root')) {
            $sports->whereRaw('nlevel(sports.label) = 1');
        }

        $sports = $sports->get();
        return SportResource::collection($sports);
    }
}
