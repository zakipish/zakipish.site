<?php

namespace App\Http\Controllers\Api\CRM;

use App\Models\Event;

use App\Http\Resources\CRM\EventResource;

use App\Http\Requests\CRM\Event\CreateRequest;
use App\Http\Requests\CRM\Event\UpdateRequest;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EventController extends Controller
{
    public function index(Request $request)
    {
        $events = Auth::user()->moderatedClubs()
            ->with(['events', 'events.activeSlugs', 'events.club', 'events.playground', 'events.sport', 'events.gameLevel'])
            ->get()->pluck('events')->flatten();
        return EventResource::collection($events);
    }

    public function show(Request $request, Event $event)
    {
        $event->load(['activeSlugs', 'club', 'playground', 'sport', 'gameLevel']);
        return new EventResource($event);
    }

    public function store(CreateRequest $request)
    {
        $event = new Event($request->validated());
        $event->owner_id = Auth::id();
        $event->save();
        return new EventResource($event);
    }

    public function update(UpdateRequest $request, Event $event)
    {
        $event->update($request->validated());
        return new EventResource($event);
    }
}
