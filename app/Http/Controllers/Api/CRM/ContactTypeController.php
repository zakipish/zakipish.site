<?php

namespace App\Http\Controllers\Api\CRM;

use App\Http\Resources\CRM\ContactTypeResource;
use App\Models\ContactType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContactTypeController extends Controller
{
    public function index(Request $request)
    {
        $contactTypes = ContactType::orderBy('id');
        $contactTypes = $contactTypes->get();
        return ContactTypeResource::collection($contactTypes);
    }
}
