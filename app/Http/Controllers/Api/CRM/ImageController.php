<?php

namespace App\Http\Controllers\Api\CRM;

use App\Services\UploadService;

use App\Http\Resources\CRM\ImageResource;

use App\Http\Requests\CRM\ImageUploadRequest;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ImageController extends Controller
{
    public function store(ImageUploadRequest $request, UploadService $uploadService)
    {
        $file = $request->file('image');
        $image = $uploadService->uploadImage($file);
        return new ImageResource($image);
    }
}
