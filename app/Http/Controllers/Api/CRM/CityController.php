<?php

namespace App\Http\Controllers\Api\CRM;

use App\Http\Resources\CRM\CityResource;
use App\Models\City;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CityController extends Controller
{
    public function index(Request $request)
    {

        $cities = City::orderBy('id')->with('activeSlugs')->get();

        return CityResource::collection($cities);
    }
}
