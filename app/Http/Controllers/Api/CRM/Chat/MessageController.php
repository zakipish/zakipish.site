<?php

namespace App\Http\Controllers\Api\CRM\Chat;

use App\Models\User;
use App\Models\Chat;
use App\Models\ChatMessage;

use App\Http\Resources\CRM\Chat\MessageResource;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class MessageController extends Controller
{
    public $chatService;

    public function __construct()
    {
        $this->chatService = app('ChatService');
    }

    public function index(Chat $chat){
        $messages = $chat->messages()->with('user')->orderBy('created_at')->get();
        return MessageResource::collection($messages);
    }

    public function store(Chat $chat, Request $request){
        $user = Auth::user();
        if ($chat->chatable->users()->where('user_id', $user->id)->count() === 0 &&
            $chat->chatable->club->moderators()->where('users.id', $user->id)->count() === 0)
            return abort(403);


        $chatMessage = new ChatMessage([
            'user_id'   => $user->id,
            'chat_id'   => $chat->id,
            'message'   => $request->message,
            'type'      => ChatMessage::TYPE_MESSAGE
        ]);

        try{
            $chatMessage->save();
            $channelId = 'chat-' . $chat->id;
            $this->chatService->push($chatMessage, $channelId);

            return response()->json(['status' => 1]);
        }catch (\Exception $e){
            Log::error($e->getMessage());
            return response()->json(['status' => 0]);
        }

    }
}
