<?php

namespace App\Http\Controllers\Api\CRM;

use App\Models\Playground;

use App\Http\Requests\CRM\PlaygroundCreateRequest;
use App\Http\Requests\CRM\PlaygroundUpdateRequest;
use Illuminate\Http\Request;

use App\Http\Resources\CRM\PlaygroundResource;
use App\Http\Resources\CRM\PlaygroundWithClubResource;
use App\Http\Resources\CRM\PlannerResource;

use App\Services\UploadService;
use App\Services\DuplicateIntoParrent;

use App\Repositories\PlaygroundRepository;
use App\Repositories\CRM\Playground\PlannerRepository;

use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class PlaygroundController extends Controller
{
    public function __construct(PlaygroundRepository $playgroundRepository, PlannerRepository $plannerRepository)
    {
        $this->repository = $playgroundRepository;
        $this->plannerRepository = $plannerRepository;
    }

    public function show(Playground $playground)
    {
        $playground->load(['images', 'surfaces', 'services', 'sports', 'equipments', 'club']);
        $playground->position = DB::table('playgrounds')
                ->selectRaw('(select count(id) from playgrounds where id <= ? and club_id = ?) as position', [$playground->id, $playground->club_id])
                ->get()->first()->position;
        return new PlaygroundWithClubResource($playground);
    }

    public function update(PlaygroundUpdateRequest $request, Playground $playground, UploadService $uploadService)
    {
        if($files = $request->file('image')){
            $i = $playground->images()->count();
            $out = [];
            foreach ($files as $file){
                $image = $uploadService->uploadImage($file);
                $playground->images()->attach($image->id, ['order' => $i++]);
                $out[] = ["id"=>$image->id, 'order'=>$i++];
            }
            $playground->save();
            return $out;
        }else if ($request->action == "update_images_order"){
            $i = 0;
            $images = $request->images;
            $image_ids = $request->image_ids;
            foreach ($images as $id=>$image){
                $playground->images()->updateExistingPivot((int)$image_ids[$id],['order'=>$i]);
                $i++;

            }
            $playground->save();
        }else if ($request->action == "delete_image") {
            $playground->images()->find((int)$request->id)->delete();
            $playground->save();
            return 1;
        }else if ($request->input('imageUrl')) {
//            upload image from url
            $path = $request->input('imageUrl');
            $file = $uploadService->prepareRemoteImage($path);
            $result = $uploadService->uploadImage($file);
            $order = $playground->images()->count()+1;
            $out = [];
            if ($request->input('field') === 'image[]'){
                $playground->images()->attach($result->id, ['order' => $order]);
            }
            $out[] = ["id"=>$result->id, "order"=>$order, "preview" => asset('/storage/source/' . $result->code)];
            $playground->save();
            return $out;
        } else if ($request->action == "delete_avatar") {
            $playground->avatar()->dissociate();
            $playground->save();
            return 1;
        } else {
            $playground->update($request->validated());
            $playground->touch();
        }

        return new PlaygroundResource($playground);
    }

    public function prices(Request $request, Playground $playground)
    {
        $planner = $this->plannerRepository->getPrices($request, $playground);
        //return new PlannerResource($planner);
        return response()->json(['data' => $planner]);
    }

}
