<?php

namespace App\Http\Controllers\Api\CRM;

use App\Http\Requests\InfrastructureRequest;
use App\Http\Resources\CRM\InfrastructureResource;
use App\Models\Infrastructure;
use App\Models\Utils\Paginator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InfrastructureController extends Controller
{
    public function index(Request $request)
    {
        $infrastructures = Infrastructure::orderBy('id');
        $infrastructures = $infrastructures->get();
        return InfrastructureResource::collection($infrastructures);
    }
}
