<?php

namespace App\Http\Controllers\Api\CRM\Club;

use App\Models\Club;
use App\Models\Playground;

use App\Http\Requests\CRM\PlaygroundCreateRequest;
use Illuminate\Http\Request;

use App\Http\Resources\CRM\PlaygroundResource;

use App\Http\Controllers\Controller;

class PlaygroundController extends Controller
{
    public function store(PlaygroundCreateRequest $request, Club $club)
    {
        $playground = $club->playgrounds()->create($request->validated());
        return new PlaygroundResource($playground);
    }
}
