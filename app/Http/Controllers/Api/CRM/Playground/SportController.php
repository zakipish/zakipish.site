<?php

namespace App\Http\Controllers\Api\CRM\Playground;

use App\Http\Resources\CRM\SportResource;

use App\Models\Playground;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SportController extends Controller
{
    public function index(Request $request, Playground $playground)
    {
        $sports = $playground->sports()->orderBy('id')->with(['icon']);
        if ($request->has('root') && $request->get('root')) {
            $sports->whereRaw('nlevel(sports.label) = 1');
        }

        $sports = $sports->get();
        return SportResource::collection($sports);
    }
}
