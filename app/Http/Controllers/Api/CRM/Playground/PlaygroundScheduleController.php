<?php

namespace App\Http\Controllers\Api\CRM\Playground;

use App\Models\Playground;

use App\Repositories\CRM\PlaygroundScheduleRepository;

use App\Http\Requests\CRM\PlaygroundScheduleCreateRequest;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class PlaygroundScheduleController extends Controller
{
    protected $repository;

    public function __construct(PlaygroundScheduleRepository $repository)
    {
        $this->repository = $repository;
    }

    public function store(PlaygroundScheduleCreateRequest $request, Playground $playground)
    {
        $playgroundSchedules = $this->repository->create($playground, $request->validated());
        return response()->json([]);
    }
}
