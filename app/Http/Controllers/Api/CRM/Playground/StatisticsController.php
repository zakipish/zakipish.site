<?php

namespace App\Http\Controllers\Api\CRM\Playground;

use App\Models\Playground;

use App\Repositories\CRM\Playground\StatisticsRepository;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \DateTime;
use Illuminate\Support\Facades\Log;

class StatisticsController extends Controller
{
    protected $repository;

    public function __construct(StatisticsRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request, Playground $playground)
    {
        $startDate = new DateTime();
        $endDate = (clone $startDate)->modify('+14 days');
        $statistics = $this->repository->dashboard($playground, $startDate, $endDate);

        return response()->json(['data' => $statistics]);
    }
}
