<?php

namespace App\Http\Controllers\Api\CRM\Playground;

use App\Models\User;
use App\Models\Game;
use App\Models\Playground;
use App\Models\Transaction;

use App\Http\Resources\CRM\GameResource;

use App\Http\Requests\CRM\GameRequest;

use App\Services\PriceService;

use Carbon\Carbon;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class GameController extends Controller
{
    public function index(Request $request, Playground $playground)
    {
        $games = $playground->games()->with(['busyRecord', 'owner', 'owner.avatarsDesc']);

        if ($request->has('dateStart') && $request->has('dateEnd'))
        {
            $period = [$request->dateStart, $request->dateEnd];
            $games->whereHas('busyRecord', function($q) use ($period) {
                $q->whereBetween('date', $period);
            });
        }

        $games = $games->get();
        return GameResource::collection($games);
    }

    public function store(GameRequest $request, Playground $playground)
    {
        $gameData = array_except($request->validated(), ['owner']);

        $game = new Game();
        $game->playground_id = $playground->id;
        $game->fill($gameData);

        if (empty($request->get('owner_id')) && $request->has('owner') && !empty($request->owner)) {
            $owner = User::create($request->get('owner'));
            $game->owner()->associate($owner);
        }

        if (!$game->price) {
            $game->price = PriceService::getPriceForInterval($game->playground, $game->date, $request->validated()['interval']);
        }

        $game->save();

        return new GameResource($game);
    }

    public function update(GameRequest $request, Playground $playground, Game $game)
    {
        $gameData = array_except($request->validated(), ['owner']);

        $game->update($request->validated());

        if (empty($request->get('owner_id')) && $request->has('owner')) {
            $owner = User::create($request->get('owner'));
            $game->owner()->associate($owner);
        }

        $game->price = PriceService::getPriceForInterval($game->playground, $game->date, $request->validated()['interval']);
        $game->save();

        return new GameResource($game);
    }
}
