<?php

namespace App\Http\Controllers\Api\CRM;

use App\Events\ClubChanged;
use App\Http\Requests\CRM\ClubCreateRequest;
use App\Http\Requests\CRM\ClubUpdateRequest;
use App\Http\Resources\CRM\ClubResource;
use App\Http\Resources\CRM\ClubUserResource;
use App\Http\Resources\CRM\ClubTinyResource;
use App\Models\Club;
use App\Http\Controllers\Controller;
use App\Models\Sport;
use App\Models\Tasks\ClubAmoCrmSyncTask;
use App\Services\UploadService;
use App\Services\DuplicateIntoParrent;
use App\Services\GeoPointModifierService;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ClubController extends Controller
{
    public function index(Request $request)
    {
        $clubs = Auth::user()->moderatedClubs;
        $clubs->load([
            'sports', 'sports.icon',
            'playgrounds' => function($q) {
                $q->orderBy('id');
            },
            'transactions'
        ]);
        return ClubTinyResource::collection($clubs);
    }

    public function show(Club $club)
    {
        $club->load([
            'images',
            'playgrounds' => function ($q) {
                $q->orderBy('id');
            },
            'infrastructures',
            'contacts',
            'playgrounds.slugs',
            'playgrounds.surfaces',
            'playgrounds.services',
            'playgrounds.images',
            'playgrounds.sports',
            'playgrounds.equipments',
            'playgrounds.club',
            'playgrounds.club.images', //to load images in avatar
        ]);
        return new ClubResource($club);
    }


    public function update(ClubUpdateRequest $request, Club $club, UploadService $uploadService, GeoPointModifierService $modifier)
    {
        if ($file = $request->file('logo')) {
            $image = $uploadService->uploadImage($file);
            $club->logo()->associate($image);

            $modifier->modify($club);

            $club->save();
        } else if ($files = $request->file('image')) {
            $i = $club->images()->count();
            $out = [];
            foreach ($files as $file) {
                $image = $uploadService->uploadImage($file);
                $club->images()->attach($image->id, ['order' => $i++]);
                $out[] = ["id" => $image->id, 'order' => $i++];
            }
            $club->save();
            if ($club->parent_club_id) {
                $helper = new DuplicateIntoParrent($club);
                $helper->duplicate_images();
            }
            return $out;
        }else if ($request->action == "update_images_order") {
            $i = 0;
            $images = $request->images;
            $image_ids = $request->image_ids;
            foreach ($images as $id => $image) {
                $club->images()->updateExistingPivot((int)$image_ids[$id], ['order' => $i]);
                $i++;
            }
            $club->save();
        }else if ($request->action == "delete_image") {
            $club->images()->find((int)$request->id)->delete();
            $club->save();
            return 1;
        } else if ($request->action == "delete_logo") {
            $club->logo()->dissociate();
            $club->save();
            return 1;
        }
        $club->update($request->validated());
        $club->save();
        event(new ClubChanged($club));
        return new ClubResource($club);
    }
}
