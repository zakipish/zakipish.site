<?php

namespace App\Http\Controllers\Api\Front;

use Illuminate\Support\Facades\Auth;
use App\Services\PriceService;
use App\Http\Requests\GameUpdateRequest;
use App\Http\Requests\GameCreateRequest;

use App\Http\Resources\Front\Filter\GamesCollection;
use App\Http\Resources\Front\Filter\GameResource as FilterGameResource;
use App\Http\Resources\Front\GameResource;
use App\Http\Resources\Front\GameCardResource;
use App\Http\Resources\Front\FilterDatesResource;
use App\Http\Resources\Front\FilterTimesResource;
use App\Models\Game;
use App\Models\Playground;
use App\Models\ChatMessage;
use App\Models\Utils\Paginator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use \DateTime;

use App\Repositories\GameRepository;

use Illuminate\Support\Facades\Log;

class GameController extends Controller
{

    public $chatService;

    public function __construct(GameRepository $gameRepository)
    {
        $this->repository = $gameRepository;
        $this->chatService = app('ChatService');
    }

    public function index(Request $request)
    {
        $requester_user_id = $request->user() ? $request->user()->id : null;
        $city_id = $request->city_id;
        $sport_id = $request->sport_id;
        $date = new Carbon($request->date);

        $timeStart = $date->copy();
        if ($request->time_start) {
            $timeStart->setTimeFromTimeString($request->time_start);
        } else {
            $timeStart->setTime(0, 0, 0);
        }

        $timeEnd = $date->copy();
        if ($request->time_end) {
            $timeEnd->setTimeFromTimeString($request->time_end);
        } else {
            $timeEnd->setTime(23, 59, 59);
        }

        $perPage = $request->get('perPage', 2);
        $page = $request->get('page', 1);

        $interval = [$timeStart, $timeEnd];

        $params = [
            'city_id' => $city_id,
            'sport_id' => $sport_id,
            'requester_user_id' => $requester_user_id,
            'date' => $date,
            'interval' => $interval,
            'perPage' => $perPage,
            'page' => $page,
        ];
        $games = $this->repository->getGames($params);

        $paramsTotal = [
            'city_id' => $city_id,
            'sport_id' => $sport_id,
            'requester_user_id' => $requester_user_id,
            'date' => $date,
            'interval' => $interval,
        ];
        $total = $this->repository->getCount($paramsTotal);

        return new GamesCollection(collect(['data' => $games, 'total' => $total]));
    }

    public function my(Request $request)
    {
        $games = Auth::user()->games()
                 ->with([
                     'users', 'users.avatarsDesc', 'users.sportsDesc', 'users.sportsDesc',
                     'playground', 'playground.club'
                 ])
                 ->get();
        return GameCardResource::collection($games);
    }

    public function store(GameCreateRequest $request)
    {
        $game = new Game($request->validated());
        $playground = Playground::find($request->playground_id);
        $game->owner_id = Auth::id();
        $game->price = PriceService::getPriceForInterval($playground, $game->date, $request->validated()['interval']);

        $game->save();

        $game->users()->attach(Auth::id());

        if ($request->friend_id && Auth::user()->hasFriend($request->friend_id))
            $game->users()->attach($request->friend_id);

        return new GameResource($game);
    }

    public function show(Game $game)
    {
        $game->load(['chat']);
        return new GameResource($game);
    }

    public function update(GameUpdateRequest $request, Game $game){
        $game->update($request->validated());
        return new GameResource($game);
    }

    public function dates(Request $request)
    {
        $requester_user_id = $request->user() ? $request->user()->id : null;
        $city_id = $request->city_id;
        $sport_id = $request->sport_id;

        $dateStart = null;
        if ($request->date) $dateStart = new Carbon($request->date);

        $params = [
            'requester_user_id' => $requester_user_id,
            'city_id' => $city_id,
            'sport_id' => $sport_id,
            'dateStart' => $dateStart,
        ];
        $dates = $this->repository->getDates($params);

        return response()->json(['data' => $dates]);
    }

    public function times(Request $request)
    {
        $requester_user_id = $request->user() ? $request->user()->id : null;
        $city_id = $request->city_id;
        $sport_id = $request->sport_id;
        $date = new Carbon($request->date);

        $params = [
            'requester_user_id' => $requester_user_id,
            'city_id' => $city_id,
            'sport_id' => $sport_id,
            'date' => $date,
        ];
        $times = $this->repository->getTimes($params);
        return response()->json(['data' => $times]);
    }

    public function attach(Request $request,$id)
    {
        $game = Game::findOrFail($id);
        $game->users()->attach(Auth::id());

        try {
            $chatMessage = new ChatMessage([
                'game_id'   => $game->id,
                'message'   => "Новый игрок " . Auth::user()->fullName . " присоединился к игре.",
                'type'      => ChatMessage::TYPE_SERVICE
            ]);

            $chatMessage->save();
            $channelId = 'game-' . $game->id;

            $this->chatService->push($chatMessage, $channelId);
        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }
        return new GameResource($game);
    }

    public function unattach(Request $request,$id)
    {
        $game = Game::findOrFail($id);
        $game->users()->detach(Auth::id());

        try {
            $chatMessage = new ChatMessage([
                'game_id'   => $game->id,
                'message'   => Auth::user()->fullName . " вышел из игры.",
                'type'      => ChatMessage::TYPE_SERVICE
            ]);

            $chatMessage->save();
            $channelId = 'game-' . $game->id;

            $this->chatService->push($chatMessage, $channelId);
        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }
        return new GameResource($game);
    }
}
