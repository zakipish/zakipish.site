<?php

namespace App\Http\Controllers\Api\Front;

use App\Http\Resources\Front\FriendResource;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class FriendController extends Controller
{
    public function index(Request $request)
    {
        $friends = Auth::user()->friends();

        return FriendResource::collection($friends);
    }
}
