<?php

namespace App\Http\Controllers\Api\Front;

use App\Models\User;
use App\Models\Chat;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class ChatController extends Controller
{
    public function auth(Request $request, Chat $chat)
    {
        $user = Auth::user();
        if(empty($user)){
            return response()->json([
                'auth' => false
            ]);
        }
        if(empty($chat)){
            return response()->json([
                'auth' => false
            ]);
        }
        if ($chat->chatable->users()->where('user_id', $user->id)->count() === 0 &&
            $chat->chatable->club->moderators()->where('users.id', $user->id)->count() === 0)
            return response()->json([
                'auth' => false
            ]);

        return response()->json([
            'auth' => true,
        ]);
    }
}
