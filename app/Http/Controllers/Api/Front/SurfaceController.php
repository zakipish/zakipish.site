<?php

namespace App\Http\Controllers\Api\Front;

use App\Models\Surface;

use App\Http\Resources\Front\SurfaceResource;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SurfaceController extends Controller
{
    public function index(Request $request) {
        $surfaces = Surface::all();
        return SurfaceResource::collection($surfaces);
    }
}
