<?php

namespace App\Http\Controllers\Api\Front;

use App\Models\Event;

use App\Http\Resources\Front\Filter\EventsCollection;
use App\Http\Requests\Front\EventParticipationRequest;

use App\Http\Resources\Front\EventResource;
use App\Http\Resources\Front\Event\UserResource as EventUserResource;

use App\Repositories\EventRepository;

use App\Http\Controllers\Controller;

use Carbon\Carbon;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EventController extends Controller
{
    public function __construct(EventRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $requester_user_id = $request->user() ? $request->user()->id : null;
        $city_id = $request->city_id;
        $sport_id = $request->sport_id;
        $date = new Carbon($request->date);

        $timeStart = $date->copy();
        if ($request->time_start) {
            $timeStart->setTimeFromTimeString($request->time_start);
        } else {
            $timeStart->setTime(0, 0, 0);
        }

        $timeEnd = $date->copy();
        if ($request->time_end) {
            $timeEnd->setTimeFromTimeString($request->time_end);
        } else {
            $timeEnd->setTime(23, 59, 59);
        }

        $interval = [$timeStart, $timeEnd];

        $perPage = $request->get('perPage', 2);
        $page = $request->get('page', 1);

        $params = [
            'city_id' => $city_id,
            'sport_id' => $sport_id,
            'date' => $date,
            'requester_user_id' => $requester_user_id,
            'interval' => $interval,
            'perPage' => $perPage,
            'page' => $page,
        ];
        $events = $this->repository->getEvents($params);

        $paramsTotal = [
            'city_id' => $city_id,
            'sport_id' => $sport_id,
            'date' => $date,
            'requester_user_id' => $requester_user_id,
            'interval' => $interval,
            'perPage' => $perPage,
            'page' => $page,
        ];
        $total = $this->repository->getCount($paramsTotal);

        return new EventsCollection(collect(['data' => $events, 'total' => $total]));
    }

    public function show(Request $request, Event $event)
    {
        $event->load(['activeSlugs', 'club', 'playground', 'sport', 'gameLevel']);
        return new EventResource($event);
    }

    public function participate(EventParticipationRequest $request, Event $event)
    {
        if (!($event->users()->updateExistingPivot(Auth::id(), $request->validated()))) {
            $event->users()->attach(Auth::id(), $request->validated());
        }

        $user = $event->users()->find(Auth::id());

        return new EventUserResource($user);
    }
}
