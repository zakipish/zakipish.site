<?php

namespace App\Http\Controllers\Api\Front;

use App\Models\User;
use App\Http\Requests\UserUpdateRequest;
use App\Http\Resources\Front\UserResource;
use App\Http\Controllers\Controller;
use App\Services\UploadService;
use http\Env\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;

use App\Services\TmpPasswordService;
use App\Services\ResizeService;
use Psy\Util\Json;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function update(Request $request)
    {
        $validation = [
            'name' => 'filled|string',
            'lastName' => 'string|nullable',
            'midName' => 'string|nullable',
            'birthDate' => 'string|nullable',
            'city_id' => 'integer|nullable',
            'about' => 'string|nullable',
            'height' => 'integer|nullable',
            'weight' => 'integer|nullable',
            'gender' => 'integer|nullable',
            'sports' => 'array',
            'sports.game_level' => 'integer',
            'sports.game_exp' => 'integer'
        ];
//        return $request->oldPassword;
        $credValidation = [
            'email' => ['required_without_all:phone', 'nullable', 'email', Rule::unique('users')->ignore(Auth::id())],
            'phone' => ['required_without_all:email', 'nullable', Rule::unique('users')->ignore(Auth::id()), 'digits:10']
        ];

        $imageValidation = [
            'image' => 'image|mimes:jpeg,png'
        ];

        $passwordValidation = [
            'oldPassword' => 'string|nullable',
            'newPassword' => 'min:6|filled|string',
            'confirmedPassword' => 'same:newPassword|filled|string',
        ];

        if ($request->has('email') || $request->has('phone')) {
            $validation += $credValidation;
        }

        if ($request->file('image')) {
            $validation += $imageValidation;
        }

        if ($request->has('newPassword') && !empty($request->get('newPassword'))) {
            $validation += $passwordValidation;
        }

        $request->validate($validation);

        $user = Auth::user();
        $file = $request->file('image');
        $oldPassword = $request->oldPassword;

        if($file){
            $uploadService = new UploadService();
            $image = $uploadService->uploadImage($file);
            $user->avatars()->attach($image->id);
            $this->warmingUpCache($image);
        }

        $user->update($request->all());
        $user->touch();

        if ($request->has('newPassword') && $request->has('confirmedPassword') && !empty($request->get('newPassword')) && !empty($request->get('confirmedPassword'))) {
            if (empty($user->getAuthPassword()) || Hash::check($oldPassword, $user->getAuthPassword()) || Hash::check($oldPassword, $user->tmp_password)) {
                $user->password = Hash::make($request->newPassword);
                $user->save();

                if ($user->tmp_password) {
                    $service = new TmpPasswordService($user);
                    $service->reset();
                    $request->session()->forget('tmp');
                }
            } else {
                return response()->json(['errors' => ['oldPassword' => ['Старый пароль введен неверно']]], 422);
            }
        }


        return new UserResource($user);
    }

    protected function warmingUpCache($image)
    {
        $resizeService = new ResizeService;
        $resizeService->process($image, 'sq24');
        $resizeService->process($image, 'wl332');
        $resizeService->process($image, 'sq50');
    }
}
