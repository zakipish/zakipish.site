<?php

namespace App\Http\Controllers\Api\Front;

use App\Models\Playground;
use App\Http\Resources\Front\PlaygroundResource;

use App\Http\Resources\Front\Filter\ClubResource as FilterClubResource;

use App\Http\Resources\Front\FilterDatesResource;
use App\Http\Resources\Front\FilterTimesResource;
use App\Http\Resources\Front\DatePriceResource;
use App\Repositories\PlaygroundRepository;
use App\Repositories\CRM\Playground\PlannerRepository;
use App\Models\Sport;
use App\Models\Utils\Paginator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \DateTime;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

class PlaygroundController extends Controller
{
    public function __construct(PlaygroundRepository $playgroundRepository, PlannerRepository $plannerRepository)
    {
        $this->repository = $playgroundRepository;
        $this->plannerRepository = $plannerRepository;
    }

    public function index(Request $request)
    {
        $start = null;
        $end = null;
        if ($request->time_start || $request->time_end) {
            $start = $request->time_start ? new Carbon($request->time_start) : (new Carbon())->setTime(0, 0, 0);
            $end = $request->time_end ? new Carbon($request->time_end) : (new Carbon())->setTime(23, 59, 59);
        }
        $interval = [$start, $end];

        $params = [
            'date' => new Carbon($request->date),
            'city_id' => $request->city_id,
            'sport_id' => $request->sport_id,
            'type' => $request->type,
            'department_id' => $request->department_id,
            'surface_id' => $request->surface_id,
            'search' => $request->search,
            'interval' => $interval,
        ];

        $playgrounds = $this->repository->getPlaygrounds($params);
        $clubs = $this->repository->groupPlaygroundsByClub($playgrounds);

        return FilterClubResource::collection($clubs);
    }

    public function dates(Request $request)
    {
        $params = [
            'start' => $request->date ? new Carbon($request->date) : new Carbon(),
            'city_id' => $request->city_id,
            'sport_id' => $request->sport_id,
        ];

        $dates = $this->repository->getDates($params);
        return response()->json(['data' => $dates]);
    }

    public function times(Request $request)
    {
        $times = $this->repository->getTimes($request);
        return response()->json(['data' => $times]);
    }

    public function show(Request $request, Playground $playground)
    {
        if (!$playground->club || !$playground->club->coordinates)
            abort(404);
        return new PlaygroundResource($playground);
    }

    public function show_dates(Request $request, Playground $playground)
    {
        $dates = $this->repository->getPlaygroundDates($request, $playground);
        return FilterDatesResource::collection($dates);
    }

    public function show_times(Request $request, Playground $playground)
    {
        $times = $this->repository->getPlaygroundTimes($request, $playground);
        return FilterTimesResource::collection($times);
    }

    public function prices(Request $request, Playground  $playground)
    {
        $prices = $this->plannerRepository->getPrices($request, $playground, true);
        return response()->json(['data' => $prices]);
    }
}
