<?php

namespace App\Http\Controllers\Api\Front;

use App\Events\ClubChanged;
use App\Http\Requests\ClubCreateRequest;
use App\Http\Requests\ClubUpdateRequest;
use App\Http\Resources\Front\ClubResource;
use App\Models\Club;
use App\Http\Controllers\Controller;
use App\Models\Sport;
use App\Models\Tasks\ClubAmoCrmSyncTask;
use App\Services\UploadService;
use App\Services\DuplicateIntoParrent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ClubController extends Controller
{

    public function show(Club $club)
    {
        if (!$club->coordinates)
            abort(404);

        $club->load(['images', 'logo',
            'playgrounds', 'playgrounds.images', 'playgrounds.surfaces',
            'playgrounds.sports',
                'playgrounds.sports.icon', 'playgrounds.sports.photo',
            'playgrounds.club.images',
            'sports', 'sports.icon', 'sports.photo',
            'infrastructures.icon']);
        return new ClubResource($club);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Club $club
     */

}
