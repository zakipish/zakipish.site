<?php

namespace App\Http\Controllers\Api\Front;

use App\Http\Resources\Front\SportResource;
use App\Models\Sport;
use App\Models\Utils\Paginator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SportController extends Controller
{
    public function index(Request $request)
    {

        $sports = Sport::query()->with(['activeSlugs', 'icon', 'photo']);

        if($request->has('root') && $request->get('root')) {
            $sports->whereRaw("nlevel(sports.label) = 1");
        }

        return SportResource::collection($sports->orderBy('id')->get());
    }
}
