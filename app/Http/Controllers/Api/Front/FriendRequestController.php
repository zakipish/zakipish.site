<?php

namespace App\Http\Controllers\Api\Front;

use App\Models\FriendRequest;

use App\Http\Resources\Front\FriendRequestResource;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class FriendRequestController extends Controller
{
    public function index(Request $request)
    {
        $friendRequests = Auth::user()->friendRequests()->with(['requester'])->awaiting()->get();

        return FriendRequestResource::collection($friendRequests);
    }

    public function store(Request $request)
    {
        $friendRequest = FriendRequest::query()->where(['requester_id' => $request->responser_id, 'responser_id' => Auth::id()])->first();

        if ($friendRequest) {
            $friendRequest->accept();
        } else {
            $friendRequest = FriendRequest::create([ 'requester_id' => Auth::id(), 'responser_id' => $request->responser_id ]);
        }
        return new FriendRequestResource($friendRequest);
    }

    public function accept(Request $request, FriendRequest $friendRequest)
    {
        if ($friendRequest->accept()) {
            return response()->json([], 200);
        } else {
            return response()->json([], 422);
        }
    }

    public function decline(Request $request, FriendRequest $friendRequest)
    {
        if ($friendRequest->decline()) {
            return response()->json([], 200);
        } else {
            return response()->json([], 422);
        }
    }

    public function deleteByResponserId(Request $request, $responser_id)
    {
        $friendRequest = FriendRequest::where(['responser_id' => $responser_id, 'requester_id' => Auth::id()])->first();
        if ($friendRequest)
            $friendRequest->delete();

        return response()->json([], 200);
    }
}
