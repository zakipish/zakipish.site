<?php

namespace App\Http\Controllers\Api\Front\User;

use Illuminate\Support\Facades\Auth;
use App\Services\PriceService;
use App\Http\Requests\GameUpdateRequest;
use App\Http\Requests\GameCreateRequest;

use App\Http\Resources\Front\FilterGameResource;
use App\Http\Resources\Front\GameResource;
use App\Http\Resources\Front\GameCardResource;
use App\Http\Resources\Front\FilterDatesResource;
use App\Http\Resources\Front\FilterTimesResource;
use App\Repositories\GameRepository;
use App\Models\Game;
use App\Models\User;
use App\Models\ChatMessage;
use App\Models\Utils\Paginator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \DateTime;
use Illuminate\Support\Facades\Log;

class GameController extends Controller
{

    public function index(Request $request, User $user)
    {
        $games = $user->games()
                 ->with([
                     'users', 'users.avatarsDesc', 'users.sportsDesc', 'users.sportsDesc',
                     'playground', 'playground.club'
                 ])
                 ->get();
        return GameCardResource::collection($games);
    }
}
