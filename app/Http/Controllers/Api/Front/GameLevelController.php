<?php

namespace App\Http\Controllers\Api\Front;

use App\Models\GameLevel;
use App\Http\Resources\Front\GameLevelResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class GameLevelController extends Controller
{
    public function index(Request $request)
    {
        $gameLevels = GameLevel::query()->with('activeSlugs')->get();
        return GameLevelResource::collection($gameLevels);
    }
}
