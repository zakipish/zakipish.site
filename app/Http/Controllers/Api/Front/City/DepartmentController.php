<?php

namespace App\Http\Controllers\Api\Front\City;

use App\Models\City;
use App\Models\Department;

use App\Http\Resources\Front\DepartmentResource;
use App\Http\Requests\DepartmentRequest;

use App\Models\Utils\Paginator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DepartmentController extends Controller
{
    use Paginator;

    public function index(Request $request, City $city)
    {
        $departments = $city->departments()->orderBy('id');
        $departments = $this->getAllOrPaginate($departments, $request);

        return DepartmentResource::collection($departments);
    }
}
