<?php

namespace App\Http\Controllers\Api\Front;

use Carbon\Carbon;

use App\Services\TmpPasswordService;
use App\Http\Controllers\Controller;
use App\Http\Resources\Front\UserResource;
use App\Models\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    public function username() {
        return 'login';
    }

    public function authenticateDefault(Request $request)
    {
        $credentials = $this->credentials($request);
        $login = strtolower($credentials['login']);
        $password = $credentials['password'];

        $field = $this->getField($login);

        if ($field == 'phone') {
            $login = $this->getPhone($login);
        }

        $login = trim(strtolower($login));

        return Auth::attempt([$field => $login, 'password' => $password]);
    }

    public function authenticateTmp(Request $request)
    {
        $credentials = $this->credentials($request);
        $login = $credentials['login'];
        $password = $credentials['password'];

        $field = $this->getField($login);

        if ($field == 'phone') {
            $login = $this->getPhone($login);
        }

        $login = trim(strtolower($login));


        $now = new Carbon();
        $user = User::query()->where($field, $login)->first();

        if ($user
            && $user->tmp_password
            && Hash::check($password, $user->tmp_password)
            && $user->tmp_password_valid_until->greaterThanOrEqualTo($now))
            return Auth::loginUsingId($user->id);
    }

    public function getField(String $value)
    {
        return filter_var($value, FILTER_VALIDATE_EMAIL) ? 'email' : 'phone';
    }

    public function getPhone(String $phone)
    {
        $matches = [];

        $phone = preg_replace('/[^0-9\+.]/', '', $phone);
        preg_match('/(7|\+7|8)?(\d{10})/', $phone, $matches);

        if ($matches) {
            return $matches[2];
        }

        return false;
    }

    public function login(Request $request)
    {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->authenticateDefault($request)) {
            return $this->sendLoginResponse($request);
        }

        if ($this->authenticateTmp($request)) {
            return $this->sendTmpLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    public function logout(Request $request)
    {
        Auth::logout();
        return response()->json(null, 200);
    }

    public function reset(Request $request)
    {
        $user = User::where('email', $request->email)->first();
        if (!$user)
            abort(404);

        $service = new TmpPasswordService($user);
        $service->create();
        $service->sendMail();

        return response()->json([], 200);
    }

    public function index(Request $request)
    {
        $user = $request->user();
        if ($user) {
            if ($request->session()->has('tmp') && $request->session()->get('tmp') == 1) {
                $user->tmp = true;
            }
            return new UserResource($user);
        } else {
            return response()->json([
            ], 403);
        }
    }

    protected function sendTmpLoginResponse(Request $request)
    {
        $request->session()->regenerate();
        $request->session()->put('tmp', 1);

        $this->clearLoginAttempts($request);

        $user = Auth::user();
        $user->tmp = true;
        return new UserResource($user);
    }

    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();
        $request->session()->forget('tmp');

        $this->clearLoginAttempts($request);

        return new UserResource(Auth::user());
    }

}
