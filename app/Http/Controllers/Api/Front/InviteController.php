<?php

namespace App\Http\Controllers\Api\Front;

use App\Models\Game;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InviteController extends Controller
{
    public function store(Request $request, Game $game)
    {
        $friend_id = $request->user_id;
        if (empty($friend_id) || !$request->user()->hasFriend($friend_id))
            return response()->json([], 403);

        $game->users()->attach($friend_id);

        return response()->json([], 201);
    }
}
