<?php

namespace App\Http\Controllers\Api\Front;

use App\Models\User;
use App\Models\FriendRequest;
use App\Http\Resources\Front\UserResource;
use App\Http\Controllers\Controller;
use App\Services\UploadService;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show(User $user)
    {
        $user->friendRequest = FriendRequest::where(['requester_id' => Auth::id(), 'responser_id' => $user->id])->orWhere(['requester_id' => $user->id, 'responser_id' => Auth::id()])->first();

        return new UserResource($user);
    }
}
