<?php

namespace App\Http\Controllers\Api\Front;

use App\Http\Resources\Front\CityResource;
use App\Models\City;
use App\Models\Utils\Paginator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CityController extends Controller
{
    use Paginator;

    public function index(Request $request)
    {

        $cities = City::orderBy('id')->with('activeSlugs');
        $cities = $this->getAllOrPaginate($cities, $request);

        return CityResource::collection($cities);
    }
}
