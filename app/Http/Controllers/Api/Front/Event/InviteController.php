<?php

namespace App\Http\Controllers\Api\Front\Event;

use App\Models\Event;
use App\Models\EventUser;

use App\Http\Resources\Front\Event\UserResource as EventUserResource;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InviteController extends Controller
{
    public function store(Request $request, Event $event)
    {
        $friend_id = $request->user_id;
        if (empty($friend_id) || !$request->user()->hasFriend($friend_id))
            return response()->json([], 403);

        $event->users()->attach($friend_id, ['type' => EventUser::TYPE_DEFINITELY]);


        $user = $event->users()->find($friend_id);

        return new EventUserResource($user);
    }
}
