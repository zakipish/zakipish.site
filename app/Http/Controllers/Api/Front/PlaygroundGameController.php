<?php

namespace App\Http\Controllers\Api\Front;

use App\Models\Playground;

use App\Http\Resources\Front\Planner\GameResource;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use \DateTime;

class PlaygroundGameController extends Controller
{
    public function index(Request $request, Playground $playground)
    {
        $dateStart = new DateTime($request->get('dateStart'));
        $dateEnd = new DateTime($request->get('dateEnd'));

        $games = $playground->games()
                            ->with(['users', 'busyRecord'])
                            ->whereHas('busyRecord', function ($q) use ($dateStart, $dateEnd) {
                                $q->whereBetween('busy_records.date', [$dateStart->format('Y-m-d'), $dateEnd->format('Y-m-d')]);
                            })->get();

        return GameResource::collection($games);
    }
}
