<?php

namespace App\Http\Controllers\Api\Front\Club;

use App\Models\Club;
use App\Models\Playground;
use App\Http\Resources\Front\PlaygroundResource;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class PlaygroundController extends Controller
{
    public function show(Request $request, Club $club, Playground $playground)
    {
        if (!$club->coordinates)
            abort(404);

        return new PlaygroundResource($playground);
    }
}
