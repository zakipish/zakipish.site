<?php

namespace App\Http\Controllers;

use \Exception;
use App\Models\Club;

use App\Services\GeoPointModifierService;
use Illuminate\Http\Request;

class GeoPointController extends Controller
{
    protected $modifier;

    public function __construct(GeoPointModifierService $modifier)
    {
        $this->modifier = $modifier;
    }

    public function show(Club $club)
    {
        try {
            if (!$club->logo) {
                throw new Exception();
            }

            $path = $this->modifier->modify($club);
            return response()->file($path);
        } catch(Exception $e) {
            return response()->file(public_path('geopoint-event-default.png'));
        }
    }
}
