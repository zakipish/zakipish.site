<?php

namespace App\Providers;

use App\Models\BusyRecord;
use App\Observers\BusyRecordObserver;

use App\Models\User;
use App\Models\Club;
use App\Models\ClubChain;
use App\Models\ContactType;
use App\Models\Equipment;
use App\Models\Event;
use App\Models\Game;
use App\Models\GameLevel;
use App\Models\GameStatus;
use App\Models\PlayerStatus;
use App\Models\Playground;
use App\Models\PlaygroundType;
use App\Models\Service;
use App\Models\Sport;
use App\Models\Surface;
use App\Models\Contact;
use App\Observers\EventObserver;
use App\Observers\ContactObserver;
use App\Observers\UserObserver;
use App\Observers\ClubObserver;
use App\Observers\GameObserver;
use App\Observers\PlaygroundObserver;
use App\Services\AmoCrmService;
use App\Services\ChatService;
use App\Services\UploadService;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\ServiceProvider;
use Jenssegers\Date\Date;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $conn = $this->app->make('Illuminate\Database\ConnectionInterface');
        $conn->getDoctrineSchemaManager()
            ->getDatabasePlatform()
            ->registerDoctrineTypeMapping('point', 'string');

        $conn->getDoctrineSchemaManager()
            ->getDatabasePlatform()
            ->registerDoctrineTypeMapping('timetzrange', 'string');

        $conn->getDoctrineSchemaManager()
            ->getDatabasePlatform()
            ->registerDoctrineTypeMapping('text', 'string');

        $conn->getDoctrineSchemaManager()
            ->getDatabasePlatform()
            ->registerDoctrineTypeMapping('_text', 'string');

        User::observe(UserObserver::class);
        Playground::observe(PlaygroundObserver::class);
        Club::observe(ClubObserver::class);
        Game::observe(GameObserver::class);
        BusyRecord::observe(BusyRecordObserver::class);
        Contact::observe(ContactObserver::class);
        Event::observe(EventObserver::class);

        Relation::morphMap([
            'events' => 'App\Models\Event',
        ]);

        Date::setLocale('ru');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('UploadService', function(){
            return new UploadService();
        });

        $this->app->singleton('AmoCrmService', function(){
            return new AmoCrmService();
        });

        $this->app->singleton('ChatService', function(){
            return new ChatService();
        });
    }
}
