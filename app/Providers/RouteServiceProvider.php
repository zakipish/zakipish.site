<?php

namespace App\Providers;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();

        Route::model('message', App\ChatMessage::class);

        // SMART SLUGS
        $smartSlugModels = ['club' => \App\Models\Club::class, 'playground' => \App\Models\Playground::class, 'event' => \App\Models\Event::class];
        foreach ($smartSlugModels as $name => $modelClass) {
            Route::bind($name, function ($value) use ($modelClass) {
                $morphClass = (new $modelClass)->getMorphClass();
                $slug = \App\Models\Slug::where([
                    'value' => $value,
                    'sluggable_type' => $morphClass,
                ])->first();

                if ($slug) {
                    return $slug->sluggable()->with(['activeSlugs'])->first() ?? abort(404);
                } else {
                    if (is_numeric($value)) {
                        return $modelClass::findOrFail($value);
                    } else {
                        return abort(404);
                    }
                }
            });
        }
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));
    }
}
