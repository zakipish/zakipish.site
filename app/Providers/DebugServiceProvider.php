<?php

namespace App\Providers;

use Illuminate\Database\Events\QueryExecuted;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\ServiceProvider;

class DebugServiceProvider extends ServiceProvider
{
    public function register()
    {
        if (env('APP_DEBUG'))
            Event::listen(
                QueryExecuted::class,
                function (QueryExecuted $query) {
                    Log::debug("[{$query->time}ms] {$query->sql} " . json_encode($query->bindings) . "\n");
                }
            );
    }
}
