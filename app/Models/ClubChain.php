<?php

namespace App\Models;

use App\Models\Utils\Sluggable;
use App\Models\Utils\SluggableInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ClubChain
 * @package App\Models
 * @property integer id
 * @property string slug
 * @property string name
 * @property User owner
 */
class ClubChain extends Model implements SluggableInterface
{
    use Sluggable;

    protected $fillable = [
        'slug',
        'name',
        'owner_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function owner() {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function logo() {
        return $this->belongsTo(Image::class);
    }
}
