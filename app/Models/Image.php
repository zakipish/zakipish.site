<?php

namespace App\Models;

use App\Services\UploadService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

/**
 * @property integer id
 * @property string code
 */
class Image extends Model
{
    protected $fillable = [
        'code',
        'original_name'
    ];

    public function getUrl($mode = null){
        if(empty($mode)){
            $uploadService = app(UploadService::class);

            return $uploadService->getOriginalUrl($this);
        }

        return route('resizer', ['mode' => $mode, 'code' => $this->code]);
    }
}
