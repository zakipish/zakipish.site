<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Relations\Pivot;

class UserSport extends Pivot
{
    public function gameLevel()
    {
        return $this->belongsTo(GameLevel::class);
    }
}
