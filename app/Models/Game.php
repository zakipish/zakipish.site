<?php

namespace App\Models;

use App\GameUser;
use App\Models\Exceptions\PlaygroundNotFoundException;
use App\Models\Utils\Chatable;
use App\Models\Utils\ChatableInterface;
use App\Models\Utils\BusyRecordable;
use App\Models\Utils\BusyRecordableInterface;
use App\Models\Utils\Sluggable;
use App\Models\Utils\SluggableInterface;
use App\Services\PriceService;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;
use \DateTime;

class Game extends Model implements SluggableInterface, BusyRecordableInterface, ChatableInterface
{
    use BusyRecordable;
    use Sluggable;
    use Chatable;

    protected $fillable = [
        'slug',
        'name',
        'description',
        'min_players',
        'max_players',
        'playground_id',
        'sport_id',
        'game_status_id',
        'game_level_id',
        'users',
        'price',
        'booking',
        'age_start',
        'age_end',
        'owner_id',
        'interval',
        'date',
    ];

    public function owner()
    {
        return $this->belongsTo(User::class, 'owner_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sport()
    {
        return $this->belongsTo(Sport::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function gameStatus()
    {
        return $this->belongsTo(GameStatus::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function gameLevel()
    {
        return $this->belongsTo(GameLevel::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(User::class, 'game_user')->withPivot('player_status_id');
    }

    /**
     * @return \DateTimeZone
     */

    protected $__users;

    /**
     * add, update, delete user from select debounce in game form
     * @param array $users
     * @return null
     */
    public function setUsersAttribute(array $users)
    {
        $newUsers = [];
        foreach ($users as $user) {
            $gameUser = $user['game_user'];
            $newUsers[$gameUser['user_id']] = ['player_status_id' => $gameUser['player_status_id']];
        }
        if ($this->id) {
            $this->users()->sync($newUsers);
        } else {
            $this->__users = $newUsers;
        }
    }

    public function getUsersToSave()
    {
        return $this->__users;
    }

    public function scopeBooking($query, $booking = true) {
        return $query->where('booking', $booking);
    }

    public function scopeHasntUsers($query, Array $userIds)
    {
        return $query->whereDoesntHave('users', function($u) use ($userIds) {
            return $u->whereIn('users.id', $userIds);
        });
    }

    public function scopeHasUsers($query, Array $userIds)
    {
        return $query->whereHas('users', function($u) use ($userIds) {
            return $u->whereIn('users.id', $userIds);
        });
    }

    public function scopeFilter($query, Array $params)
    {
        foreach ($params as $param => $value) {
            if ($value)
                $query->where($param, $value);
        }

        return $query;
    }

    public function getPlayersCountAttribute()
    {
        return $this->users->count();
    }

    public function getPlayerPriceAttribute()
    {
        return $this->price && $this->min_players ? ceil($this->price / $this->min_players) : $this->price;
    }

    public function getOldParamsAttribute()
    {
        return array_only($this->attributes, ['playground_id', 'interval', 'date']);
    }

    public function club()
    {
        return $this->playground->club();
    }

    public function scopeWhereDateBetween($query, Carbon $start, Carbon $end)
    {
        return $query->whereBetween('date', [$start->toDateString(), $end->toDateString()]);
    }

    public function scopeCrossingWith($query, $timetzrange)
    {
        return $query->where('interval', '&&', $timetzrange);
    }

    public function scopeWhereCityId($query, Int $city_id)
    {
        return $query->whereHas('playground', function($p) use ($city_id) {
            $p->whereHas('club', function($q) use ($city_id) {
                $q->where('clubs.city_id', $city_id);
            });
        });
    }

    public function scopeWithoutUserId($query, Int $user_id)
    {
        return $query->whereDoesntHave('users', function($u) use ($user_id) {
            $u->where('users.id', $user_id);
        });
    }

    protected $blockSchedule;

    public function getBlockScheduleAttribute()
    {
        if (isset($this->blockSchedule))
            return $this->blockSchedule;

        if ($this->exists)
            return !!$this->busyRecord;

        if (!$this->playground_id)
            return false;

        if ($this->playground->playground_schedules()->where('date', $this->date)->exists()) {
            $this->blockSchedule = true;
            return true;
        }
    }
}
