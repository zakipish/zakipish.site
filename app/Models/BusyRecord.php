<?php

namespace App\Models;

use \DateTime;
use Carbon\Carbon;

use App\Models\Playground;

use App\Services\PriceService;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class BusyRecord extends Model
{

    protected $fillable = [
        'date',
        'interval',
        'playground_id'
    ];

    public function playground()
    {
        return $this->belongsTo(Playground::class);
    }

    public function busyRecordable()
    {
        return $this->morphTo();
    }

    public function getIntervalAttribute()
    {
        $string = $this->attributes['interval'];
        if ($string) {
            preg_match('/([0-9]{2}:[0-9]{2})[0-9:\+]+,\s?([0-9]{2}:[0-9]{2})[0-9:\+]+/', $string, $matches);
            if (count($matches) === 3)
                return [
                    $matches[1],
                    $matches[2],
                ];
        }

        return $this->attributes['interval'];
    }

    public function setIntervalAttribute($value)
    {
        $timezone = $this->getTimezone();
        $times = array_map(function ($string) use ($timezone) {
            if (is_string($string)) {
                $string = Carbon::createFromFormat('H:i', $string, $timezone);
            }
            return $string->format('H:iO');
        }, $value);

        $this->attributes['interval'] = DB::raw("'[$times[0], $times[1])'");
    }

    public function getTimezone()
    {
        if (!$this->playground_id)
            throw new PlaygroundNotFoundException();

        return $this->playground->getTimezone();
    }

    public function getPostgresIntervalAttribute()
    {
        return $this->attributes['interval'];
    }

    public function getTimeStartAttribute()
    {
        return new DateTime($this->date . " " . $this->interval[0]);
    }

    public function getTimeEndAttribute()
    {
        return new DateTime($this->date . " " . $this->interval[1]);
    }
}
