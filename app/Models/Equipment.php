<?php

namespace App\Models;

use App\Models\Utils\Sluggable;
use App\Models\Utils\SluggableInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Equipment
 * @package App\Models
 * @property integer id
 * @property string slug
 * @property string name
 */
class Equipment extends Model implements SluggableInterface
{
    protected $table = 'equipments';

    use Sluggable;

    protected $fillable = [
        'slug',
        'name',
    ];
}
