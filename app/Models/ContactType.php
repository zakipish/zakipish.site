<?php

namespace App\Models;

use App\Models\Utils\Sluggable;
use App\Models\Utils\SluggableInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * @property integer id
 * @property string slug
 * @property string name
 */
class ContactType extends Model implements SluggableInterface
{
    use Sluggable;

    protected $fillable = [
        'slug',
        'name',
    ];
}
