<?php

namespace App\Models;

use App\Models\City;
use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $fillable = [
        'name','city_id'
    ];
    public $timestamps = false;
    public function city(){
        return $this->belongsTo(City::class);
    }
    public function clubs(){
        return $this->hasMany(Club::class);
    }
}
