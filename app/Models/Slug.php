<?php

namespace App\Models;

use App\Models\Utils\Sluggable;
use Illuminate\Database\Eloquent\Model;

/**
 * @property string value
 * @property boolean is_active
 */
class Slug extends Model
{

    protected $fillable = [
        'value',
        'is_active',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function sluggable()
    {
        return $this->morphTo();
    }
}
