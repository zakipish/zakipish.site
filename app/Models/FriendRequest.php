<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FriendRequest extends Model
{
    protected $fillable = [
        'requester_id',
        'responser_id',
        'accepted'
    ];

    public function requester()
    {
        return $this->belongsTo(User::class, 'requester_id');
    }

    public function responser()
    {
        return $this->belongsTo(User::class, 'responser_id');
    }

    public function accept()
    {
        return $this->update(['accepted' => true]);
    }

    public function decline()
    {
        return $this->delete();
    }

    public function scopeAccepted($query)
    {
        return $query->where(['accepted' => true]);
    }

    public function scopeAwaiting($query)
    {
        return $query->where(['accepted' => false]);
    }
}
