<?php

namespace App\Models;

use App\Models\Utils\Sluggable;
use App\Models\Utils\SluggableInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * @property integer id
 * @property string slug
 * @property string name
 * @property string description
 * @property string rules
 * @property integer min_players
 * @property integer max_players
 * @property string label
 * @property Equipment[] equipments
 */
class Sport extends Model implements SluggableInterface
{
    use Sluggable;

    protected $fillable = [
        'slug',
        'name',
        'description',
        'rules',
        'min_players',
        'max_players',
        'label',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function equipments()
    {
        return $this->belongsToMany(Equipment::class, 'sport_equipment');
    }

    public function icon()
    {
        return $this->belongsTo(Image::class);
    }

    public function getIconUrlAttribute()
    {
        return $this->icon ? $this->icon->getUrl() : null;
    }
    public function photo()
    {
        return $this->belongsTo(Image::class);
    }

    public function getPhotoUrlAttribute()
    {
        return $this->photo ? $this->photo->getUrl() : null;
    }
    public function children()
    {
        return Sport::query()->where("label","<@",$this.$this->label)->get();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function clubs()
    {
        return $this->belongsToMany(Club::class, 'club_sport');
    }
}
