<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer id
 * @property string value
 * @property ContactType contact_type
 * @property integer contact_type_id
 */
class Contact extends Model
{

    protected $fillable = [
        'value',
        'contact_type_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function contact_type() {
        return $this->belongsTo(ContactType::class);
    }
}
