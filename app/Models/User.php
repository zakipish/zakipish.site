<?php

namespace App\Models;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Log;

/**
 * @property integer id
 * @property string email
 * @property array moderated_clubs
 */
class User extends Authenticatable
{
    use Notifiable;

    const ROLE_USER = 0;
    const ROLE_ADMIN = 1;
    const ROLE_MODERATOR = 2;

    const GENDER_FEMALE = 0;
    const GENDER_MALE = 1;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'lastName',
        'midName',
        'birthDate',
        'about',
        'city_id',
        'email',
        'password',
        'phone',
        'moderated_clubs',
        'role',
        'weight',
        'height',
        'gender',
        'sports',
        'tmp_password',
        'tmp_password_valid_until',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'tmp_password',
    ];

    protected $dates = [
        'tmp_password_valid_until',
    ];

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function socialAccounts()
    {
        return $this->hasMany(SocialAccount::class);
    }

    public function moderatedClubs()
    {
        return $this->belongsToMany(Club::class)->using(ClubModerator::class);
    }

    public function moderatedPlaygrounds()
    {
        return $this->hasManyThrough(
            Playground::class,
            ClubModerator::class,
            'user_id',
            'club_id',
            'id',
            'club_id'
        );
    }

    public function moderatedEvents()
    {
        return $this->hasManyThrough(
            Event::class,
            ClubModerator::class,
            'user_id',
            'club_id',
            'id',
            'club_id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function avatars()
    {
        return $this->belongsToMany(Image::class, 'user_avatars')->withTimestamps();
    }

    public function avatarsDesc()
    {
        return $this->avatars()->orderBy('created_at', 'desc');
    }

    public function getAvatarAttribute()
    {
        return $this->avatarsDesc->first();
    }

    public function sports()
    {
        return $this->belongsToMany(Sport::class)
                    ->using(UserSport::class)
                    ->withPivot(['game_level_id', 'game_exp', 'games_count']);
    }

    public function getAvatarUrlAttribute()
    {
        if($this->avatar){
            return $this->avatar->getUrl('wl332');
        } else {
            return null;
        }
    }

    public function getAvatarBigThumbUrlAttribute()
    {
        if($this->avatar){
            return $this->avatar->getUrl('sq200');
        } else {
            return null;
        }
    }

    public function getAvatarThumbUrlAttribute()
    {
        if($this->avatar){
            return $this->avatar->getUrl('sq50');
        } else {
            return null;
        }
    }

    public function getAvatarHeaderUrlAttribute()
    {
        if($this->avatar){
            return $this->avatar->getUrl('sq24');
        } else {
            return null;
        }
    }

    public function sportsDesc()
    {
        return $this->sports()->orderByDesc('sport_user.created_at');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function clubChains()
    {
        return $this->hasMany(ClubChain::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function games()
    {
        return $this->belongsToMany(Game::class, 'game_user')->withPivot('player_status_id');
    }

    /**
     * @param array $clubs
     */
    public function setModeratedClubsAttribute(array $clubs)
    {

        $newClubs = [];
        foreach ($clubs as $club) {
            $newClubs[$club['id']] = ['user_id' => $this->id, 'club_id' => $club['id']];
        }
        //        sync all clubs
        $this->moderatedClubs()->sync($newClubs);
    }

    protected $__sports;

    public function setSportsAttribute(array $sports)
    {
        $this->__sports = array_reduce(
            array_map(
                function($sport)
                {
                    return [$sport['id'] => [ 'game_level_id' => array_get($sport, 'game_level_id'), 'game_exp' => array_get($sport, 'game_exp'), 'games_count' => array_get($sport, 'games_count') ] ];
                },
                $sports
            ),
            function($res, $sport) { return $res + $sport; },
            []
        );
    }

    public function getSportsToSave()
    {
        return $this->__sports;
    }

    public function hasRole(array $roles)
    {
        return in_array( $this->role, $roles );
    }

    public function getFullNameAttribute()
    {
        return $this->lastName ? $this->name . ' ' . $this->lastName : $this->name;
    }

    public function setPhoneAttribute($value)
    {
        $phone = preg_replace('/[^0-9\+.]/', '', $value);
        preg_match('/(7|\+7|8)?(\d{10})/', $phone, $matches);
        if ($matches) {
            $phone = $matches[2];
            $this->attributes['phone'] = $phone;
        }
    }

    // requests to me
    public function friendRequests()
    {
        return $this->hasMany(FriendRequest::class, 'responser_id');
    }

    // requests from me
    public function friendRequestsFromMe()
    {
        return $this->hasMany(FriendRequest::class, 'requester_id');
    }

    public function friends()
    {
        $friendRequests = $this->friendRequests()->accepted()->get()->pluck('requester');

        $friendRequestsFromMe = $this->friendRequestsFromMe()->accepted()->get()->pluck('responser');

        return $friendRequests->merge($friendRequestsFromMe);
    }

    public function hasFriend($friend_id)
    {
        return $this->friends()->contains('id', $friend_id);
    }

    public function events()
    {
        return $this->belongsToMany(Event::class)->using(EventUser::class)->withPivot(['type'])->withTimestamps();
    }

    protected $tmp = false;
    public function setTmpAttribute($value)
    {
        $this->tmp = $value;
    }

    public function getTmpAttribute($value)
    {
        return $this->tmp;
    }

    public function setEmailAttribute($value)
    {
        return $this->attributes['email'] = strtolower($value);
    }

    public function getEmailAttribute()
    {
        return strtolower($this->attributes['email']);
    }
}
