<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BookingType extends Model
{
    const STATUS_UNAVAILABLE    = 1;
    const STATUS_AVAILABLE      = 2;

    protected $fillable = [
        'type',
        'club_id',
        'status',
        'comment'
    ];

    public function club(){
        return $this->belongsTo(Club::class);
    }


}
