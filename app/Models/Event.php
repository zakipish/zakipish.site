<?php

namespace App\Models;

use Carbon\Carbon;
use App\Models\Utils\Chatable;
use App\Models\Utils\ChatableInterface;
use App\Models\Utils\BusyRecordable;
use App\Models\Utils\BusyRecordableInterface;
use App\Models\Utils\Sluggable;
use App\Models\Utils\SluggableInterface;
use Illuminate\Support\Facades\Log;

use Illuminate\Database\Eloquent\Model;

class Event extends Model implements SluggableInterface, BusyRecordableInterface, ChatableInterface
{
    use Sluggable;
    use BusyRecordable;
    use Chatable;

    protected $fillable = [
        'owner_id',
        'club_id',
        'playground_id',
        'sport_id',
        'chat_disabled',
        'title',
        'description',
        'price',
        'type',
        'min_players',
        'max_players',
        'game_level_id',
        'date',
        'interval',
        'coordinates',
        'blockSchedule',
        'age_start',
        'age_end',
        'images',
        'address',
        'users',
    ];

    const TYPE_MEETING = 1;
    const TYPE_GAME = 2;

    protected $_createBusyRecord, $_users, $_images;

    public function owner()
    {
        return $this->belongsTo(User::class, 'owner_id');
    }

    public function club()
    {
        return $this->belongsTo(Club::class);
    }

    public function playground()
    {
        return $this->belongsTo(Playground::class);
    }

    public function gameLevel()
    {
        return $this->belongsTo(GameLevel::class);
    }

    public function sport()
    {
        return $this->belongsTo(Sport::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class)->using(EventUser::class)->withPivot(['type'])->withTimestamps();
    }

    public function setUsersAttribute(Array $users)
    {
        $this->_users = array_reduce($users, function($acc, $user) {
            $acc[$user['id']] = [
                'type' => $user['type'],
            ];

            return $acc;
        }, []);
    }

    public function getUsersToSync()
    {
        return $this->_users;
    }

    public function images()
    {
        return $this->belongsToMany(Image::class, 'event_image')->withPivot(['order']);
    }

    public function setBlockScheduleAttribute($value)
    {
        $this->_createBusyRecord = $this->playground_id && $value;
    }

    public function getBlockScheduleAttribute()
    {
        if ($this->type == self::TYPE_GAME)
            return true;

        if (isset($this->_createBusyRecord)) {
            return $this->_createBusyRecord;
        } else {
            return $this->exists && $this->busyRecord;
        }
    }

    public function needToCreateChat()
    {
        return !$this->chat_disabled;
    }

    public function getCoordinatesAttribute()
    {
        $raw = array_get($this->attributes, 'coordinates');
        if (is_string($raw)) {
            $result = [];
            preg_match('/\(([\d,\.]*),([\d,\.]*)\)/', $raw, $result);
            if ($result) {
                return [$result[1], $result[2]];
            } else {
                return $raw;
            }
        }

        if (is_array($raw)) {
            return $raw;
        }
    }

    public function setCoordinatesAttribute($value)
    {
        if (is_string($value)) {
            $this->attributes['coordinates'] = $value;
        }

        if (is_array($value)) {
            $this->attributes['coordinates'] = "(" . $value[0] . "," . $value[1] . ")";
        }
    }

    public function setImagesAttribute($value)
    {
        $this->_images = $value;
    }

    public function getImagesForSave()
    {
        return $this->_images;
    }

    public function getPlaceTypeAttribute()
    {
        if ($this->type == Event::TYPE_GAME || $this->playground)
            return "Playground";

        return "Coordinates";
    }

    // SCOPES

    public function scopeWhereCityId($query, Int $city_id)
    {
        return $query->whereHas('club', function ($c) use ($city_id) {
            $c->where('city_id', $city_id);
        });
    }

    public function scopeWithoutUserId($query, Int $user_id)
    {
        return $query->whereDoesntHave('users', function ($u) use ($user_id) {
            $u->where('users.id', $user_id);
        });
    }

    public function scopeCrossingWith($query, $timetzrange)
    {
        return $query->where('interval', '&&', $timetzrange);
    }

    public function scopeWhereDate($query, Carbon $date)
    {
        return $query->where('date', $date->toDateString());
    }
}
