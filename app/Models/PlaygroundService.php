<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlaygroundService extends Model
{
    protected $table = 'playground_service';
    protected $fillable=[
        'price','unit', 'service_id','playground_id', 'payable'
    ];
    //
}
