<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SocialAccount extends Model
{
    const PROVIDER_MAIL      = 'mailru';
    const PROVIDER_OK        = 'odnoklassniki';
    const PROVIDER_FB        = 'facebook';
    const PROVIDER_VK        = 'vkontakte';
    const PROVIDER_INSTAGRAM = 'instagram';
    const PROVIDER_LINKED    = 5;
    const PROVIDER_GOOGLE    = 6;

    const AVAILABLE_PROVIDERS = [
        self::PROVIDER_VK,
        self::PROVIDER_FB,
        self::PROVIDER_MAIL,
        self::PROVIDER_OK,
    ];

    protected $fillable = [
        'provider',
        'provider_id',
        'user_id',
        'token',
        'mailru_profile_url',
        'instagram_nickname'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function profileUrl()
    {
        switch($this->provider)
        {
            case self::PROVIDER_MAIL:
                return $this->mailru_profile_url;
            case self::PROVIDER_OK:
                return "https://ok.ru/profile/" . $this->provider_id;
            case self::PROVIDER_VK:
                return "https://vk.com/id" . $this->provider_id;
            case self::PROVIDER_INSTAGRAM:
                return "https://www.instagram.com/" . $this->instagram_nickname;
            default:
                return '';
        }
    }
}
