<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    protected $fillable = [
        'chatable_id',
        'chatable_type'
    ];

    public function chatable()
    {
        return $this->morphTo();
    }

    public function messages()
    {
        return $this->hasMany(ChatMessage::class);
    }
}
