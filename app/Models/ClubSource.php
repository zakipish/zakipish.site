<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClubSource extends Model
{
    protected $fillable = [
        'remote_type',
        'remote_id',
        'club_id'
    ];


    public function club(){
        return $this->belongsTo(Club::class);
    }
}
