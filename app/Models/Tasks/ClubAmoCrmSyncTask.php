<?php

namespace App\Models\Tasks;

use App\Models\Club;
use Illuminate\Database\Eloquent\Model;

class ClubAmoCrmSyncTask extends Model
{
    const STATUS_READY      = 0;
    const STATUS_PROGRESS   = 1;
    const STATUS_DONE       = 2;
    const STATUS_ERROR      = 3;
    const STATUS_NON_NEEDED = 10;

    protected $fillable = [
        'club_id',
        'status'
    ];

    public function getQueueName(){
        return 'club_amo_sync_queue';
    }

    public function club(){
        return $this->belongsTo(Club::class);
    }
}
