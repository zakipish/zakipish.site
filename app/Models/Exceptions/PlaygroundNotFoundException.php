<?php

namespace App\Models\Exceptions;

use App\Models\Utils\SluggableInterface;
use RuntimeException;

class PlaygroundNotFoundException extends RuntimeException
{

}
