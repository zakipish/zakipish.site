<?php

namespace App\Models\Exceptions;

use App\Models\Utils\SluggableInterface;
use RuntimeException;

class ClubNotFoundException extends RuntimeException
{

}
