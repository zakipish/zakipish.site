<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Relations\Pivot;

class EventUser extends Pivot
{
    const TYPE_POSSIBLY = 1;
    const TYPE_DEFINITELY = 2;
    const TYPE_DECLINED = 3;

    public function event()
    {
        return $this->belongsTo(Event::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
