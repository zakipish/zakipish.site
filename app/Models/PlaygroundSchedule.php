<?php

namespace App\Models;

use App\Models\Exceptions\PlaygroundNotFoundException;
use App\Services\PriceService;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;
use \DateTime;
use \DateTimeZone;
use App\Services\IntervalService;

/**
 * @property integer id
 * @property integer playground_id
 * @property Playground playground
 * @property float price
 * @property mixed duration
 * @property mixed interval
 * @property mixed date
 */
class PlaygroundSchedule extends Model
{

    protected $fillable = [
        'playground_id',
        'interval',
        'price',
        'date',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function playground()
    {
        return $this->belongsTo(Playground::class);
    }

    /**
     * @return \DateTimeZone
     */
    public function getTimezone()
    {
        if (!$this->playground_id) {
            return new \DateTimeZone('Europe/Moscow');
            //throw new PlaygroundNotFoundException();
        }
        return $this->playground->getTimezone();
    }

    public function setIntervalAttribute($value)
    {
        if (!$value) {
            $this->attributes['interval'] = null;
            return;
        }
        $timezone = $this->getTimezone();
        $times = array_map(function ($string) use ($timezone) {
            if (is_string($string)) {
                $string = Carbon::createFromFormat('H:i', $string, $timezone);
            }
            return $string->format('H:iO');
        }, $value);

        $this->attributes['interval'] = DB::raw("'[$times[0], $times[1])'");
    }

    public function getIntervalAttribute()
    {
        $string = isset($this->attributes['interval']) ? $this->attributes['interval'] : false;
        $string = $this->attributes['interval'];
        if ($string) {
            preg_match('/([0-9]{2}:[0-9]{2})[0-9:\+]+,\s?([0-9]{2}:[0-9]{2})[0-9:\+]+/', $string, $matches);
            if (count($matches) === 3) {
                return [
                    $matches[1],
                    $matches[2],
                ];
            }
        }
        return null;
    }

    public function getPostgresIntervalAttribute()
    {
        return $this->attributes['interval'];
    }

    public function setPriceAttribute($value)
    {
        if ($this->playground->min_interval == null){
            throw ValidationException::withMessages([
                'price' => ['У зала № ' . $this->playground->id . ' "' . $this->playground->name . '" не указан временной интервал.']
            ]);
        }
        $this->attributes['price'] = $value;
    }

    public function getTimeStartAttribute()
    {
        return new DateTime($this->date . " " . $this->interval[0]);
    }

    public function getTimeEndAttribute()
    {
        return new DateTime($this->date . " " . $this->interval[1]);
    }

    public function getTimeIntervalAttribute()
    {
        return [$this->timeStart, $this->timeEnd];
    }

    public function scopeIntervalCrossing($query, $interval)
    {
        if (is_array($interval)) {
            $timezone = new DateTimeZone('Europe/Moscow');
            $interval = IntervalService::getTimetzrange($interval, $timezone);
        }

        return $query->where('interval', '&&', $interval);
    }
}
