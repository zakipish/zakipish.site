<?php

namespace App\Models;

use App\Models\Department;
use App\Models\Utils\Sluggable;
use App\Models\Utils\SluggableInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * @property integer id
 * @property string name
 * @property string slug
 */

class City extends Model implements SluggableInterface
{
    use Sluggable;

    protected $fillable = [
        'name',
        'slug'
    ];
    public function departments()
    {
        return $this->hasMany(Department::class);
    }
}
