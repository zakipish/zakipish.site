<?php

namespace App\Models\Utils;

use App\Exceptions\NewSlugFoundException;
use App\Models\Slug;
use App\Observers\SlaggableObserver;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\Eloquent\Relations\Relation;

trait Sluggable
{
    public static function bootSluggable()
    {
        static::observe(new SlaggableObserver);
        Relation::morphMap([
            (new self)->getTable() => self::class,
        ], true);
    }

    private $_newSlag;

    public function setSlugAttribute($slug)
    {
        $this->_newSlag = $slug;
    }

    public function getSlugAttribute()
    {
        $slug = $this->activeSlugs->first();
        return $slug ? $slug->value : null;
    }

    public function getSlugToSave()
    {
        return $this->_newSlag;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\morphMany
     */
    public function slugs()
    {
        /** @var Model $this */
        return $this->morphMany(Slug::class, 'sluggable');
    }

    public function activeSlugs() {
        return $this->slugs()->where('is_active', true);
    }

    /**
     *
     * @param Builder $query
     * @param string $slug
     * @return Builder
     */
    public function scopeOfSlug($query, $slug)
    {
        return $query->with('slugs')->where('slugs.value', '=', $slug);
    }

    /**
     * @param $slug
     * @return SluggableInterface
     */
    static function findBySlug($slug) {
        /** @var Builder|Model $builder */
        $builder = new self;

        /** @var SluggableInterface $model */
        $model = $builder->leftJoin('slugs', 'slugs.sluggable_id', '=', $builder->getTable() . '.id')
            ->where('slugs.value', $slug)
            ->select('slugs.value as slug_value', 'slugs.is_active as slug_is_active', $builder->getTable() . '.*')
            ->first();

        if (!$model) {
            throw (new ModelNotFoundException())->setModel(self::class, $slug);
        }

        if (!$model->slug_is_active) {
            $activeSlug = $model->slugs()->where('is_active', true)->first();
            if ($activeSlug) {
                throw (new NewSlugFoundException)->setModel($model, $activeSlug->value);
            } else {
                throw (new ModelNotFoundException())->setModel(self::class, $slug);
            }
        }

        return $model;
    }

}
