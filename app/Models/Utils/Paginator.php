<?php

namespace App\Models\Utils;

use App\Exceptions\NewSlugFoundException;
use App\Models\Slug;
use App\Observers\SlaggableObserver;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\Eloquent\Relations\Relation;

trait Paginator
{
    public function getAllOrPaginate($collection, $request){
        if($request->has('all') && $request->get('all') == 1){
            $collection = $collection->get();
        }else{
            $perPage = (int)$request->get('per_page');
            if ($perPage < 1 or $perPage > 1000) {
                $perPage = config('database.paginate');
            }

            $collection = $collection->paginate($perPage);
        }
        return $collection;
    }

}
