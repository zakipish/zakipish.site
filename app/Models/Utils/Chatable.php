<?php

namespace App\Models\Utils;

use \DateTime;

use App\Models\Chat;

use App\Observers\ChatableObserver;

use Illuminate\Database\Eloquent\Relations\Relation;

trait Chatable
{
    public static function bootChatable()
    {
        static::observe(new ChatableObserver);
        Relation::morphMap([
            (new self)->getTable() => self::class,
        ], true);
    }

    public function chat()
    {
        return $this->morphOne(Chat::class, 'chatable');
    }

    public function getNeedToCreateChatAttribute()
    {
        return true;
    }
}
