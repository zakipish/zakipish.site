<?php

namespace App\Models\Utils;

interface BusyRecordableInterface
{

    public function busyRecord();
}
