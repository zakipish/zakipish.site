<?php

namespace App\Models\Utils;

use \DateTime;
use Carbon\Carbon;

use App\Models\Playground;
use App\Models\BusyRecord;
use App\Models\Exceptions\PlaygroundNotFoundException;

use App\Services\IntervalService;

use App\Observers\BusyRecordableObserver;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\Eloquent\Relations\Relation;

trait BusyRecordable
{
    public static function bootBusyRecordable()
    {
        static::observe(new BusyRecordableObserver);
    }

    public function busyRecord()
    {
        return $this->morphOne(BusyRecord::class, 'busy_recordable');
    }

    public function playground()
    {
        return $this->belongsTo(Playground::class);
    }

    // ALIASES

    public function getTimezone()
    {
        if (!$this->busyRecord)
            return null;

        return $this->busyRecord->getTimezone();
    }

    public function getIntervalAttribute()
    {
        return $this->getArrayInterval($this->attributes['interval']);
    }

    public function setIntervalAttribute($value)
    {
        $this->attributes['interval'] = $this->getPostgresInterval($value);

        if ($this->blockSchedule && $this->busyRecord)
            $this->busyRecord->interval = $value;
    }

    public function setDateAttribute($value)
    {
        $this->attributes['date'] = $value;

        if ($this->blockSchedule && $this->busyRecord)
            $this->busyRecord->date = $value;
    }

    public function setBlockScheduleAttribute($value)
    {
        return true;
    }

    public function getBlockScheduleAttribute()
    {
        return true;
    }

    // ALIASES END

    public function getTimeStartAttribute()
    {
        return new DateTime($this->date . " " . $this->interval[0]);
    }

    public function getTimeEndAttribute()
    {
        return new DateTime($this->date . " " . $this->interval[1]);
    }

    // SCOPES

    public function scopeIntervalCrossing($query, $interval)
    {
        $timezone = new \DateTimeZone('Europe/Moscow');
        $timetzrange = IntervalService::getTimetzrange($interval, $timezone);

        return $query->where('interval', '&&', $timetzrange);
    }

    public function scopeIntervalIsIn($query, $interval)
    {
        $timezone = new \DateTimeZone('Europe/Moscow');
        $timetzrange = IntervalService::getTimetzrange($interval, $timezone);

        return $query->where('interval', '@>', $timetzrange);
    }

    public function scopeIntervalIncludes($query, $interval)
    {
        $timezone = new \DateTimeZone('Europe/Moscow');
        $timetzrange = IntervalService::getTimetzrange($interval, $timezone);

        return $query->where('interval', '<@', $timetzrange);
    }

    public function getPostgresIntervalAttribute()
    {
        return $this->getPostgresInterval($this->interval);
    }

    protected function getPostgresInterval($value)
    {
        $timezone = $this->getTimezone();
        $times = array_map(function ($string) use ($timezone) {
            if (is_string($string)) {
                $string = Carbon::createFromFormat('H:i', $string, $timezone);
            }
            return $string->format('H:iO');
        }, $value);

        return DB::raw("'[$times[0], $times[1])'");
    }

    protected function getArrayInterval($value)
    {
        preg_match('/([0-9]{2}:[0-9]{2})[0-9:\+]+,\s?([0-9]{2}:[0-9]{2})[0-9:\+]+/', $value, $matches);
        if (count($matches) === 3)
            return [
                $matches[1],
                $matches[2],
            ];
    }
}
