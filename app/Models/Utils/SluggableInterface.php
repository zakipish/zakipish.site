<?php
/**
 * Created by IntelliJ IDEA.
 * User: Exsmund
 * Date: 25.09.2018
 * Time: 17:05
 */

namespace App\Models\Utils;
use App\Models\Slug;
use Carbon\Carbon;

/**
 * Interface SluggableInterface
 * @package App\Models\Utils
 * @property boolean slug_is_active
 * @property string slug_value
 * @property string name
 * @property Carbon created_at
 * @property Slug[] slugs
 */
interface SluggableInterface
{

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function slugs();

    /**
     * @return string|null
     */
    public function getSlugToSave();

    public function getTable();

}
