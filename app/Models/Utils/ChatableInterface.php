<?php

namespace App\Models\Utils;

interface ChatableInterface
{
    public function chat();
}
