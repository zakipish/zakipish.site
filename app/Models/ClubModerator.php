<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Relations\Pivot;

class ClubModerator extends Pivot
{
    protected $table = 'club_user';
}
