<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class ChatMessage extends Model
{
    const TYPE_MESSAGE = 1;
    const TYPE_SERVICE = 2;

    protected $fillable = [
        'user_id',
        'chat_id',
        'message',
        'type'
    ];

    public function chat() {
        return $this->belongsTo(Chat::class);
    }

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function color() {
        $userSport = $this->user->sports()->where(['sports.id' => $this->chat->chatable->sport_id])->first();
        if ($userSport) {
            $gameLevel = $userSport->pivot->gameLevel;
            if ($gameLevel) {
                return $gameLevel->color;
            }
        }

        return null;
    }
}
