<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

//model for game form (processing - model GAME)
class GameUser extends Model
{
    protected $table = ['game_user'];
    protected $fillable=[
        'is_array','player_status_id'
    ];
    //
}
