<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    const TYPE_INCOME = 1;
    const TYPE_EVENT_PAYMENT = 2;

    protected $fillable = [
        'type',
        'club_id',
        'value'
    ];

    public function club()
    {
        return $this->belongsTo(Club::class);
    }
}
