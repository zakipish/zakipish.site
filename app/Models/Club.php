<?php

namespace App\Models;

use \DateTime;
use App\Models\Utils\Sluggable;
use App\Models\Utils\SluggableInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

/**
 * Class Club
 * @package App\Models
 * @property integer id
 * @property integer amo_id
 * @property string slug
 * @property string name
 * @property string description
 * @property User owner
 * @property mixed coordinates
 * @property Contact[] contacts
 * @property Service[] services
 * @property integer club_chain_id
 * @property string country
 * @property string region
 * @property City city
 * @property integer city_id
 * @property string department
 * @property integer rank
 * @property string source_link
 * @property string source
 * @property string type
 * @property mixed amo_updated_at
 * @property Image logo
 * @property Club parent_club
 * @property integer parent_club_id
 * @property BookingType[] booking_types
 */
class Club extends Model implements SluggableInterface
{
    use Sluggable;

    const NDS_0 = 0;
    const NDS_10 = 1;
    const NDS_18 = 2;
    const NDS_20 = 3;

    const RANK_0 = 0;
    const RANK_1 = 1;
    const RANK_2 = 2;
    const RANK_3 = 3;
    const RANK_4 = 4;

    const TYPE_ZAL = 1;
    const TYPE_SECTION = 2;
    const TYPE_ZAL_AND_SECTION = 3;

    protected $fillable = [
        'slug',
        'name',
        'description',
        'owner_id',
        'club_chain_id',
        'coordinates',
        'country',
        'region',
        //'city',
        'department',
        'department_id',
        'source_link',
        'rank',
        'contacts',
        'type',
        'city_id',
        'infrastructures',
        'infrastructure_ids',
        'amo_id',
        'amo_updated_at',
        'source',
        'sports',
        'parent_club_id',
        'address',
        'booking_types',
        'moderators',
        'entity',
        'BIK',
        'number_rs',
        'NDS',
        'company_name',
        'INN',
        'KPP',
        'OGRN',
        'entity_address',
        'registration_date',
        'registration_certificate',
        'entity_boss_name',
        'entity_boss_position',
        'company_account',
        'bank_name',
        'post_index',
        'post_street',
        'post_building',
        'post_building_number',
        'event_price'
    ];

    public function getBalanceAttribute()
    {
        return $this->transactions->sum('value');
    }

    public function transactions()
    {
        return $this->hasMany(Transaction::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }
    public function cityDepartment()
    {
        return $this->belongsTo(Department::class);
    }

    public function playgrounds()
    {
        return $this->hasMany(Playground::class);
    }

    public function events()
    {
        return $this->hasMany(Event::class);
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function owner()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function logo()
    {
        return $this->belongsTo(Image::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function images()
    {
        return $this->belongsToMany(Image::class, 'club_image')->withPivot(['main', 'order']);
    }

    public function getAvatarAttribute()
    {
        return $this->images->sortBy('pivot.order')->first();

    }
    public function getAvatarUrlAttribute()
    {
        if ($this->avatar) {
            return $this->avatar->getUrl('wl332');
        } else {
            return null;
        }
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function clubChain()
    {
        return $this->belongsTo(ClubChain::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function contacts()
    {
        return $this->belongsToMany(Contact::class, 'club_contact');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function services()
    {
        return $this->belongsToMany(Service::class, 'club_service');
    }

    public function infrastructures()
    {
        return $this->belongsToMany(Infrastructure::class, 'club_infrastructure');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function sports()
    {
        return $this->belongsToMany(Sport::class, 'club_sport');
    }

    public function parent_club()
    {
        return $this->belongsTo(Club::class);
    }

    public function bookingTypes()
    {
        return $this->hasMany(BookingType::class);
    }


    public function setContactsAttribute($value)
    {
        $byId = [];
        $new = [];
        foreach ($value as $contact) {
            if (!empty($contact['value'])) {
                if (isset($contact['id'])) {
                    $byId[(int)$contact['id']] = $contact;
                } else {
                    $new[] = $contact;
                }
            }
        }

        $actualContacts = [];
        foreach ($this->contacts as $contact) {
            if (isset($byId[$contact->id])) {
                $newContact = $byId[$contact->id];
                $contact->value = $newContact['value'];
                $contact->contact_type_id = $newContact['contact_type_id'];
                $contact->save();
                $actualContacts[] = $contact->id;
            } else {
//                $contact->delete();
            }
        }

//        $newContactModels = [];
        foreach ($new as $newContact) {
            $contact = new Contact($newContact);
            $contact->save();
//            $newContactModels[] = $contact;
            $actualContacts[] = $contact->id;
        }
        $this->contacts()->sync($actualContacts);
        $this->load('contacts');
    }

    protected $__infrastructures;


    public function setInfrastructuresAttribute($value)
    {
        if (is_array($value) && sizeof($value) > 0 && is_array($value[0])) {
            $infrastructure_ids = [];
            foreach($value as $infrastructure) {
                if ($infrastructure['id']) {
                    $infrastructure_ids[] = $infrastructure['id'];
                } else {
                    $newInfrastructure = Infrastructure::create($infrastructure);
                    $infrastructure_ids[] = $newInfrastructure['id'];
                }
            }
            if ($this->id) {
                $this->infrastructures()->sync($infrastructure_ids);
            } else {
                $this->__infrastructures = $infrastructure_ids;
            }
        } else {
            if ($this->id) {
                $this->infrastructures()->sync($value);
            } else {
                $this->__infrastructures = $value;
            }
        }
    }

    public function setInfrastructureIdsAttribute($value)
    {
        $this->infrastructures = $value;
    }

    public function getInfrastructuresToSave()
    {
        return $this->__infrastructures;
    }

    protected
        $__sports;

    public function setSportsAttribute($value)
    {
        Log::info('Club setSportsAttribute', [$value]);
        $labels = [];
        $sports = Sport::query()->whereIn('id', $value)->orderBy(DB::raw('nlevel(label)'))->get();
        $value = [];
        foreach ($sports as $sport) {
            $labels[] = $sport->label;
            $value[] = $sport->id;
            loop_end:
        }

        if ($this->id) {
            Log::info('Club setSportsAttribute', [$value]);
            $this->sports()->sync($value);
        } else {
            $this->__sports = $value;
        }
    }

    /**
     * @return mixed
     */
    public function getSportsToSave()
    {
        return $this->__sports;
    }

    /**
     * return playground's unique sports
     * @return array
     */
    public function getSportsList()
    {
        $sports = [];
//        from playground
        foreach ($this->playgrounds as $playground) {
            foreach ($playground->sports()->pluck('name') as $sport) {
                $sports[] = $sport;
            }
        }
//        from club
        foreach ($this->sports()->pluck('name') as $sport) {
            $sports[] = $sport;
        }
        return array_unique($sports);
    }

    /**
     * Возвращает таймзону для данного клуба.
     * Сейчас это Москва, в будущем надо брать у городов.
     * @return \DateTimeZone
     */
    public function getTimezone()
    {
        return new \DateTimeZone('Europe/Moscow');
    }

    /**
     * Booking Types Attribute
     * @param $value
     */
    public function setBookingTypesAttribute($value)
    {
        $byId = [];
        $new = [];
        foreach ($value as $type) {
            if (isset($type['id'])) {
                $byId[(int)$type['id']] = $type;
            } else {
                $new[] = $type;
            }
        }

        $actualTypes = [];
        foreach ($this->bookingTypes as $type) {
            if (isset($byId[$type->id])) {
                $newType = $byId[$type->id];
                $type->type = $newType['type'];
                $type->status = $newType['status'];
                $type->comment = $newType['comment'];
                $type->save();
                $actualTypes[] = $type->id;
            } else {
                $type->delete();
            }
        }
        foreach ($new as $newType) {
            $this->bookingTypes()->save(new BookingType($newType));
        }
        $this->load('bookingTypes');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function moderators()
    {
        return $this->belongsToMany(User::class)->using(ClubModerator::class);
    }

    /**
     * @param array $users
     */
    public function setModeratorsAttribute(array $users)
    {

        $newUsers = [];
        foreach ($users as $user) {
            $newUsers[$user['id']] = ['user_id' => $user['id'], 'club_id' => $this->id];
        }
        //        sync all clubs
        $this->moderators()->sync($newUsers);
    }

    public function getThisWeekScheduleIntervalAttribute()
    {
        $startDate = new DateTime('monday this week');
        $endDate = new DateTime('sunday this week');

        $playgrounds = $this->playgrounds()->with(['playground_schedules' => function ($q) use ($startDate, $endDate) {
            $q->whereBetween('date', [$startDate->format('Y-m-d'), $endDate->format('Y-m-d')]);
        }])->get();

        $schedules = $playgrounds->pluck('playground_schedules')->flatten();

        $start = $schedules->min('timeStart');
        $end = $schedules->max('timeEnd');

        $startStr = $start ? $start->format('H:i') : null;
        $endStr = $end ? $end->format('H:i') : null;

        $result = [];
        if ($startStr && $endStr) {
            $result = [$startStr, $endStr];
        }

        return $result;
    }

    public function getThisWeekScheduleWeekdaysIntervalAttribute()
    {
        $startDate = new DateTime('monday this week');
        $endDate = new DateTime('friday this week');

        $playgrounds = $this->playgrounds()->with(['playground_schedules' => function ($q) use ($startDate, $endDate) {
            $q->whereBetween('date', [$startDate->format('Y-m-d'), $endDate->format('Y-m-d')]);
        }])->get();

        $schedules = $playgrounds->pluck('playground_schedules')->flatten();

        $start = $schedules->min('timeStart');
        $end = $schedules->max('timeEnd');

        $startStr = $start ? $start->format('H:i') : null;
        $endStr = $end ? $end->format('H:i') : null;

        return [$startStr, $endStr];
    }

    public function getThisWeekScheduleWeekendIntervalAttribute()
    {
        $startDate = new DateTime('saturday this week');
        $endDate = new DateTime('sunday this week');

        $playgrounds = $this->playgrounds()->with(['playground_schedules' => function ($q) use ($startDate, $endDate) {
            $q->whereBetween('date', [$startDate->format('Y-m-d'), $endDate->format('Y-m-d')]);
        }])->get();

        $schedules = $playgrounds->pluck('playground_schedules')->flatten();

        $start = $schedules->min('timeStart');
        $end = $schedules->max('timeEnd');

        $startStr = $start ? $start->format('H:i') : null;
        $endStr = $end ? $end->format('H:i') : null;

        return [$startStr, $endStr];
    }

    public function getHasFeatureSchedulesAttribute()
    {
        return $this->playgrounds->reduce(function ($acc, $playground) {
            return $acc || $playground->hasFeatureSchedules;
        }, false);
    }

    public function setCoordinatesAttribute($value)
    {
        if (is_string($value)) {
            $this->attributes['coordinates'] = $value;
        }

        if (is_array($value)) {
            $this->attributes['coordinates'] = "(" . $value[0] . "," . $value[1] . ")";
        }
    }
}
