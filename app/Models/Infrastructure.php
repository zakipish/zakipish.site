<?php

namespace App\Models;

use App\Models\Utils\Sluggable;
use App\Models\Utils\SluggableInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * @property integer id
 * @property string slug
 */
class Infrastructure extends Model implements SluggableInterface
{
    use Sluggable;

    protected $fillable = [
        'name',
        'slug'
    ];

    public function icon()
    {
        return $this->belongsTo(Image::class);
    }

    public function iconUrl()
    {
        $icon =  $this->icon;
        if($icon){
            return $icon->getUrl();
        }
    }
}
