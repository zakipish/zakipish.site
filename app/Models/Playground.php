<?php

namespace App\Models;

use App\Models\Service;
use App\Models\BusyRecord;
use App\Models\Exceptions\ClubNotFoundException;
use App\Models\Utils\Sluggable;
use App\Models\Utils\SluggableInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use \DateTime;
use \DateTimeZone;
use Carbon\Carbon;
use App\Services\IntervalService;

/**
 * Class Playground
 * @package App\Models
 * @property integer id
 * @property string slug
 * @property string name
 * @property string description
 * @property integer type
 * @property integer min_interval
 * @property Club club
 * @property Equipment[] equipments
 * @property Service[] services
 * @property Sport[] sports
 * @property Surface[] surfaces
 * @property integer club_id
 * @property PlaygroundSchedule playground_schedules
 */
class Playground extends Model implements SluggableInterface
{
    use Sluggable;
    use SoftDeletes;

    const TYPE_OPEN = 1;
    const TYPE_CLOSE = 2;

    protected $fillable = [
        'slug',
        'name',
        'club_id',
        'services',
        'equipments',
        'surfaces',
        'surface_ids',
        'sports',
        'sport_ids',
        'type',
        'description',
        'min_interval',
        'isMinIntervalSetted',
    ];
    protected $dates = ['deleted_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function club()
    {
        return $this->belongsTo(Club::class);
    }

    public function busyRecords()
    {
        return $this->hasMany(BusyRecord::class);
    }

    public function games()
    {
        return $this->belongsToMany(Game::class, 'busy_records', 'playground_id', 'busy_recordable_id')->as('busyRecord')->wherePivot('busy_recordable_type', '=', 'games');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function equipments()
    {
        return $this->belongsToMany(Equipment::class, 'playground_equipment')->withPivot(['price', 'unit', 'id', 'payable']);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function services()
    {
        return $this->belongsToMany(Service::class, 'playground_service')->withPivot(['price', 'unit', 'id', 'payable']);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function sports()
    {
        return $this->belongsToMany(Sport::class, 'playground_sport');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function surfaces()
    {
        return $this->belongsToMany(Surface::class, 'playground_surface');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function images()
    {
        return $this->belongsToMany(Image::class, 'playground_image')->withPivot(['main', 'order']);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function playground_schedules()
    {
        return $this->hasMany(PlaygroundSchedule::class);
    }


    protected $__surfaces;

    public function setSurfacesAttribute($value)
    {
        $this->__surfaces = $value;
    }

    public function setSurfaceIdsAttribute($value)
    {
        $this->surfaces = $value;
    }

    public function getSurfacesToSave()
    {
        return $this->__surfaces;
    }

    protected $__services;

    public function setServicesAttribute($value)
    {
        $this->__services = $value;
    }

    public function getServicesToSave()
    {
        return $this->__services;
    }

    protected $__equipments;

    public function setEquipmentsAttribute($value)
    {
        $this->__equipments = $value;
    }

    public function getEquipmentsToSave()
    {
        return $this->__equipments;
    }


    protected $__sports;

    public function setSportsAttribute($value)
    {
        $this->__sports = $value;
    }

    public function setSportIdsAttribute($value)
    {
        $this->sports = $value;
    }

    public function getSportsToSave()
    {
        return $this->__sports;
    }

    public function getTimezone()
    {
        if (!$this->club_id) {
            throw new ClubNotFoundException();
        }
        return $this->club->getTimezone();
    }

    public function getAvatarAttribute()
    {
        $avatar = $this->images->sortBy('pivot.order')->first();

        if (!$avatar)
            $avatar = $this->club->avatar;

        return $avatar;
    }

    public function getAvatarUrlAttribute()
    {
        if ($this->avatar) {
            return $this->avatar->getUrl('wl332');
        } else {
            return null;
        }
    }

    public function getAvatarMidUrlAttribute()
    {
        if ($this->avatar) {
            return $this->avatar->getUrl('wl241');
        } else {
            return null;
        }
    }

    public function compiledSchedule(String $date, Array $interval = ["00:00:00", "23:59:59"])
    {
        $timezone = $this->getTimezone();
        $timetzrange = IntervalService::getTimetzrange($interval, $timezone);
        $schedules = $this->playground_schedules()
            ->where('date', '=', $date)
            ->where('interval', '&&', $timetzrange)
            ->select('id', 'playground_id', DB::raw("interval * $timetzrange as crossing"), 'price')
            ->get();

        $result = [];
        foreach ($schedules as $schedule) {
            $crossing = IntervalService::getIntervalFromTimetzrange($schedule->crossing);
            $begin = new DateTime($date . $crossing[0], $timezone);
            $end = new DateTime($date . $crossing[1], $timezone);

            $newResult = [];
            foreach ($result as $otherInterval) {
                if ($end <= $otherInterval[0]) {
                    $newResult[] = $otherInterval;
                    continue;
                }
                if ($begin >= $otherInterval[1]) {
                    $newResult[] = $otherInterval;
                    continue;
                }
                if ($end >= $otherInterval[1]) {
                    if ($begin <= $otherInterval[0]) {
                        continue;
                    } else {
                        $newResult[] = [$otherInterval[0], $begin, $otherInterval[2]];
                    }
                } else {
                    if ($begin <= $otherInterval[0]) {
                        $newResult[] = [$end, $otherInterval[1], $otherInterval[2]];
                    } else {
                        $newResult[] = [$otherInterval[0], $begin, $otherInterval[2]];
                        $newResult[] = [$end, $otherInterval[1], $otherInterval[2]];
                    }
                }
            }

            $newResult[] = [$begin, $end, $schedule];
            $result = $newResult;
        }

        usort($result, function ($intervalA, $intervalB) {
            return $intervalA[0] > $intervalB[0];
        });

        return $result;
    }

    public function getThisWeekScheduleIntervalAttribute()
    {
        $startDate = new DateTime('monday this week');
        $endDate = new DateTime('sunday this week');

        $schedules = $this->playground_schedules()->whereBetween('date', [$startDate->format('Y-m-d'), $endDate->format('Y-m-d')])->get();

        $start = $schedules->min('timeStart');
        $end = $schedules->max('timeEnd');

        $startStr = $start ? $start->format('H:i') : null;
        $endStr = $end ? $end->format('H:i') : null;

        return [$startStr, $endStr];
    }

    public function getThisWeekScheduleWeekdaysIntervalAttribute()
    {
        $startDate = new DateTime('monday this week');
        $endDate = new DateTime('friday this week');

        $schedules = $this->playground_schedules()->whereBetween('date', [$startDate->format('Y-m-d'), $endDate->format('Y-m-d')])->get();

        $start = $schedules->min('timeStart');
        $end = $schedules->max('timeEnd');

        $startStr = $start ? $start->format('H:i') : null;
        $endStr = $end ? $end->format('H:i') : null;

        return [$startStr, $endStr];
    }

    public function getThisWeekScheduleWeekendIntervalAttribute()
    {
        $startDate = new DateTime('saturday this week');
        $endDate = new DateTime('sunday this week');

        $schedules = $this->playground_schedules()->whereBetween('date', [$startDate->format('Y-m-d'), $endDate->format('Y-m-d')])->get();

        $start = $schedules->min('timeStart');
        $end = $schedules->max('timeEnd');

        $startStr = $start ? $start->format('H:i') : null;
        $endStr = $end ? $end->format('H:i') : null;

        return [$startStr, $endStr];
    }

    public function getHasFeatureSchedulesAttribute()
    {
        $todayStr = (new DateTime())->format('Y-m-d');
        $nowtimeStr = (new DateTime())->format('H:i:s');

        return $this->playground_schedules()->whereRaw("\"playground_schedules\".\"date\" > ? or \"playground_schedules\".\"date\" = ? and lower(\"playground_schedules\".\"interval\") > ?", [$todayStr, $todayStr, $nowtimeStr])->count() > 0;
    }

    public function scopeWhereCityId($query, Int $city_id)
    {
        return $query->whereHas('club', function ($clubQuery) use ($city_id) {
            $clubQuery->where('clubs.city_id', $city_id);
        });
    }

    public function scopeWhereDepartmentId($query, Int $department_id)
    {
        return $query->whereHas('club', function ($clubQuery) use ($department_id) {
            $clubQuery->where('clubs.department_id', $department_id);
        });
    }

    public function scopeWhereNotNullCoordinates($query)
    {
        return $query->whereHas('club', function ($clubQuery) {
            $clubQuery->whereNotNull('coordinates');
        });
    }

    public function scopeWhereHasSurfaceId($query, Int $surface_id)
    {
        return $query->whereHas('surfaces', function($surfacesQuery) use ($surface_id){
            $surfacesQuery->where('surfaces.id', $surface_id);
        });
    }

    public function scopeSearchByName($query, String $searchString)
    {
        $searchString = mb_strtolower(trim($searchString));

        return $query->where(function($searchQuery) use ($searchString) {
            $searchQuery->whereRaw("lower(playgrounds.name) like '%{$searchString}%'")
                ->orWhereHas('club', function($clubQuery) use ($searchString){
                    $q->whereRaw("lower(clubs.name) like '%{$searchString}%'")
                    ->orWhereRaw("lower(clubs.address) like '%{$searchString}%'");
                });
        });
    }

    public function scopeWhereSportId($query, Int $sport_id)
    {
        return $query->whereHas('sports', function($sportsQuery) use ($sport_id){
            $sportsQuery->where('playground_sport.sport_id', $sport_id);
        });
    }

    public function scopeWhereHasSchedulesForInterval($query, Carbon $date, Array $interval)
    {
        $dateStr = $date->toDateString();
        $timezone = new DateTimeZone('Europe/Moscow');
        $timetzrange = IntervalService::getTimetzrange($interval, $timezone);

        return $query->whereRaw("
            EXTRACT('epoch' FROM (current_date + upper($timetzrange::timetzrange)) - (current_date + lower($timetzrange::timetzrange))) =
            (SELECT
                COALESCE(
                    EXTRACT('epoch' FROM
                        SUM((date + upper(interval * $timetzrange::timetzrange)) - (date + lower(interval * $timetzrange::timetzrange)))
                    )
                , 0)
            FROM playground_schedules WHERE
                playground_id = playgrounds.id AND
                date = '$dateStr' AND
                interval && $timetzrange::timetzrange)
        ");
    }

    public function scopeWhereDoesntHasBusyRecordsForInterval($query, Carbon $date, Array $interval)
    {
        $dateStr = $date->toDateString();
        $timezone = new DateTimeZone('Europe/Moscow');
        $timetzrange = IntervalService::getTimetzrange($interval, $timezone);

        return $query->whereDoesntHave('busyRecords', function($busyRecordsQuery) use ($dateStr, $timetzrange) {
            $busyRecordsQuery->where('date', $dateStr)
              ->where('interval', '&&', $timetzrange);
        });
    }

    public function scopeWhereHasFreeTime($query, Carbon $date)
    {
        $dateStr = $date->toDateString();
        $timezone = new DateTimeZone('Europe/Moscow');
        $timeStart = $date->copy()->setTime(0, 0, 0);
        if ($date->isCurrentDay()) $timeStart = new Carbon();

        $timeEnd = $date->copy()->setTime(23, 59, 59);
        $timetzrange = IntervalService::getTimetzrange([$timeStart, $timeEnd], $timezone);

        return $query->whereRaw("
            (SELECT
                COALESCE(
                    EXTRACT('epoch' FROM
                        SUM((date + upper(interval * $timetzrange::timetzrange)) - (date + lower(interval * $timetzrange::timetzrange)))
                    )
                , 0)
            FROM playground_schedules WHERE
                playground_id = playgrounds.id AND
                date = '$dateStr'
            ) -
            (SELECT
                COALESCE(
                    EXTRACT('epoch' FROM
                        SUM((date + upper(interval)) - (date + lower(interval)))
                    )
                , 0)
            FROM busy_records WHERE
                playground_id = playgrounds.id AND
                date = '$dateStr'
            ) > 0
        ");
    }

    public function scopeWhereDoesntHasSchedules($query, Carbon $date)
    {
        $query->whereDoesntHave('playground_schedules', function ($schedulesQuery) use ($date) {
            $schedulesQuery->whereDate('date', $date);
        });
    }
}
