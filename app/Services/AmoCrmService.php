<?php
/**
 * Created by PhpStorm.
 * User: sergeisporyshev
 * Date: 09.10.18
 * Time: 14:44
 */

namespace App\Services;


use App\Models\Club;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class AmoCrmService
{
    const CMD_AUTH      = 'auth';
    const CMD_ACCOUNT   = 'account';
    const CMD_COMPANIES = 'companies';

    private $user;
    private $hash;
    private $subdomain;
    private $cookieFile;

    public function __construct()
    {
        $this->user = config('amo.user');
        $this->hash = config('amo.hash');
        $this->subdomain = config('amo.subdomain');
        $this->cookieFile = config('amo.cookiefile');
    }

    public function auth(){
        $result = $this->call();
        return $result;
    }

    public function syncClub(Club $club)
    {
        try{
            $fld['tel'] = 348429; // Телефон
            $fld['email'] = 348431; // Email
            $fld['site'] = 348433; // Web site
            $fld['city'] = 349001; // Город
            $fld['city_enum'] = [ // cities where amoid is set
                2 => 681329, //  Москва
                3 => 681327, //  Санкт-Петербург
                4 => 1265337, // Новосибирск
                5 => 1265339, // Екатеринбург
                6 => 1265341, // Нижний Новгород
                7 => 1265343, // Казань
                8 => 1265345, // Челябинск
                9 => 1265347, // Самара
                10 => 1265349, // Омск
                11 => 1265351, // Ростов-на-Дону
                12 => 1265355, // Великий Новгород
                13 => 1265353, // Уфа
                14 => 1265389, // Саратов
                15 => 1265535, // Абакан
                16 => 1265429, // Алтайский край
                17 => 1265419, // Архангельск
                18 => 1265367, // Барнаул
                19 => 1265443, // Благовещенск
                20 => 1265549, // Братск
                21 => 1265421, // Владимир
                22 => 1265445, // Волгоград
                23 => 1265383, // Вологда
                24 => 1265363, // Воронеж
                26 => 1265527, // Заинск
                27 => 1265357, // Иркутск
                28 => 1265469, // Йошкар-Ола
                29 => 1265371, // Калининград
                30 => 1265397, // Калуга
                31 => 1265553, // Карачаево-Черкесская Республика
                32 => 1265415, // Кемерово
                33 => 1265561, // Астрахань
                34 => 1265387, // Киров
                35 => 1265407, // Краснодар
                36 => 1265551, // Краснодарский край
                37 => 1265393, // Красноярск
                38 => 1265525, // Курган
                39 => 1265501, // Курск
                40 => 1265359, // Липецк
                41 => 1265543, // Магадан
                42 => 1265395, // Майкоп
                43 => 1265373, // Набережные Челны
                44 => 1265369, // Нальчик
                46 => 1265417, // Нижнекамск
                47 => 1265547, // Новокузнецк
                48 => 1265361, // Новый Уренгой
                51 => 1265487, // Ноябрьск
                52 => 1265365, // Оренбург
                53 => 1265377, // Орёл
                54 => 1265405, // Пенза
                55 => 1265411, // Пермь
                56 => 1265441, // Псков
                57 => 1265447, // Республика Башкортостан
                58 => 1265431, // Республика Татарстан
                59 => 1265541, // Рязань
                61 => 1265423, // Салехард
                62 => 1265455, // Саранск
                63 => 1265529, // Севастополь
                64 => 1265375, // Симферополь
                65 => 1265379, // Смоленск
                66 => 1265401, // Ставрополь
                67 => 1265399, // Тамбов
                68 => 1265471, // Тверь
                69 => 1265385, // Томск
                70 => 1265381, // Тюмень
                71 => 1265533, // Тула
                72 => 1265531, // Удмуртская Республика
                73 => 1265427, // Ульяновск
                75 => 1265413, // Ухта
                76 => 1265539, // Хабаровск
                77 => 1265435, // Ханты-Мансийск
                78 => 1265545, // Ханты-Мансийский АО
                79 => 1265433, // Чебоксары
                81 => 1265391, // Чита
                82 => 1265409, // Элиста
                83 => 1265537, // Южно-Сахалинск
                84 => 1265425, // Якутск
                85 => 1265485, // Ямало-Ненецкий АО
                86 => 1265403, // Ярославль
                87 => 1265563, // Ижевск
                88 => 1265565, // Комсомольск-на-Амуре
                89 => 1265555, // Махачкала
                90 => 1265559, // Мурманск
                91 => 1265569, // Сахалин
                92 => 1265567, // Улан-Удэ
                93 => 1265557, // Энгельс
                'Unknown' => 1265717
            ];
            $fld['addr'] = 348437; // Адрес
            $fld['type'] = 349229; // Тип: Зал/Секция
            $fld['type_enum'] = [  // Возможные значения
                Club::TYPE_ZAL => 681709,
                Club::TYPE_SECTION => 681717,
                Club::TYPE_ZAL_AND_SECTION => 681719,
                'Unknown' => 1265725
            ];
            $c_fields = [
                ['id' => 363653, // ID
                    'values' => [
                        ['value' => $club->id]
                    ]
                ], ['id' => 636141, // Edit URL
                    'values' => [
                        ['value' => "http://admin.devclub.zakipish.ru/clubs/{$club->id}/edit"]
                    ]
                ], ['id' => $fld['city'],
                    'values' => [
                        ['value' => $fld['city_enum'][$club->city_id] ?? $fld['city_enum']['Unknown']]
                    ]
                ], ['id' => 637777, // src, источник базы
                    'values' => [
                        ['value' => $club->source] //
                    ]
                ], ['id' => $fld['type'], // Тип: Зал/Секция
                    'values' => [
                        ['value' => $fld['type_enum'][$club->type] ?? $fld['type_enum']['Unknown']]
                    ]
                ], ['id' => 636145, // Rank
                    'values' => [
                        ['value' => $club->rank]
                    ]
                ]
            ];

            foreach ($club->contacts as $ct) {
                switch ($ct->contact_type->id) {
                    case 1: // tel
                        $c_fields[] = ['id' => $fld['tel'], //
                            'values' => [
                                ['value' => $ct->value, 'enum' => "WORK"]
                            ]
                        ];
                        break;
                    case 2: // email
                        $c_fields[] = ['id' => $fld['email'], //
                            'values' => [
                                ['value' => $ct->value, 'enum' => "WORK"]
                            ]
                        ];
                        break;
                    case 3: // Адрес
                        $c_fields[] = ['id' => 348437, //
                            'values' => [
                                ['value' => $ct->value]
                            ]
                        ];
                        break;
                    case 4: // site
                        $c_fields[] = ['id' => $fld['site'], //
                            'values' => [
                                ['value' => $ct->value]
                            ]
                        ];
                        break;
                    case 5: // VK
                        $c_fields[] = ['id' => 349131, //
                            'values' => [
                                ['value' => $ct->value]
                            ]
                        ];
                        break;
                    case 6: // Facebook
                        $c_fields[] = ['id' => 349127, //
                            'values' => [
                                ['value' => $ct->value]
                            ]
                        ];
                        break;
                    default: // 7 | Прочее
                }
            }
            $body = [];
            if (empty($club->amo_id)) {
                $body['add'] = [[
                    'name' => $club->name,
                    'custom_fields' => $c_fields
                ]];
            } else {
                $body['update'] = [[
                    'id' => $club->amo_id,
                    'updated_at' => Carbon::now()->timestamp,
                    'custom_fields' => $c_fields
                ]];
            }


        }catch (\Exception $e){
            Log::info($e->getMessage());
            return false;
        }

        try{
            $response = $this->call(self::CMD_COMPANIES, [], $body);
        }catch (\Exception $e){
            Log::error($e->getMessage());
            return false;
        }
        if(!isset($response['out']->_embedded->items[0]->id)){
            Log::error('Error on sync');
            Log::info(serialize($response));
            return false;
        }else{
            $club->update([
                'amo_id'            => $response['out']->_embedded->items[0]->id,
                'amo_updated_at'    => Carbon::now()
            ]);
            return true;
        }
    }




    private function call($cmd = self::CMD_AUTH, $getA = [], $postA = []){
        $link = $this->getBaseEndpoint();
        switch ($cmd){
            case self::CMD_AUTH:
                $link = $this->getAuthEndpoint();
                $postA = [
                    'USER_LOGIN'    => $this->user,
                    'USER_HASH'     => $this->hash
                ];
                break;
            case self::CMD_ACCOUNT:
            case self::CMD_COMPANIES:
                $link = $this->getEndpoint($cmd);
                break;
        }
        $curl = curl_init();
        if(isset($getA['since'])){
            curl_setopt($curl, CURLOPT_HTTPHEADER, ["IF-MODIFIED-SINCE: {$getA['since']}"]);
            unset($getA['since']);
        }

        $link .= '?';
        foreach ($getA as $k => $v)
            $link .= "${k}=${v}&";

        $link = substr($link, 0, -1);
        $r = [];

        curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0' );
        curl_setopt( $curl, CURLOPT_URL, $link );
        if( count($postA) ) {
            curl_setopt( $curl, CURLOPT_CUSTOMREQUEST, 'POST' );
            curl_setopt( $curl, CURLOPT_POSTFIELDS, json_encode($postA) );
            curl_setopt( $curl, CURLOPT_HTTPHEADER, ['Content-Type: application/json'] );
        }
        curl_setopt( $curl, CURLOPT_HEADER, false );
        curl_setopt( $curl, CURLOPT_COOKIEFILE, $this->cookieFile);
        curl_setopt( $curl, CURLOPT_COOKIEJAR, $this->cookieFile );
        curl_setopt( $curl, CURLOPT_SSL_VERIFYPEER, 0 );
        curl_setopt( $curl, CURLOPT_SSL_VERIFYHOST, 0 );

        $out = curl_exec($curl);
        $r['code'] = (int) curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        $r['out'] = json_decode($out);
        return $r;
    }

    private function getEndpoint($method = ''){
        return $this->getBaseEndpoint()  . $method;
    }

    private function getBaseEndpoint(){
        return "https://".$this->subdomain.".amocrm.ru/api/v2/";
    }

    private function getAuthEndpoint(){
        return "https://".$this->subdomain.".amocrm.ru/private/api/auth.php?type=json";
    }
}