<?php
/**
 * Created by PhpStorm.
 * User: sergeisporyshev
 * Date: 04.10.18
 * Time: 18:52
 */

namespace App\Services;


use App\Models\Image;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image as IntImage;
use Spatie\ImageOptimizer\OptimizerChainFactory;

class ResizeService
{
    const METHOD_RESIZE_RATIO      = 'resize-ratio';
    const METHOD_CROP              = 'crop';
    const METHOD_CROP_RESIZE       = 'crop-resize';
    const METHOD_RESIZE_CROP_RATIO = 'resize-crop-ratio';
    const METHOD_RATIO             = 'ratio';
    const METHOD_SAFE_RATIO        = 'safe-ratio';

    const RULE_SQ_400              = 'sq400';
    const RULE_WALL_400            = 'wl400';
    const RULE_WALL_649            = 'wl649';
    const RULE_WALL_295            = 'wl295';
    const RULE_WALL_282            = 'wl282';
    const RULE_SQ_24               = 'sq24';
    const RULE_WALL_332            = 'wl332';
    const RULE_WALL_241            = 'wl241';
    const RULE_WALL_1250            = 'wl1250';
    const RULE_WALL_675            = 'wl675';
    const RULE_WALL_700            = 'wl700';
    const RULE_SQ_50               = 'sq50';
    const RULE_SQ_40               = 'sq40';
    const RULE_SQ_80               = 'sq80';
    const RULE_SQ_100               = 'sq100';
    const RULE_SQ_120               = 'sq120';
    const RULE_SQ_160               = 'sq160';
    const RULE_SQ_200               = 'sq200';

    const AVAILABLE_RULES = [
        self::RULE_SQ_400,
        self::RULE_WALL_649,
        self::RULE_WALL_295,
        self::RULE_WALL_282,
        self::RULE_WALL_400,
        self::RULE_SQ_24,
        self::RULE_WALL_332,
        self::RULE_WALL_241,
        self::RULE_WALL_1250,
        self::RULE_WALL_675,
        self::RULE_WALL_700,
        self::RULE_SQ_50,
        self::RULE_SQ_40,
        self::RULE_SQ_80,
        self::RULE_SQ_100,
        self::RULE_SQ_120,
        self::RULE_SQ_160,
        self::RULE_SQ_200,
    ];

    const CONFIGS = [
        self::RULE_SQ_50 => [
            'type'      => self::METHOD_CROP_RESIZE,
            'width'     => 50,
            'height'    => 50
        ],
        self::RULE_SQ_40 => [
            'type'      => self::METHOD_CROP_RESIZE,
            'width'     => 40,
            'height'    => 40
        ],
        self::RULE_SQ_80 => [
            'type'      => self::METHOD_CROP_RESIZE,
            'width'     => 80,
            'height'    => 80
        ],
        self::RULE_SQ_100 => [
            'type'      => self::METHOD_CROP_RESIZE,
            'width'     => 100,
            'height'    => 100
        ],
        self::RULE_SQ_120 => [
            'type'      => self::METHOD_CROP_RESIZE,
            'width'     => 120,
            'height'    => 120
        ],
        self::RULE_SQ_160 => [
            'type'      => self::METHOD_CROP_RESIZE,
            'width'     => 160,
            'height'    => 160
        ],
        self::RULE_SQ_200 => [
            'type'      => self::METHOD_CROP_RESIZE,
            'width'     => 200,
            'height'    => 200
        ],
        self::RULE_SQ_24 => [
            'type'      => self::METHOD_CROP_RESIZE,
            'width'     => 24,
            'height'    => 24
        ],
        self::RULE_WALL_332 => [
            'type'      => self::METHOD_RESIZE_CROP_RATIO,
            'width'     => 332,
            'height'    => 396
        ],
        self::RULE_WALL_1250 => [
            'type'      => self::METHOD_RESIZE_CROP_RATIO,
            'width'     => 1250,
            'height'    => 340
        ],
        self::RULE_WALL_675 => [
            'type'      => self::METHOD_RESIZE_CROP_RATIO,
            'width'     => 675,
            'height'    => 506
        ],
        self::RULE_WALL_700 => [
            'type'      => self::METHOD_RESIZE_CROP_RATIO,
            'width'     => 700,
            'height'    => 305
        ],
        self::RULE_WALL_241 => [
            'type'      => self::METHOD_RESIZE_CROP_RATIO,
            'width'     => 241,
            'height'    => 112
        ],
        self::RULE_SQ_400 => [
            'type'      => self::METHOD_CROP_RESIZE,
            'width'     => 400,
            'height'    => 400
        ],
        self::RULE_WALL_400 => [
            'type'      => self::METHOD_RESIZE_RATIO,
            'width'     => 400,
            'height'    => 225
        ],
        self::RULE_WALL_649 => [
            'type'      => self::METHOD_RESIZE_RATIO,
            'width'     => 649,
            'height'    => 365
        ],
        self::RULE_WALL_295 => [
            'type'      => self::METHOD_RESIZE_RATIO,
            'width'     => 295,
            'height'    => 256
        ],
        self::RULE_WALL_282 => [
            'type'      => self::METHOD_RESIZE_RATIO,
            'width'     => 282,
            'height'    => 282
        ]
    ];

    public function process(Image $image, $mode)
    {
        if(!in_array($mode, self::AVAILABLE_RULES) || !array_key_exists($mode, self::CONFIGS))
            return abort(404);

        $config = $this->getConfig($mode);
        $this->generateFolder($mode);
        $file = Storage::disk('public')->get('source/'. $image->code);
        $workImage = IntImage::make($file);

        switch ($config['type']){
            case self::METHOD_SAFE_RATIO:
                $img = $this->safeRatio($workImage, $config);
                break;
            case self::METHOD_CROP_RESIZE:
                $img = $this->cropResize($workImage, $config);
                break;
            case self::METHOD_RESIZE_CROP_RATIO:
                $img = $this->resizeCropRatio($workImage, $config);
                break;

            case self::METHOD_CROP:
                $img = $this->crop($workImage, $config);
                break;

            case self::METHOD_RESIZE_RATIO:
                $img = $this->resizeRatio($workImage, $config);
                break;

            case self::METHOD_RATIO:
                $img = $this->ratio($workImage, $config);
                break;

            default:
                return abort(404);
        }

        $path = public_path('resizer/' . $mode . '/' . $image->code);
        $img->save($path);
        $this->optimize($path);
        return response()->file($path);
    }

    private function resizeCropRatio($workImage, $config) {
        $width = $config['width'] * 2;
        $height = $config['height'] * 2;

        if($workImage->height() >= $workImage->width()){
            $workImage->resize($width, null, function($constraint){
                $constraint->aspectRatio();
            });
        }else{
            $workImage->resize(null, $height, function($constraint){
                $constraint->aspectRatio();
            });
        }

        $workImage->crop($width, $height);
        return $workImage;
    }

    private function cropResize($workImage, $config){
        $width = $config['width'] * 2;
        $height = $config['height'] * 2;


        if($workImage->height() > $workImage->width()){
            $baseCroper = $workImage->width();
        }else{
            $baseCroper = $workImage->height();
        }

        $workImage->crop($baseCroper, $baseCroper);
        $workImage->resize($width, $height);
        return $workImage;
    }

    private function safeRatio($workImage, $config)
    {
        $ratio = $config['width'] / $config['height'];
        $width = $workImage->width();
        $height = $workImage->height();
        if($width / $height >= $ratio){
            $workImage->resizeCanvas($width, round($width / $ratio));
        }else{
            $workImage->resizeCanvas(round($height * $ratio), $height);
        }
        return $workImage;
    }

    private function resizeRatio($workImage, $config)
    {
        $width = $config['width'] * 2;
        $height = $config['height'] * 2;

        if($workImage->height() >= $workImage->width()){
            $workImage->resize(null, $height, function($constraint){
                $constraint->aspectRatio();
            });
        }else{
            $workImage->resize($width, null, function($constraint){
                $constraint->aspectRatio();
            });
        }
        return $workImage;
    }

    private function ratio($workImage, $config)
    {
        $ratio = $config['width'] / $config['height'];

        if($workImage->width() / $workImage->height() >= $ratio){
            $workImage->crop(round($workImage->height() * $ratio), $workImage->height());
        }else{
            $workImage->crop($workImage->width(), round($workImage->width() / $ratio));
        }

        return $workImage;
    }

    private function crop($workImage, $config)
    {
        $width = $config['width'];
        $height = $config['height'];
        $workImage->crop($width, $height);
        return $workImage;
    }


    private function getConfig($rule)
    {
        return self::CONFIGS[$rule];
    }

    private function generateFolder($mode)
    {
        if(!is_dir(public_path('resizer/' . $mode)))
            mkdir(public_path('resizer/' . $mode), 0777, true);
    }

    private function optimize($path)
    {
        $optimizer = OptimizerChainFactory::create();
        $optimizer->optimize($path);
    }
}
