<?php

namespace App\Services;

use App\Models\Club;
use App\Models\Image;
use Intervention\Image\Facades\Image as IntImageFacade;
use Intervention\Image\Image as IntImage;
use Spatie\ImageOptimizer\OptimizerChainFactory;

use Illuminate\Support\Facades\Storage;

class GeoPointModifierService
{

    const LOGO_WIDTH = 26;
    const LOGO_HEIGHT = 26;
    const LOGO_SIZE_MULTIPLIER = 2;
    const LOGO_OFFSET_TYPE = 'top-left';
    const LOGO_OFFSET_X = 28;
    const LOGO_OFFSET_Y = 26;

    const TEMPLATE_PATH = 'geopoint-event-template.png';
    const DESTINATION_PATH = 'geopoint/club';
    const DESTINATION_RULES = 0777;

    public function modify(Club $club)
    {
        $geopointFilename = $club->id . '.png';
        $img = $club->logo;

        return $this->process($img, $geopointFilename);
    }

    private function process(Image $image, String $destinationFilename)
    {
        $logoFile = Storage::disk('public')->get('source/'. $image->code);
        $templateFile = public_path(GeoPointModifierService::TEMPLATE_PATH);

        $workLogoFile = IntImageFacade::make($logoFile);
        $workTemplateFile = IntImageFacade::make($templateFile);

        $result = $this->overlay($workLogoFile, $workTemplateFile);
        $destinationPath = $this->generateFolder();

        $path = $destinationPath . '/' . $destinationFilename;

        $result->save($path);
        $this->optimize($path);

        return $path;
    }

    private function overlay(IntImage $logoFile, IntImage $templateFile)
    {
        $multiplier = GeoPointModifierService::LOGO_SIZE_MULTIPLIER;
        $resizeWidth = GeoPointModifierService::LOGO_WIDTH * $multiplier;
        $resizeHeight = GeoPointModifierService::LOGO_HEIGHT * $multiplier;

        $logoFile = $logoFile->resize($resizeWidth, $resizeHeight);

        $offsetType = GeoPointModifierService::LOGO_OFFSET_TYPE;
        $offsetX = GeoPointModifierService::LOGO_OFFSET_X * $multiplier;
        $offsetY = GeoPointModifierService::LOGO_OFFSET_Y * $multiplier;

        $result = $templateFile->insert($logoFile, $offsetType, $offsetX, $offsetY);

        return $result;
    }

    private function generateFolder()
    {
        $path = public_path(GeoPointModifierService::DESTINATION_PATH);
        $rules = GeoPointModifierService::DESTINATION_RULES;

        if(!is_dir($path))
            mkdir($path, $rules, true);

        return $path;
    }

    private function optimize($path)
    {
        $optimizer = OptimizerChainFactory::create();
        $optimizer->optimize($path);
    }
}
