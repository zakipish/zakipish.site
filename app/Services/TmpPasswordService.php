<?php

namespace App\Services;

use Mail;
use Carbon\Carbon;

use App\Models\User;
use Illuminate\Support\Facades\Hash;

use Illuminate\Support\Facades\Log;

class TmpPasswordService
{
    protected $user, $tmp_password;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function create()
    {
        $tmp_password = $this->generatePassword();
        //Log::debug('$tmp_password = '. $tmp_password);
        $this->tmp_password = $tmp_password;
        $tmp_password_hash = Hash::make($tmp_password);
        $tmp_password_valid_until = $this->validUntilDate();

        $this->user->update([
            'tmp_password' => $tmp_password_hash,
            'tmp_password_valid_until' => $tmp_password_valid_until
        ]);

        return $this->user;
    }

    public function sendMail()
    {
        if (!$this->tmp_password || !$this->user->tmp_password_valid_until)
            throw new \Exception('Tmp password is empty or was created in another session');

        if (!$this->user->email)
            throw new \Exception("User's email is empty");

        $mailData = [
            'tmp_password' => $this->tmp_password,
            'tmp_password_valid_until' => $this->user->tmp_password_valid_until,
        ];

        $user = $this->user;

        Mail::send('emails.tmp_password', $mailData, function ($m) use ($user) {
            $m->from('no@zakipish.ru', 'ZaKipish');
            $m->to($user->email, $user->name)->subject('Временный пароль');
        });
    }

    public function reset()
    {
        $this->user->update([
            'tmp_password' => null,
            'tmp_password_valid_until' => null
        ]);

        return $this->user;
    }

    protected function generatePassword(Int $length = 6)
    {
        return bin2hex(random_bytes($length));
    }

    protected function validUntilDate(Int $days = 7)
    {
        $today = new Carbon();
        return $today->addDays($days);
    }
}
