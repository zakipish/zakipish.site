<?php
/**
 * Created by PhpStorm.
 * User: sergeisporyshev
 * Date: 09.10.18
 * Time: 14:44
 */

namespace App\Services;


use App\Models\Club;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class AmoCrmService
{
    const CMD_AUTH      = 'auth';
    const CMD_ACCOUNT   = 'account';
    const CMD_COMPANIES = 'companies';

    private $user;
    private $hash;
    private $subdomain;
    private $cookieFile;

    public function __construct()
    {
        $this->user = config('amo.user');
        $this->hash = config('amo.hash');
        $this->subdomain = config('amo.subdomain');
        $this->cookieFile = config('amo.cookiefile');
    }

    public function auth(){
        $result = $this->call();
        return $result;
    }

    public function syncClub(Club $club){
        $fld['tel'] = 348429; // Телефон
        $fld['email'] = 348431; // Email
        $fld['site'] = 348433; // Web site
        $fld['city'] = 349001; // Город
        $fld['addr'] = 348437; // Адрес
        $c_fields = [
            [	'id' => 363653, // ID
                'values' => [
                    [ 'value' => $club->id ]
                ]
            ],[ 'id' => 636141, // Edit URL
                'values' => [
                    [ 'value' => "http://admin.devclub.zakipish.ru/clubs/{$club->id}/edit" ]
                ]
            ],[ 'id' => $fld['city'],
                'values' => [
                    [ 'value' => 681327 ] // [681327] => Санкт-Петербург
                ]
            ],[ 'id' => 637777, // src, источник базы
                'values' => [
                    [ 'value' => $club->source ] //
                ]
            ],[ 'id' => 636145, // Rank
                'values' => [
                    [ 'value' => $club->rank ]
                ]
            ]
        ];

        switch ($club->type){
            case Club::TYPE_ZAL:
                $c_fields[] = [	'id' => 349229, //
                    'values' => [
                        [ 'value' => 681709 ]
                    ]
                ];
                break;

            case Club::TYPE_SECTION:
                $c_fields[] = [	'id' => 349229, //
					'values' => [
						[ 'value' => 681717 ]
					]
				];
				break;

            case Club::TYPE_ZAL_AND_SECTION:
                $c_fields[] = [	'id' => 349229, //
					'values' => [
						[ 'value' => 681719 ]
					]
				];
				break;
        }

        foreach ($club->contacts as $ct){
            switch ($ct->contact_type->id){
                case 1: // tel
                    $c_fields[] = [	'id' => $fld['tel'], //
                        'values' => [
                            [ 'value' => $ct->value, 'enum' => "WORK"  ]
                        ]
                    ];
                    break;
                case 2: // email
                    $c_fields[] = [	'id' => $fld['email'], //
                        'values' => [
                            [ 'value' => $ct->value, 'enum' => "WORK"  ]
                        ]
                    ];
                    break;
                case 3: // Адрес
                    $c_fields[] = [	'id' => 348437, //
                        'values' => [
                            [ 'value' => $ct->value ]
                        ]
                    ];
                    break;
                case 4: // site
                    $c_fields[] = [	'id' => $fld['site'], //
                        'values' => [
                            [ 'value' => $ct->value ]
                        ]
                    ];
                    break;
                case 5: // VK
                    $c_fields[] = [	'id' => 349131, //
                        'values' => [
                            [ 'value' => $ct->value  ]
                        ]
                    ];
                    break;
                case 6: // Facebook
                    $c_fields[] = [	'id' => 349127, //
                        'values' => [
                            [ 'value' => $ct->value  ]
                        ]
                    ];
                    break;
                default: // 7 | Прочее
            }
        }
        $body = [];
        if(empty($club->amo_id)){
            $body['add'] = [[
                'name'          => $club->name,
                'custom_fields' => $c_fields
            ]];
        }else{
            $body['update'] = [[
                'id'            => $club->amo_id,
                'updated_at'    => Carbon::now()->timestamp,
                'custom_fields' => $c_fields
            ]];
        }
        try{
            $response = $this->call(self::CMD_COMPANIES, [], $body);
        }catch (\Exception $e){
            Log::error($e->getMessage());
        }
        if(!isset($response['out']->_embedded->items[0]->id)){
            Log::error('Error on sync');
            Log::info(serialize($response));
            return false;
        }else{
            $club->update([
                'amo_id'            => $response['out']->_embedded->items[0]->id,
                'amo_updated_at'    => Carbon::now()
            ]);
            return true;
        }
    }




    private function call($cmd = self::CMD_AUTH, $getA = [], $postA = []){
        $link = $this->getBaseEndpoint();
        switch ($cmd){
            case self::CMD_AUTH:
                $link = $this->getAuthEndpoint();
                $postA = [
                    'USER_LOGIN'    => $this->user,
                    'USER_HASH'     => $this->hash
                ];
                break;
            case self::CMD_ACCOUNT:
            case self::CMD_COMPANIES:
                $link = $this->getEndpoint($cmd);
                break;
        }
        $curl = curl_init();
        if(isset($getA['since'])){
            curl_setopt($curl, CURLOPT_HTTPHEADER, ["IF-MODIFIED-SINCE: {$getA['since']}"]);
            unset($getA['since']);
        }

        $link .= '?';
        foreach ($getA as $k => $v)
            $link .= "${k}=${v}&";

        $link = substr($link, 0, -1);
        $r = [];

        curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0' );
        curl_setopt( $curl, CURLOPT_URL, $link );
        if( count($postA) ) {
            curl_setopt( $curl, CURLOPT_CUSTOMREQUEST, 'POST' );
            curl_setopt( $curl, CURLOPT_POSTFIELDS, json_encode($postA) );
            curl_setopt( $curl, CURLOPT_HTTPHEADER, ['Content-Type: application/json'] );
        }
        curl_setopt( $curl, CURLOPT_HEADER, false );
        curl_setopt( $curl, CURLOPT_COOKIEFILE, $this->cookieFile);
        curl_setopt( $curl, CURLOPT_COOKIEJAR, $this->cookieFile );
        curl_setopt( $curl, CURLOPT_SSL_VERIFYPEER, 0 );
        curl_setopt( $curl, CURLOPT_SSL_VERIFYHOST, 0 );

        $out = curl_exec($curl);
        $r['code'] = (int) curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        $r['out'] = json_decode($out);
        return $r;
    }

    private function getEndpoint($method = ''){
        return $this->getBaseEndpoint()  . $method;
    }

    private function getBaseEndpoint(){
        return "https://".$this->subdomain.".amocrm.ru/api/v2/";
    }

    private function getAuthEndpoint(){
        return "https://".$this->subdomain.".amocrm.ru/private/api/auth.php?type=json";
    }
}