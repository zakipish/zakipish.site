<?php

namespace App\Services;

use \DateTimeZone;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class IntervalService
{
    public static function getTimetzrange(Array $interval, DateTimeZone $timezone)
    {
        $times = array_map(function ($time) use ($timezone) {
            if (is_string($time)) {
                $time = Carbon::createfromformat('H:i', $time, $timezone);
            }
            return $time->format('H:iO');
        }, $interval);

        return DB::raw("'[$times[0], $times[1])'");
    }

    public static function getIntervalFromTimetzrange($string)
    {
        if ($string) {
            preg_match('/([0-9]{2}:[0-9]{2})[0-9:\+]+,\s?([0-9]{2}:[0-9]{2})[0-9:\+]+/', $string, $matches);
            if (count($matches) === 3) {
                return [
                    $matches[1],
                    $matches[2],
                ];
            }
        }
        return null;
    }
}
