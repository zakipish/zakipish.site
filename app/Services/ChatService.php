<?php
/**
 * Created by PhpStorm.
 * User: sergeisporyshev
 * Date: 12.12.18
 * Time: 21:39
 */

namespace App\Services;


use App\Http\Resources\Front\Chat\MessageResource;
use Illuminate\Support\Facades\Redis;

class ChatService
{
    public function push($message, $channelId){
        $redis = Redis::connection();
        $redis->publish($channelId, json_encode(new MessageResource($message)));
    }
}
