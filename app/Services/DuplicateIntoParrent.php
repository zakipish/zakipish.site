<?php
/**
 * Created by PhpStorm.
 * User: serufim
 * Date: 01.11.18
 * Time: 12:23
 */

namespace App\Services;

use App\Models\Club;
use Illuminate\Support\Facades\Log;

class DuplicateIntoParrent
{
    public function __construct(Club $club)
    {
        //Таак, теперь мы должны получить объект родителя
        $duplicate = $club->parent_club_id;
        $this->parent_club = Club::find($duplicate);
        $this->club = $club;
    }

    /**
     * Возвращет список элементов содержащихся в одном массиве и не содержащихся в другом
     * Проверка по ключу @key
     * @param $current_array
     * @param $parrent_array
     * @param string $key
     * @param array $check_unique_fields
     * @return array
     */
    public static function calculate_difference($current_array, $parrent_array, $key = 'id', array $check_unique_fields = [])
    {
        $out = [];
        foreach ($current_array as $current_item) {
            $img_key = $current_item[$key];
            $logic_key = true;
            foreach ($parrent_array as $parent_item) {
//                проверка айдишников
                if ($img_key == $parent_item[$key]) {
                    $logic_key = false;
                    break;
                }
//                дополнительная проверка уникальности полей между собой
//                  (parent[name] !== child[name] and parent[type] !== child[type] ...)
                if (!empty($check_unique_fields)) {
                    $logic_arr = [];
                    foreach ($check_unique_fields as $unique_field_key) {
                        if ($parent_item[$unique_field_key] !== $current_item[$unique_field_key]) {
                            $logic_arr[] = false;
                        } else {
                            $logic_arr[] = true;
                        }
                    }
                    if(in_array(false, $logic_arr)){
                        $logic_key = true;
                    }else{
                        $logic_key = false;
                        break;
                    }
                }
            }
            if ($logic_key) {
                $out[] = $current_item;
            }
        }
        return $out;
    }

    public function duplicate_images()
    {
        $this->resolve_one_to_many_relation($this->parent_club->images(), $this->club->images(), true);
        $this->parent_club->save();
    }

    public function resolve_one_to_many_relation($parent_method, $child_method, $has_order = false, array $check_unique_fields = [])
    {
        $parent_items = $parent_method->get();
        $child_items = $child_method->get();
        $calculated_items = DuplicateIntoParrent::calculate_difference($child_items, $parent_items, 'id', $check_unique_fields);
        $i = $parent_method->count();
        foreach ($calculated_items as $calculated_item) {
            if ($has_order)
                $parent_method->attach($calculated_item->id, ['order' => $i++]);
            else
                $parent_method->attach($calculated_item->id);
        }
    }

    public function dupicate()
    {

        //Родитель теперь у нас и мы должны значит узнать что там у него с остальными полями
        $this->resolve_one_to_many_relation($this->parent_club->images(), $this->club->images(), true);
        $this->resolve_one_to_many_relation($this->parent_club->contacts(), $this->club->contacts(), false, ['value', 'contact_type_id']);
        $this->resolve_one_to_many_relation($this->parent_club->infrastructures(), $this->club->infrastructures());
        $this->resolve_one_to_many_relation($this->parent_club->sports(), $this->club->sports());

        //Проверка полей
        $fields = ['coordinates', 'country', 'department', 'region', 'source_link', 'source'];
        foreach ($fields as $field) {
            if (!$this->parent_club->$field and $this->club->$field)
                $this->parent_club->$field = $this->club->$field;
        }
        $this->parent_club->save();
    }
}
