<?php
/**
 * Created by PhpStorm.
 * User: sergeisporyshev
 * Date: 02.10.18
 * Time: 21:22
 */

namespace App\Services;


use App\Models\Playground;
use App\Models\PlaygroundSchedule;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use Illuminate\Database\Query\Expression;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class PriceService
{

    public static function getPriceForSchedule(PlaygroundSchedule $schedule, CarbonInterval $interval)
    {
        return $schedule->price * $interval->totalMinutes / $schedule->playground->min_interval;
    }

    public static function getPriceForInterval(Playground $playground, $date, $interval)
    {
        $timezone = $playground->getTimezone();

        $begin = new Carbon($interval[0], $timezone);
        $end = new Carbon($interval[1], $timezone);

        $totalPrice = 0;

        $timetzrange = IntervalService::getTimetzrange($interval, $timezone);
        $schedules = $playground->playground_schedules()
            ->where('interval', '&&', $timetzrange)
            ->where('date', '=', $date)
            ->orderBy('created_at')
            ->select('id', 'playground_id', DB::raw("interval * $timetzrange as interval_merge"), 'price')
            ->get();

        $result = [];
        foreach ($schedules as $schedule) {
            $crossing = IntervalService::getIntervalFromTimetzrange($schedule->interval_merge);
            $begin = new Carbon($date . $crossing[0], $timezone);
            $end = new Carbon($date . $crossing[1], $timezone);
            $newResult = [];
            foreach ($result as $otherInterval) {
                if ($end <= $otherInterval[0]) {
                    $newResult[] = $otherInterval;
                    continue;
                }
                if ($begin >= $otherInterval[1]) {
                    $newResult[] = $otherInterval;
                    continue;
                }
                if ($end >= $otherInterval[1]) {
                    if ($begin <= $otherInterval[0]) {
                        continue;
                    } else {
                        $newResult[] = [$otherInterval[0], $begin, $otherInterval[2]];
                    }
                } else {
                    if ($begin <= $otherInterval[0]) {
                        $newResult[] = [$end, $otherInterval[1], $otherInterval[2]];
                    } else {
                        $newResult[] = [$otherInterval[0], $begin, $otherInterval[2]];
                        $newResult[] = [$end, $otherInterval[1], $otherInterval[2]];
                    }
                }
            }
            $newResult[] = [$begin, $end, $schedule];
            $result = $newResult;
        }
        usort($result, function ($intervalA, $intervalB) {
            return $intervalA[0] > $intervalB[0];
        });

        $rightBound = $begin;
        foreach ($result as $piece) {
            /** @var Carbon $pieceStart */
            $pieceStart = $piece[0];
            if ($piece[0] > $rightBound) {
//                    dd('error');
            }
            /** @var Carbon $pieceEnd */
            $pieceEnd = $piece[1];
            /** @var PlaygroundSchedule $schedule */
            $schedule = $piece[2];
            $totalPrice += self::getPriceForSchedule($schedule, $pieceEnd->diffAsCarbonInterval($pieceStart));
            $rightBound = $end;
        }

        if ($rightBound < $end) {
//                dd('error');
        }
        return $totalPrice;
    }
}
