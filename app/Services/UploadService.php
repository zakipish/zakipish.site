<?php
/**
 * Created by PhpStorm.
 * User: sergeisporyshev
 * Date: 02.10.18
 * Time: 21:22
 */

namespace App\Services;


use App\Models\Image;
use Illuminate\Http\File;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Spatie\ImageOptimizer\OptimizerChainFactory;
use Illuminate\Support\Facades\Log;

class UploadService
{

    const FOLDER = 'source';

    //Подготавливает к обработке изображение из внешних источников
    public function prepareRemoteImage($path){
        $tmpFileName = tempnam('/tmp', 'TMP_IMG');
        $img = file_get_contents($path);
        $originalFilename = basename(parse_url($path)['path']);
        file_put_contents($tmpFileName, $img);
        return new UploadedFile($tmpFileName, $originalFilename);
    }

    //Грузит изображение и оптимизирует его

    /**
     * @param File|UploadedFile $file
     * @return Image
     */
    public function uploadImage(UploadedFile $file)
    {
        Log::debug($file->getClientOriginalExtension());
        if ($file->getClientOriginalExtension() == 'svg') {
            $extension = 'svg';
        } else {
            $extension = $file->guessExtension();
        }
        $name = $this->generateUuid() . '.' .$extension;
        $originalName = $file->getFilename();

        $path = Storage::disk('public')->putFileAs(self::FOLDER, $file, $name);
        $fullPath = storage_path('app/public/' . $path);
        $optimizer = OptimizerChainFactory::create();
        $optimizer->optimize($fullPath);

        return Image::create([
            'code'  => $name,
            'original_name' => $originalName
        ]);
    }

    public function getOriginalUrl(Image $image)
    {
        return url(Storage::url(self::FOLDER . DIRECTORY_SEPARATOR . $image->code));
    }

    protected function generateUuid(){
        return preg_replace_callback('/[xy]/', function ($matches) {
            return dechex('x' == $matches[0] ? mt_rand(0, 15) : (mt_rand(0, 15) & 0x3 | 0x8));
        }
            , 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx');
    }
}
