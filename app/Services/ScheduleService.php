<?php

namespace App\Services;

use App\Models\Playground;
use App\Models\PlaygroundSchedule;
use \DateTime;
use Illuminate\Support\Facades\Log;

class ScheduleService
{
    public static function create(Playground $playground, $state, $date, $interval, $price)
    {
        $date = new DateTime($date);
        $dateStr = $date->format('Y-m-d');
        $timezone = $playground->getTimezone();
        $intervalStr = IntervalService::getTimetzrange($interval, $timezone);
        $schedules = $playground->playground_schedules()
                                ->where('date', '=', $dateStr)
                                ->where('interval', '&&', $intervalStr)
                                ->get();

        $timeInterval = [new DateTime($dateStr." ".$interval[0]), new DateTime($dateStr." ".$interval[1])];

        $schedules->each(function($schedule) use ($timezone, $timeInterval, $playground, $dateStr) {
            $schTimeInterval = $schedule->timeInterval;

            if ($schTimeInterval[0] >= $timeInterval[0] && $schTimeInterval[1] <= $timeInterval[1]) {
                $schedule->delete();
            } elseif ($schTimeInterval[0] <= $timeInterval[0] && $schTimeInterval[1] >= $timeInterval[1]) {
                $startInterval = [$schTimeInterval[0]->format('H:i'), $timeInterval[0]->format('H:i')];
                $endInterval = [$timeInterval[1]->format('H:i'), $schTimeInterval[1]->format('H:i')];
                if (ScheduleService::isIntervalEmpty($dateStr, $startInterval)) {
                    $schedule->delete();
                } else {
                    $schedule->update([ 'interval' => $startInterval ]);
                }

                if (!ScheduleService::isIntervalEmpty($dateStr, $endInterval)) {
                    PlaygroundSchedule::create([
                        'playground_id' => $playground->id,
                        'type' => 'date',
                        'state' => 'open',
                        'date' => $schedule->date,
                        'interval' => $endInterval,
                        'price' => $schedule->price
                    ]);
                }
            } elseif ($schTimeInterval[0] <= $timeInterval[0] && $schTimeInterval[1] >= $timeInterval[0]) {
                $cuttedInterval = [$schTimeInterval[0]->format('H:i'), $timeInterval[0]->format('H:i')];

                if (ScheduleService::isIntervalEmpty($dateStr, $cuttedInterval)) {
                    $schedule->delete();
                } else {
                    $schedule->update([
                        'interval' => $cuttedInterval
                    ]);
                }
            } elseif ($schTimeInterval[0] <= $timeInterval[1] && $schTimeInterval[1] >= $timeInterval[1]) {
                $cuttedInterval = [$timeInterval[1]->format('H:i'), $schTimeInterval[1]->format('H:i')];

                if (ScheduleService::isIntervalEmpty($dateStr, $cuttedInterval)) {
                    $schedule->delete();
                } else {
                    $schedule->update([
                        'interval' => $cuttedInterval
                    ]);
                }
            }
        });

        if ($state == 'open'){
            $schedule = PlaygroundSchedule::create([
                'playground_id' => $playground->id,
                'type' => 'date',
                'state' => 'open',
                'date' => $dateStr,
                'interval' => $interval,
                'price' => $price
            ]);

            return $schedule;
        }

        return true;
    }

    private static function isIntervalEmpty(String $dateStr, Array $interval)
    {
        $start = new DateTime($dateStr . " " . $interval[0]);
        $end = new DateTime($dateStr . " " . $interval[1]);

        return ($end->getTimestamp() - $start->getTimestamp()) <= 0;
    }
}
