<?php
/**
 * Created by PhpStorm.
 * User: sergeisporyshev
 * Date: 02.10.18
 * Time: 21:22
 */

namespace App\Services;


use App\Models\Slug;
use App\Models\Utils\SluggableInterface;

class SlugService
{
    protected $cyrillicMap = [
        'а' => 'a',
        'б' => 'b',
        'в' => 'v',
        'г' => 'g',
        'д' => 'd',
        'е' => 'e',
        'ё' => 'yo',
        'ж' => 'zh',
        'з' => 'z',
        'и' => 'i',
        'й' => 'j',
        'к' => 'k',
        'л' => 'l',
        'м' => 'm',
        'н' => 'n',
        'о' => 'o',
        'п' => 'p',
        'р' => 'r',
        'с' => 's',
        'т' => 't',
        'у' => 'u',
        'ф' => 'f',
        'х' => 'h',
        'ц' => 'c',
        'ч' => 'ch',
        'ш' => 'sh',
        'щ' => 'shch',
        'ъ' => '',
        'ы' => 'y',
        'ь' => '',
        'э' => 'eh',
        'ю' => 'yu',
        'я' => 'ya',
    ];


    /**
     * @param SluggableInterface $sluggable
     */
    public function generateSlug(SluggableInterface $sluggable)
    {
        $string = $sluggable->name ? $sluggable->name : $sluggable->title;
        $name = $this->transliterate($string);
        if (!$name) {
            $name = $name . '-' . $sluggable->created_at->format('Ymd');
        }
        $slugValue = $name;
        $iterator = 1;
        while (!$this->tryToUpdate($sluggable, $slugValue)) {
            $slugValue = $name . '-' . $iterator++;
        }
    }

    protected function tryToUpdate(SluggableInterface $sluggable, $slugValue)
    {
        $slag = $sluggable->slugs()->where('value', $slugValue)->first();
        if ($slag) {
            if ($slag->is_active) {
                // делать нечего, слаг уже есть и он активный
                return true;
            } else {
                $slag->is_active = true;
                $slag->save();
            }
        } elseif (!$this->checkSlug($slugValue, $sluggable)) {
            $sluggable->slugs()->create([
                'value' => $slugValue,
                'is_active' => true,
            ]);
        } else {
            return false;
        }

        $sluggable->slugs()
            ->where('value', '!=', $slugValue)
            ->where('is_active', true)
            ->update(['is_active' => false]);
        return true;
    }

    public function transliterate($text)
    {
        $res = '';
        $prepared = mb_strtolower(trim($text));
        $array = preg_split('//u', $prepared, null, PREG_SPLIT_NO_EMPTY);
        $prev = null;
        foreach ($array as $char) {
            if (preg_match('/[a-z0-9-]/', $char)) {
                $prev = $char;
                $res .= $char;
            } else if (preg_match('/\s/', $char)) {
                if ($prev !== 'SPACE') {
                    $res .= '-';
                }
                $prev = 'SPACE';
            } else if (isset($this->cyrillicMap[$char])) {
                $new = $this->cyrillicMap[$char];
                if ($char === 'х' && in_array($prev, ['c','s','e','h'])) {
                    $new = 'kh';
                }
                $prev = $new;
                $res .= $new;
            }
        }
        return $res;
    }

    public function checkSlug($slug, SluggableInterface $sluggable) {
        return Slug::query()
        ->where([
            'value' => $slug,
            'sluggable_type' => $sluggable->getTable(),
        ])->exists();
    }

}

