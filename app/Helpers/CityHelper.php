<?php
/**
 * Created by PhpStorm.
 * User: sergeisporyshev
 * Date: 15.10.18
 * Time: 19:58
 */

namespace App\Helpers;


use App\Models\City;

class CityHelper
{
    const SPB_NAME = 'Санкт-Петербург';
    const MSK_NAME = 'Москва';

    public static function getCityIdByTitle($title)
    {
        try{
            $city = City::where('name', $title)->first();
            if(empty($city))
                return null;

            return $city->id;
        }catch (\Exception $e){
            return null;
        }
    }
}