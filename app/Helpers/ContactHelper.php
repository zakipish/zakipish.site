<?php
/**
 * Created by PhpStorm.
 * User: sergeisporyshev
 * Date: 06.10.18
 * Time: 18:26
 */

namespace App\Helpers;


use App\Models\ContactType;

class ContactHelper
{
    const TYPE_PHONE    = 'Телефон';
    const TYPE_EMAIL    = 'E-mail';
    const TYPE_ADDRESS  = 'Адрес';
    const TYPE_SITE     = 'Сайт';
    const TYPE_VK       = 'Вконтакте';
    const TYPE_FB       = 'Facebook';
    const TYPE_OTHER    = 'Прочее';

    public static function getContactTypeIdByTitle($title){
        try{
            $type = ContactType::where('name', $title)->first();
            if(empty($type))
                $type = ContactType::where('name', self::TYPE_OTHER)->first();

            if(!empty($type))
                return $type->id;
            return null;

        } catch (\Exception $e){
            return null;
        }
    }
    public static function getContactSlugByTitle($title){
        try{
            $type = ContactType::where('name', $title)->first();
            if(empty($type))
                $type = ContactType::where('name', self::TYPE_OTHER)->first();

            if(!empty($type))
                return $type->slug;
            return null;

        } catch (\Exception $e){
            return null;
        }
    }
}
