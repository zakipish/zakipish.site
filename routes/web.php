<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/setauth/{id}', function($id){
    $user = \App\Models\User::find($id);
    \Illuminate\Support\Facades\Auth::login($user);
    return \Illuminate\Support\Facades\Auth::id();
});*/

Route::group([
    'prefix'    => 'auth',
    'namespace' => 'Auth',
    'as'        => 'auth.'
,], function (){
    Route::group([
        'prefix'    => 'social',
        'as'        => 'social.'
    ], function(){
        Route::get('{provider}', 'SocialAuthController@redirect')->name('redirect');
        Route::get('{provider}/callback', 'SocialAuthController@callback')->name('callback');
        Route::get('{provider}/unlink', 'SocialAuthController@unlink')->name('unlink');
    });
});


Route::get('/', 'PageController@index');
Route::get('/terms', 'PageController@terms');
Route::get('/privacy', 'PageController@privacy');

Route::get('/geopoint/club/{club}.png', 'GeoPointController@show');

Route::group([
    'prefix' => 'resizer'
], function(){
    Route::get('{mode}/{code}', 'ResizeController@process')->name('resizer');
});
