<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::namespace('Api\Front')
    ->group(function(){
        Route::get('/auth', 'AuthController@index');
        Route::post('/auth', 'AuthController@login');
        Route::delete('/auth', 'AuthController@logout');
        Route::post('/auth/reset', 'AuthController@reset');
        Route::post('/reg', 'RegController@register');
        Route::patch('/user', 'UserController@update');

        Route::apiResource('/game_levels', 'GameLevelController')
            ->only(['index']);

        Route::apiResource('/cities', 'CityController')
            ->only(['index']);

        Route::middleware(['check_tmp_session'])->group(function () {
            Route::namespace('User')->group(function() {
                Route::apiResource('/users.games', 'GameController')
                    ->only('index');
            });

            Route::namespace('City')->group(function() {
                Route::apiResource('/cities.departments', 'DepartmentController')
                    ->only(['index']);
            });

            Route::apiResource('/sports', 'SportController')
                ->only(['index']);

            Route::apiResource('/surfaces', 'SurfaceController')
                ->only(['index']);

            Route::apiResource('/users', 'UsersController')->only(['show']);
            Route::apiResource('/games.invites', 'InviteController')
                ->only(['store']);
            Route::get('/games/my', 'GameController@my');
            Route::get('/games/dates', 'GameController@dates');
            Route::get('/games/times', 'GameController@times');
            Route::post('/games/{id}/attach', 'GameController@attach');
            Route::post('/games/{id}/unattach', 'GameController@unattach');
            Route::apiResource('/games', 'GameController')
                ->only(['index', 'store', 'show','update']);

            Route::post('/events/{event}/participate', 'EventController@participate');
            Route::namespace('Event')->group(function() {
                Route::apiResource('/events.invites', 'InviteController')
                    ->only(['store']);
            });
            Route::apiResource('/events', 'EventController')
                ->only(['index', 'show']);

            Route::namespace('Club')->group(function() {
                Route::apiResource('/clubs.playgrounds', 'PlaygroundController')
                    ->only(['show']);
            });

            Route::apiResource('/club', 'ClubController')
                ->only(['show']);

            Route::get('/playgrounds/dates', 'PlaygroundController@dates');
            Route::get('/playgrounds/times', 'PlaygroundController@times');

            Route::get('/playgrounds/{playground}/dates', 'PlaygroundController@show_dates');
            Route::get('/playgrounds/{playground}/times', 'PlaygroundController@show_times');
            Route::get('/playgrounds/{playground}/prices', 'PlaygroundController@prices');

            Route::apiResource('/playgrounds', 'PlaygroundController')
                ->only(['index', 'show']);

            Route::apiResource('/playgrounds.games', 'PlaygroundGameController')
                ->only(['index']);

            Route::namespace('Chat')->group(function() {
                Route::apiResource('/chats.messages', 'MessageController')
                    ->only('index', 'store');
            });
            Route::get('/chats/{chat}/auth', 'ChatController@auth');

            Route::post('/friend_requests/{friendRequest}/accept', 'FriendRequestController@accept');
            Route::post('/friend_requests/{friendRequest}/decline', 'FriendRequestController@decline');
            Route::delete('/friend_requests/{responser_id}/byId', 'FriendRequestController@deleteByResponserId');
            Route::apiResource('/friend_requests', 'FriendRequestController')
                ->only(['index', 'store']);

            Route::apiResource('/friends', 'FriendController')
                ->only(['index']);
        });
    });

Route::namespace('Api\CRM')
    ->prefix('crm')
    ->group(function () {
        Route::get('/auth', 'AuthController@index');
        Route::post('/auth', 'AuthController@login');
        Route::delete('/auth', 'AuthController@logout');
        Route::middleware(['auth', 'has_role:' . \App\Models\User::ROLE_MODERATOR])->group(function () {
            // CHAT
            Route::namespace('Chat')->group(function() {
                Route::apiResource('/chats.messages', 'MessageController')
                    ->only('index', 'store');
            });

            Route::middleware(['check_club_owner'])->group(function() {
                // EVENTS
                Route::apiResource('/events', 'EventController')
                    ->only(['index', 'show', 'store', 'update']);

                // CLUBS
                Route::apiResource('/clubs', 'ClubController')
                    ->only(['index', 'show', 'update'])
                    ->middleware(['check_club_owner']);

                // PLAYGROUNDS
                Route::namespace('Club')->group(function() {
                    Route::apiResource('clubs.playgrounds', 'PlaygroundController')
                        ->only(['store']);
                });

                Route::namespace('Playground')->group(function() {
                    Route::apiResource('playgrounds.playground-schedules', 'PlaygroundScheduleController')
                       ->only(["store"]);
                    Route::apiResource('playgrounds.games', 'GameController')
                        ->only(['index', 'store', 'update']);
                    Route::apiResource('playgrounds.sports', 'SportController')
                        ->only(['index']);
                    Route::apiResource('playgrounds.statistics', 'StatisticsController')
                        ->only(['index']);
                });

                Route::get('/playgrounds/{playground}/prices', 'PlaygroundController@prices');
                Route::apiResource('/playgrounds', 'PlaygroundController')
                    ->only(['show', 'update']);
            });

            // HELPERS
            Route::apiResource('/images', 'ImageController')
                ->only(['store']);
            Route::apiResource('/sports', 'SportController')
                ->only(['index']);
            Route::apiResource('/game_levels', 'GameLevelController')
                ->only(['index']);
            Route::apiResource('/infrastructures', 'InfrastructureController')
                ->only(['index']);
            Route::apiResource('/contact_types', 'ContactTypeController')
                ->only(['index']);
            Route::apiResource('/sports', 'SportController')
                ->only(['index']);
            Route::apiResource('/services', 'ServiceController')
                ->only(["index"]);
            Route::apiResource('/equipments', 'EquipmentController')
                ->only(["index"]);
            Route::apiResource('/cities', 'CityController')
                ->only(['index']);
            Route::apiResource('/surfaces', 'SurfaceController')
                ->only(['index']);
            Route::apiResource('/users', 'UserController')
                ->only(['index']);
        });
    });

Route::namespace('Api\Admin')
    ->prefix('admin')
    ->group(function () {
    Route::get('/auth', 'AuthController@index');
    Route::post('/auth', 'AuthController@login');
    Route::middleware(['auth', 'has_role:' . \App\Models\User::ROLE_ADMIN])->group(function () {
        Route::apiResource('/club-chains', 'ClubChainController');
        Route::apiResource('/clubs', 'ClubController');
        Route::apiResource('/club-filters', 'ClubFilterController');
        Route::apiResource('/contacts', 'ContactController');
        Route::apiResource('/contact-types', 'ContactTypeController');
        Route::apiResource('/departments', 'DepartmentController');
        Route::get('/departments_all/{city_id}', 'DepartmentController@show_by_city_id');
        Route::apiResource('/equipments', 'EquipmentController');
        Route::apiResource('/events', 'EventController');
        Route::apiResource('/games', 'GameController');
        Route::apiResource('/game-levels', 'GameLevelController');
        Route::apiResource('/game-statuses', 'GameStatusController');
        Route::apiResource('/player-statuses', 'PlayerStatusController');
        Route::apiResource('/playgrounds', 'PlaygroundController');
        Route::apiResource('/playground-types', 'PlaygroundTypeController');
        Route::apiResource('/services', 'ServiceController');
        Route::apiResource('/sports', 'SportController');
        Route::patch('/sports', 'SportController@updateMany');
        Route::apiResource('/surfaces', 'SurfaceController');
        Route::apiResource('/cities', 'CityController');
        Route::apiResource('/infrastructures', 'InfrastructureController');
        Route::apiResource('/playground-schedules', 'PlaygroundScheduleController');
        Route::patch('/playground-schedules', 'PlaygroundScheduleController@updateMany');
        Route::apiResource('/booking-types', 'BookingTypeController');
        Route::apiResource('/users', 'UserController');

        // HELPERS
        Route::post('/images/multiple', 'ImageController@multiple');
        Route::apiResource('/images', 'ImageController')
            ->only(['store']);
    });
});
